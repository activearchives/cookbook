(function () {
//http://stackoverflow.com/questions/11197247/javascript-equivalent-of-jquerys-extend-method
function extend(){
    for(var i=1; i<arguments.length; i++)
        for(var key in arguments[i])
            if(arguments[i].hasOwnProperty(key))
                arguments[0][key] = arguments[i][key];
    return arguments[0];
}

var defaults = {};

function ranger_controller (elt, timeline, opts) {
    opts = extend({}, defaults, opts);
    var that = {elt: elt, timeline: timeline};
    var data = timeline.data();
    $(data).each(function () {
        this.duration = (this.end) ? this.end - this.start : 10;
    })

    var width=$(elt).width();
    var s = d3.scale.linear().domain([0, timeline.max()]).range([0, width]);
    var svg = d3.select(elt).append("svg")
        .attr("class", "timeline")
        .attr("width", width)
        .attr("height", 28);

    var bgenter = svg.append("g").attr("class", "background").selectAll("rect").data(data).enter();
    bgenter.append("rect")
        .attr("class", "item")
        .attr("x", function (d) { return s(d.start); })
        .attr("y", 7)
        .attr("height", 14)
        .attr("width", function (d) { return (s(d.duration)-1) });

    var drag = d3.behavior.drag().origin(Object);
    drag.on("drag", function (d) {
        // console.log(this, d3.event);
        d.x = Math.max(0, Math.min(width, d3.event.x));
        d3.select(this).attr("transform", "translate("+d.x+" 0)");
        var ct = s.invert(d.x);
        timeline.slider.slide(ct, ct);
        if (opts.drag) {
            opts.drag.call(that);
        }
    });

    var thumb = svg.append("g")
        .attr("class", "thumb")
        .data([{x: 0, y: 0}])
    thumb.append("rect")
        .attr("transform", "rotate(45 0 14) translate(-10 4)")
        .attr("width", 20)
        .attr("height", 20);
    thumb.call(drag);

    return that;
}  
window.ranger_controller = ranger_controller;

})();