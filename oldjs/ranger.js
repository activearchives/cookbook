(function () {

/*

Ranger.js

Handy management of ranges of items,
useful for timelines, or spatial scrolls.

Values may be numbers, dates, or any comparable values
(nb: for malleable objects other than Date(),
a custom copy function should be defined)

var r = ranger();

// add items with start and end (from and to)
r.add(item, start, end)
r.add(item, from, to)
r.add(item, from, to)

...

r.max()
r.min()
r.each(function () { console.log(this+" on ranger") });

// Use from to get a (slidable) range 
var range = r.from(100, 200);

range.each(function () {
    console.log(this + " in range");
});

// Sliding the range to a new start and end values
// alters the current selection
// NB: start and end may be equal
// enter() and exit() represent items entering and exiting
// the range from the last slide
// nb items in enter will also be in the "each" of the range

var slide = range.slide(100, 100)
    .each(function (rstart, rend) {
        $(this).text("time" + rstart);
    });

slide.enter().each(function (rstart, rend) {
    $(this).addClass("active");
});

slide.exit().each(function (rstart, rend) {
    $(this).removeClass("active");
});


*/


//http://stackoverflow.com/questions/11197247/javascript-equivalent-of-jquerys-extend-method
function extend(){
    for(var i=1; i<arguments.length; i++)
        for(var key in arguments[i])
            if(arguments[i].hasOwnProperty(key))
                arguments[0][key] = arguments[i][key];
    return arguments[0];
}

function _insert_sorted (item, propname, arr) {
    var i=0, len=arr.length;
    for (; i<len; i++) {
        if ((arr[i][propname] > item[propname]) ||
             ((arr[i][propname] === undefined) && (item[propname] !== undefined))) {
            // insert before this index
            arr.splice(i, 0, item);
            return;
        }
    }
    // otherwise simply append
    arr.push(item);
}

var defaults = {
    // if start and end values are mutable, a custom copy function may be necessary to duplicate it safely; by default, assumes immutable (or not a problem)
    copy: function (x) {
        if (x instanceof Date) {
            var ret = new Date();
            ret.setTime(x.getTime());
            return ret;
        } else {
            return x;
        }
    },

    test: function (s1, e1, s2, e2) {
        // console.log("ranger.test", s1, e1, s2, e2, (((s2 < e1) && (s1 < e2)) || (s1 == s2)));
        // http://stackoverflow.com/questions/3269434/whats-the-most-efficient-way-to-test-two-integer-ranges-for-overlap
        // the s1 == s2 allows for "null range" support (where s1 == e1)
        return (((s2 < e1) && (s1 < e2)) || (s1 == s2));
    },

    debug: false
};

function ranger (opts) {

    opts = extend({}, defaults, opts);

    var that = {},
        uid = 0, /* to generate unique item ids */
        itemsByStart = [],
        itemsByEnd = [],
        min,
        max;

    that.add = function (elt, start, end) {
        /* Sanity Check */
        /* How to ensure range sets get updated properly ?!
            ie how to ensure enter & adding to update set occurs now
            // do you have a special slider.add ?! YES! though it's a little weird if multiple ranges are used
        */
        if (start === undefined) { throw "ranger.add: no start"; }
        if (end && (end < start)) { throw "ranger.add: end is before start, ignoring"; }

        var item = {
            id: "I"+(++uid),
            elt: elt,
            start: start,
            end: end
        };

        if ((min === undefined) || (item.start < min)) { min = item.start; }
        if ((max === undefined) || (item.start > max)) { max = item.start; }
        if ((max === undefined) || (item.end && (item.end > max))) { max = item.end; }

        _insert_sorted(item, 'start', itemsByStart);
        _insert_sorted(item, 'end', itemsByEnd);

        return that;
    };

    that.min = function () { return min; }
    that.max = function () { return max; }

    that.unparse = function () {
        var ret = "<ranger "+ itemsByStart.length + " items ["+min+" - "+max+"]>\n";
        ret += "itemsByStart:\n";
        for (var i=0; i<itemsByStart.length && i < 10; i++) {
            var item = itemsByStart[i];
            // if (ssi == i) { ret += "S"; }
            // if (esi == i) { ret += "E"; }
            ret += "["+item.id+ " "+item.start+"-"+item.end+"]\n";
        }
        ret += "itemsByEnd:\n";
        for (var i=0; i<itemsByEnd.length && i < 10; i++) {
            var item = itemsByEnd[i];
            // if (sei == i) { ret += "S"; }
            // if (eei == i) { ret += "E"; }
            ret += "["+item.id+ " "+item.start+"-"+item.end+"]\n";
        }
        // var actives = getActive();
        // ret += "ACTIVES: " + actives.length;
        return ret;
    };

    that.each = function (callback) {
        var i, len;
        for (i=0, len = itemsByStart.length; i<len; i++) {
            callback.call(itemsByStart[i]);
        }
        return that;
    }

    that.get = function (index) {
        var ret = itemsByStart.slice(0);
        return (index !== undefined) ? ret[index] : ret;
    }

    that.data = function () {
        return itemsByStart.slice(0);
    }

    that.from = function (start, end) {
        // console.log("from", start, end);
        var that = {},
            ssi = -1, sei = -1,
            esi = -1, eei = -1,
            _start, _end,
            lastStart, lastEnd,
            activeItems = {},
            enterItems = {},
            exitItems = {};

        function updateForValue (start, end) {
            // console.log("updateForValue", start, end, lastStart, lastEnd);
            if (itemsByStart.length === 0) { return; }
            var toUpdate = {}, n;
 
            function mark (item) {
                toUpdate[item.id] = item;
            }

            if (lastStart == undefined || start >= lastStart) { // SLIDE FORWARD
                n = itemsByStart.length;
                while ((ssi+1) < n && start >= itemsByStart[ssi+1].start) {
                    ssi++;
                    if (ssi < n) { mark(itemsByStart[ssi]); }
                }    
                n = itemsByEnd.length;
                while ((sei+1) < n && start >= itemsByEnd[sei+1].end) {
                    sei++;
                    if (sei < n) { mark(itemsByEnd[sei]); }
                }    
            } else { // SLIDE BACKWARD
                while (ssi >= 0 && start < itemsByStart[ssi].start) {
                    mark(itemsByStart[ssi]);
                    ssi--;
                }
                while (sei >= 0 && start < itemsByEnd[sei].end) {
                    mark(itemsByEnd[sei]);
                    sei--;
                }
            }
            lastStart = opts.copy(start);

            // Adjust end indices (esi, eei)
            if (lastEnd == undefined || end >= lastEnd) { // SLIDE FORWARD
                // console.log("  sliding end forward");
                n = itemsByStart.length;
                while ((esi+1) < n && end > itemsByStart[esi+1].start) {
                // while ((esi+1) < n && !opts.test(start, end, itemsByStart[esi+1].start, itemsByStart[esi+1].end)) {
                    esi++;
                    if (esi < n) { mark(itemsByStart[esi]); }
                }    
                n = itemsByEnd.length;
                while ((eei+1) < n && end >= itemsByEnd[eei+1].end) {
                // while ((eei+1) < n && !opts.test(start, end, itemsByEnd[eei+1].start, itemsByEnd[eei+1].end)) {
                    eei++;
                    if (eei < n) { mark(itemsByEnd[eei]); }
                }    
            } else { // SLIDE BACKWARD
                // console.log("  sliding end backward");
                while (esi >= 0 && end < itemsByStart[esi].start) {
                // while (esi >= 0 && opts.test(start, end, itemsByStart[esi].start, itemsByStart[esi].end)) {
                    mark(itemsByStart[esi]);
                    esi--;
                }
                while (eei >= 0 && end <= itemsByEnd[eei].end) {
                // while (eei >= 0 && opts.test(start, end, itemsByEnd[eei].start, itemsByEnd[eei].end)) {
                    mark(itemsByEnd[eei]);
                    eei--;
                }
            }
            lastEnd = opts.copy(end);

            // update
            var item_id;
            for (item_id in toUpdate) {
                if (toUpdate.hasOwnProperty(item_id)) { // JSLint (not strictly necessary)
                    // eval new state
                    var item = toUpdate[item_id],
                        was_in_range = (activeItems[item_id] !== undefined),
                        now_in_range = opts.test(_start, _end, item.start, item.end);

                    // console.log("u", item.id, item.elt, was_in_range, now_in_range);

                    if (was_in_range != now_in_range) {
                        if (now_in_range) {
                            // console.log("enter", item);
                            activeItems[item_id] = item;
                            enterItems[item_id] = item;
                        } else {
                            // console.log("exit", item);
                            delete activeItems[item_id];
                            exitItems[item_id] = item;
                        }
                    }
                }
            }
            // console.log("toUpdate", toUpdate);
            // console.log("activeItems", activeItems);
            // console.log("enterItems", enterItems);
            // console.log("exitItems", exitItems);
        }

        that.slide =function (start, end) {
            _start = start;
            _end = end;
            enterItems = {}; exitItems = {};
            updateForValue(start, end);
            // if (opts.debug) { debug(); }
            return that;
        };

        function values (set) {
            var item_id,
                ret = [];
            for (item_id in set) {
                if (set.hasOwnProperty(item_id)) { // JSLint (not strictly necessary)
                    ret.push(set[item_id]);
                }
            }
            return ret;
        }

        function each (callback, set) {
            var item_id;
            for (item_id in set) {
                if (set.hasOwnProperty(item_id)) { // JSLint (not strictly necessary)
                    var item = set[item_id];
                    callback.call(item.elt, item.start, item.end, _start, _end);
                }
            }            
        }

        that.get = function (index) {
            var ret = values(activeItems);
            return (index !== undefined) ? ret[index] : ret;
        };

        that.each = function (callback) { each(callback, activeItems); return that; };

        that.enter = function (callback) {
            var that = {},
                items = enterItems;
            that.each = function (callback) { each(callback, items); return that; };
            that.get = function (index) {
                var ret = values(items);
                return (index !== undefined) ? ret[index] : ret;
            }
            if (callback) { each(callback, items); }
            return that;
        }

        that.exit = function (callback) {
            var that = {},
                items = exitItems;
            that.each = function (callback) { each(callback, items); return that; };
            that.get = function (index) {
                var ret = values(items);
                return (index !== undefined) ? ret[index] : ret;
            }
            if (callback) { each(callback, items); }
            return that;
        }

        that.unparse = function () {
            var ret = "<ranger "+ itemsByStart.length + " items ["+min+" - "+max+"]>\n";
            ret += "itemsByStart:\n";
            for (var i=0; i<itemsByStart.length && i < 10; i++) {
                var item = itemsByStart[i];
                if (ssi == i) { ret += "S"; }
                if (esi == i) { ret += "E"; }
                ret += "["+item.id+ " "+item.start+"-"+item.end+"]\n";
            }
            ret += "itemsByEnd:\n";
            for (var i=0; i<itemsByEnd.length && i < 10; i++) {
                var item = itemsByEnd[i];
                if (sei == i) { ret += "S"; }
                if (eei == i) { ret += "E"; }
                ret += "["+item.id+ " "+item.start+"-"+item.end+"]\n";
            }
            var actives = that.get();
            ret += "ACTIVES: " + actives.length;
            return ret;
        };

        _start = start;
        _end = end;
        updateForValue(start, end);
        return that;
    }

    // make a "default" slider
    that.slider = that.from(-1, -1);

    return that;
}

window.ranger = ranger;

})();
