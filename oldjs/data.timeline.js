(function () {

function rdf_get_first (rdf, nodes, props, def) {
    var b, i, len;
    for (var x=0, xl=nodes.length; x<xl; x++) {
        var node = nodes[x];
        if (node.type != "literal") {
            for (var i=0, len=props.length; i<len; i++) {
                b = rdf.where(node.toString()+" "+props[i]+" ?v");
                if (b.length)    {
                    return b.get(0).v.value;
                }
            }
        }
    }
    return def;
}

$.fn.play = function () {
    var ct = 0,
        timeline = window.ranger({
            enter: function () {
                // console.log("enter", this);
                $(this).addClass("active");
            },
            exit: function () {
                // console.log("exit", this);
                $(this).removeClass("active");
            }
        }),
        data = [];
    this.each(function () {
        var href = $(this).attr("href"),
            n = $.rdf.resource("<"+href+">"),
            d = rdf_get_first(window.rdf, [n], ["media:duration"], 10);
        timeline.add({elt: this, start: ct, end: ct + d});
        data.push({start: ct, duration: d, playing: false});
        ct += d;
        console.log(n.toString(), d, ct);
    });

    var width=$("#timeline").width();
    var s = d3.scale.linear().domain([0, timeline.max()]).range([0, width]);
    var svg = d3.select("#timeline").append("svg")
        .attr("class", "timeline")
        .attr("width", width)
        .attr("height", 28);

    var bgenter = svg.append("g").attr("class", "background").selectAll("rect").data(data).enter();
    bgenter.append("rect")
        .attr("class", "item")
        .attr("x", function (d) { return s(d.start); })
        .attr("y", 7)
        .attr("height", 14)
        .attr("width", function (d) { return (s(d.duration)-1) });

    var drag = d3.behavior.drag().origin(Object);
    drag.on("drag", function (d) {
        // console.log(this, d3.event);
        d.x = Math.max(0, Math.min(width, d3.event.x));
        d3.select(this).attr("transform", "translate("+d.x+" 0)");
        var ct = s.invert(d.x);
        timeline.set(ct, ct);
    });

    var thumb = svg.append("g")
        .attr("class", "thumb")
        .data([{x: 0, y: 0}])
    thumb.append("rect")
        .attr("transform", "rotate(45 0 14) translate(-10 4)")
        .attr("width", 20)
        .attr("height", 20);
    thumb.call(drag);

    console.log(timeline.max());
    return this;
}

})();