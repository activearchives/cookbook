(function ($) {

function controller (elt, opts) {
    opts = $.extend({
        playable: elt
    }, opts);

    var $elt = $(elt),
        that = {elt: elt},
        $media = $(opts.playable).playable(),
        frame = $("<div></div>").addClass("aa-playable-controller").appendTo($elt),
        toggle = $("<div></div>").addClass("aa-playable-controller-toggle").appendTo(frame),
        track = $("<div></div>").addClass("aa-playable-controller-track").appendTo(frame),
        bg = $("<div></div>").addClass("aa-playable-controller-background").appendTo(track),
        thumb = $("<div></div>").addClass("aa-playable-controller-thumb").appendTo(track),
        duration = 0,
        dragging = false,
        maxpos = 0;

    duration = $media.playable("duration");

    function set_time_display (t) {
        if (t === undefined) { t = 0; }
        var timelabel = t.timecodeToString();
        if (duration) {
            timelabel += "/" + duration.timecodeToString();
        }
        thumb.attr("title", timelabel);
    }

    $media.bind("durationchange", function () {
        duration = $media.playable("duration");
        set_time_display();
        // console.log("controller: duration change", duration);
    }).bind("timeupdate", function () {
        if (dragging) { return; }
        var ct = $media.playable("currentTime");
        set_time_display(ct);
        if (duration) {
            maxpos = track.width() - thumb.width();
            var pos = Math.floor(maxpos * (ct / duration));
            thumb.css("left", pos+"px");
        }
    }).bind("play", function () {
        $elt.addClass("playing");
    }).bind("pause", function () {
        $elt.removeClass("playing");
    }).bind("waiting", function () {
        $elt.addClass("waiting");
    }).bind("canplay", function () {
        $elt.removeClass("waiting");
    }).bind("seeking", function () {
        $elt.addClass("seeking");
    }).bind("seeked", function () {
        $elt.removeClass("seeking");
    });

    var thumbelt = thumb.get(0);
    track.click(function (e) {
        // console.log("track click", e.target);
        if (e.target !== thumbelt) {
            var offset = track.offset(),
                mx = e.pageX - offset.left,
                maxpos = track.width() - thumb.width();
            // console.log("track click", (mx/maxpos));
            if (duration) {
                var t = duration * Math.max(0.0, Math.min(1.0, (((mx-(thumb.width()*0.5))/maxpos))));
                $media.playable("currentTime", t);
            }
        }
    });

    thumb.draggable({
        containment: 'parent',
        start: function () {
            dragging = true;
            duration = $media.playable("duration");
            var paused = $media.playable("paused");
            if (!paused) { $media.playable("pause"); }
            maxpos = track.width() - thumb.width();
        },
        drag: function () {
            var l = parseInt(thumb.css("left"));
            if (duration) {
                var t = duration * (l/maxpos);
                $media.playable("currentTime", t);
                set_time_display(t);
            }
        },
        stop: function () {
            dragging = false;
        }
    })
    toggle.bind("click", function () {
        $media.playable("paused") ? $media.playable("play") : $media.playable("pause");
    });

    return that;
}

$.fn.controller = function () {
    var returnvalue,
        firstarg = arguments[0],
        otherargs = [].splice.call(arguments,1);

    var chainreturn = this.each(function () {
        var $this = $(this);
        var obj = $(this).data("controller");
        if (!obj) {
            // init the object
            $this.data("controller", controller(this, firstarg));
        } else if (firstarg) {    
            // dispatch to object
            if ($.isFunction(obj[firstarg])) {
                returnvalue = obj[firstarg].apply(this, otherargs);
            } else {
                console.log("controller: error dispatching", firstarg, otherargs);
            }
        }
    });

    return (returnvalue !== undefined) ? returnvalue : chainreturn;
}


})(jQuery);
