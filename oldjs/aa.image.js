(function ($) {

var mfparset = function (url) {
    var mf = MediaFragments.parseMediaFragmentsUri(url);
    if (mf.hash && mf.hash.t && mf.hash.t.length) {
        return {
            start: mf.hash.t[0].startNormalized,
            end: mf.hash.t[0].endNormalized
        }
    }
}

var defaults = {};

function aaimage (elt, opts) {
    opts = $.extend({}, defaults, opts);
    var that = {elt: elt},
        $elt = $(elt),
        headwrap = $elt.find(".headwrap"),
        head = $elt.find(".head"),
        body = $elt.find(".body"),
        rdf = $.rdf(), 
        image;

    rdf.prefix("aa", "http://activearchives.org/terms/");
    rdf.prefix("dc", "http://purl.org/dc/elements/1.1/")

    /* create structure if missing */
    if (headwrap.size() == 0) {
        headwrap = $("<div></div>").addClass("headwrap").appendTo(elt); }
    if (head.size() == 0) {
        head = $("<div></div>").addClass("head").html(opts.src).appendTo(headwrap); }
    if (body.size() == 0) {
        body = $("<div></div>").addClass("body").appendTo(elt); }

    /* activate */
    $elt
        .addClass("aabox")
        .addClass("aaimagebox")
        // .attr("data-embed-href", opts.src) // now done by aadoc
        .mouseenter(function () {
            $(this).addClass("focus");
        })
        .mouseleave(function () {
            $(this).removeClass("focus");
        })
        .draggable({
            handle: ".head"
        });

    that.setfragment = function (f) {
        var mf = mfparset(f);
        if (mf && mf.start !== undefined) {
            video.currentTime = mf.start;
            video.play();
        }
    }

    function urlstripextension (url) {
        var a = document.createElement("a");
        a.setAttribute("href", url);
        // console.log("url", url, a)
        return "http://" + a.hostname + a.pathname.replace(/\..*$/, ""); 
    }

    function load (src) {
        var $image = $("<img />")
            .attr("src", src)
            .appendTo(body)
            .bind("loaded", function () {
                // targethref = opts.src.replace(/\..*$/, ""),
                var targethref = urlstripextension(opts.src),
                    target = $.rdf.resource("<"+targethref+">"),
                    body = $.rdf.resource("<"+opts.src+">"),
                    width = $image.width(),
                    height = $image.height();

                console.log("image loaded", width, height);

                rdf.add(target+" aa:hasAnnotation "+body);
                rdf.add(body+' aa:hasWidth '+width);
                rdf.add(body+' aa:hasHeight '+height);
                if (opts.load) {
                    opts.load.call(elt, that);
                }
            });
        image = $image.get(0);
    }
    that.load = load;

    that.rdf = function () {
        return rdf;
    }

    if (opts.src) {
        load(opts.src);
    }

    $(elt).data("aaobj", that); // deprecate --> this becomes aadoc.widget
    return that;
}

function jqwidget (name, fn) {
    var ret = function (opts) {
        return this.each(function () {
            var elt = this,
                obj = $(this).data(name);
            if (obj === undefined) {
                obj = fn.call(elt, elt, opts);
                $(this).data(name, obj);
            } else {
                var method = arguments[0],
                    args = Array.prototype.slice.call(arguments, 1);
                obj.method.apply(elt, args);
            }
        });
    };
    return ret;
}

$.fn.aaimage = jqwidget("aaimage", aaimage);

})(jQuery);