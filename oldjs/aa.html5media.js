(function ($) {

var mfparset = function (url) {
    var mf = MediaFragments.parseMediaFragmentsUri(url);
    if (mf.hash && mf.hash.t && mf.hash.t.length) {
        return {
            start: mf.hash.t[0].startNormalized,
            end: mf.hash.t[0].endNormalized
        }
    }
}

function timecode (secs) {
    /* 70 => 00:01:10, 70.5 => 00:01:10.500 */
        var h, m, s, r;
        h = Math.floor(secs/3600);
        secs -= h*3600;
        m = Math.floor(secs/60);
        secs -= m*60;
        s = Math.floor(secs);
        r = "" + (h<10 ? "0"+h:h)+":"+(m<10 ? "0"+m:m)+":"+(s<10 ? "0"+s:s);
        if (secs - s) {
            // maybe should actually round this?!
            var f = ""+(secs-s),
                fs = f.substr(2, 3);
            while (fs.length < 3) { fs = fs+"0" } 
            r += "."+fs;
        }
        return r;
}
window.timecode = timecode;

var defaults = {};

function html5media (elt, opts) {
    opts = $.extend({}, defaults, opts);
    var that = {elt: elt},
        $elt = $(elt),
        headwrap = $elt.find(".headwrap"),
        head = $elt.find(".head"),
        body = $elt.find(".body"),
        rdf = $.rdf(), 
        video;

    rdf.prefix("aa", "http://activearchives.org/terms/");
    rdf.prefix("dc", "http://purl.org/dc/elements/1.1/")

    /* create structure if missing */
    if (headwrap.size() == 0) {
        headwrap = $("<div></div>").addClass("headwrap").appendTo(elt); }
    if (head.size() == 0) {
        head = $("<div></div>").addClass("head").html(opts.src).appendTo(headwrap); }
    if (body.size() == 0) {
        body = $("<div></div>").addClass("body").appendTo(elt); }

    /* activate */
    $elt
        .addClass("aabox")
        .addClass("aahtml5mediabox")
        .attr("data-embed-href", opts.src)
        .mouseenter(function ( ){
            $(this).addClass("focus");
        }).mouseleave(function () {
            $(this).removeClass("focus");
        })
        .draggable({
            handle: ".head"
        });

    that.setfragment = function (f) {
        var mf = mfparset(f);
        if (mf && mf.start !== undefined) {
            video.currentTime = mf.start;
            video.play();
        }
    }

    function urlstripextension (url) {
        var a = document.createElement("a");
        a.setAttribute("href", url);
        // console.log("url", url, a)
        return "http://" + a.hostname + a.pathname.replace(/\..*$/, ""); 
    }

    function load (src) {
        var $video = (src.match(/.ogg/) ? $("<audio controls></audio>") : $("<video controls></video>"))
            .attr("src", src)
            .appendTo(body)
            .bind("durationchange", function () {
                // targethref = opts.src.replace(/\..*$/, ""),
                var targethref = urlstripextension(opts.src),
                    hash = "#t=0,"+timecode(this.duration),
                    target = $.rdf.resource("<"+targethref+hash+">"),
                    body = $.rdf.resource("<"+opts.src+hash+">");

                rdf.add(target+" aa:hasAnnotation "+body);
                rdf.add(body+' aa:hasDuration '+this.duration);
                if (opts.load) {
                    opts.load.call(elt, that);
                }
            })
            .bind("timeupdate", function () {
                var ct = this.currentTime;
                $(this).trigger("fragmentupdate", opts.src+"#t="+timecode(ct));
            });
        video = $video.get(0);
    }
    that.load = load;

    that.rdf = function () {
        return rdf;
    }

    if (opts.src) {
        load(opts.src);
    }

    $(elt).data("aaobj", that);
    return that;
}

function jqwidget (name, fn) {
    var ret = function (opts) {
        return this.each(function () {
            var elt = this,
                obj = $(this).data(name);
            if (obj === undefined) {
                obj = fn.call(elt, elt, opts);
                $(this).data(name, obj);
            } else {
                var method = arguments[0],
                    args = Array.prototype.slice.call(arguments, 1);
                obj.method.apply(elt, args);
            }
        });
    };
    return ret;
}

$.fn.html5media = jqwidget("html5media", html5media);

})(jQuery);