(function ($) {

var defaults = {
    // if values are mutable, you may need to specify a custom copy function to duplicate it safely
    // by default, assumes immutable (or not a problem)
    copy: function (x) {
        if (x instanceof Date) {
            var ret = new Date();
            ret.setTime(x.getTime());
            return ret;
        } else {
            return x;
        }
    },
    test: function (start, end, s, e) {
        // http://stackoverflow.com/questions/3269434/whats-the-most-efficient-way-to-test-two-integer-ranges-for-overlap
        // console.log("test", start, end, s, e, (((s < end) && (start < e)) || (start == s)));
        return (((s < end) && (start < e)) || (start == s)); // the start == s check is for "null range" support (where start == end)
    },
    debug: false
};

function ranger (opts) {
    opts = $.extend({}, defaults, opts);
    var that = {},
        min, max,
        _start, _end,
        lastStart, lastEnd,
        uid = 0, /* to generate item ids */
        itemsByStart = [],
        itemsByEnd = [],
        ssi = -1, sei = -1,
        esi = -1, eei = -1,
        activeItems = {}; /* maintains index of currently in range items */

    that.add = function (args) {
        var elt = args.elt,
            start = args.start,
            end = args.end,
            enter = args.enter,
            exit = args.exit;

        var item = {
            id: "I"+(++uid),
            elt: elt,
            start: start,
            end: end,
            enter: enter,
            exit: exit
        };
        // console.log("elt", elt);
        if (start === undefined) { console.log("ranger.add: no start, ignoring"); return; }
        if (end && (end < start)) { console.log("ranger.add: end is before start, ignoring"); return; }

        /* maintain min/max */
        if ((min === undefined) || (item.start < min)) { min = item.start; }
        if ((max === undefined) || (item.start > max)) { max = item.start; }
        if ((max === undefined) || (item.end && (item.end > max))) { max = item.end; }

        _insert_sorted(item, 'start', itemsByStart);
        _insert_sorted(item, 'end', itemsByEnd);

        // assert proper state on insertion
        if (opts.test(_start, _end, item.start, item.end)) {
            _enter(item);
            activeItems[item.id] = item;
        } else {
            _exit(item);
        }
        set(_start, _end);
    }

    function _insert_sorted (item, propname, arr) {
        var i=0, len=arr.length;
        for (; i<len; i++) {
            if ((arr[i][propname] > item[propname]) ||
                 ((arr[i][propname] === undefined) && (item[propname] !== undefined))) {
                // insert before this index
                arr.splice(i, 0, item);
                return;
            }
        }
        // otherwise simply append
        arr.push(item);
    }
    
    function _enter (item) {
        if (item.enter) { item.enter.call(item.elt); }
        else if (opts.enter) { opts.enter.call(item.elt); }
    }

    function _exit (item) {
        if (item.exit) { item.exit.apply(item.elt); }
        else if (opts.exit) { opts.exit.apply(item.elt); }
    }
    /*
    function _in_range (item) {
        // s in [start, end) OR
        // e in [start, end) OR
        // (s < start and e >= end)
        var ret = ( ((item.start >= _start) && (item.start < _end)) ||
                 ((item.end >= _start) && (item.end < _end)) ||
                 ((item.start < _start) && (item.end >= _end))
               );
        if (item.id == "I4") {
            console.log("I4 in range?", item.start, item.end, _start, _end, ret);
        }
        return ret;

    }
    */

    function updateForValue (start, end) {
        // console.log("updateForValue", start, end, lastStart, lastEnd);
        if (itemsByStart.length === 0) { return; }

        var toUpdate = {}, n;

        function mark (item) {
            toUpdate[item.id] = item;
        }

        /*
        For both the range start and end values,
        we maintain an index on both the itemsByStart (_si)
        and itemsByEnd (_ei) pointing to the "edge" of elements between
        which the start or end value (respectively) now sits

        start: ssi, sei
        end: esi, eei

        Why? Because these four variables then allow the following
        propositions to be determined:
            s < start
            s < end
            e < start
            e < end

        start, end: are the ranges (new) start & end values
        s, e: are (some) item start and end values

        Adjust ranges by sliding forward/backward
        indexes -- "touched" elements are simply "marked" for update
        Update reevaluates whether element is in range or not and
        shows / hide when state CHANGES (need to compare with "old" state)
        */

        // Adjust start indices (ssi, sei)
        // console.log("updating");
        if (lastStart == undefined || start >= lastStart) { // SLIDE FORWARD
            // console.log("  sliding start forward");
            n = itemsByStart.length;
            while ((ssi+1) < n && start >= itemsByStart[ssi+1].start) {
            // while ((ssi+1) < n && !opts.test(start, end, itemsByStart[ssi+1].start, itemsByStart[ssi+1].end)) {
                ssi++;
                if (ssi < n) { mark(itemsByStart[ssi]); }
            }    
            n = itemsByEnd.length;
            while ((sei+1) < n && start >= itemsByEnd[sei+1].end) {
            // while ((sei+1) < n && !opts.test(start, end, itemsByEnd[sei+1].start, itemsByEnd[sei+1].end)) {
                sei++;
                if (sei < n) { mark(itemsByEnd[sei]); }
            }    
        } else { // SLIDE BACKWARD
            // console.log("  sliding start backward");
            while (ssi >= 0 && start < itemsByStart[ssi].start) {
            // while (ssi >= 0 && opts.test(start, end, itemsByStart[ssi].start, itemsByStart[ssi].end)) {
                mark(itemsByStart[ssi]);
                ssi--;
            }
            while (sei >= 0 && start < itemsByEnd[sei].end) {
            // while (sei >= 0 && opts.test(start, end, itemsByEnd[sei].start, itemsByEnd[sei].end)) {
                mark(itemsByEnd[sei]);
                sei--;
            }
        }
        lastStart = opts.copy(start);

        // Adjust end indices (esi, eei)
        if (lastEnd == undefined || end >= lastEnd) { // SLIDE FORWARD
            // console.log("  sliding end forward");
            n = itemsByStart.length;
            while ((esi+1) < n && end > itemsByStart[esi+1].start) {
            // while ((esi+1) < n && !opts.test(start, end, itemsByStart[esi+1].start, itemsByStart[esi+1].end)) {
                esi++;
                if (esi < n) { mark(itemsByStart[esi]); }
            }    
            n = itemsByEnd.length;
            while ((eei+1) < n && end >= itemsByEnd[eei+1].end) {
            // while ((eei+1) < n && !opts.test(start, end, itemsByEnd[eei+1].start, itemsByEnd[eei+1].end)) {
                eei++;
                if (eei < n) { mark(itemsByEnd[eei]); }
            }    
        } else { // SLIDE BACKWARD
            // console.log("  sliding end backward");
            while (esi >= 0 && end < itemsByStart[esi].start) {
            // while (esi >= 0 && opts.test(start, end, itemsByStart[esi].start, itemsByStart[esi].end)) {
                mark(itemsByStart[esi]);
                esi--;
            }
            while (eei >= 0 && end <= itemsByEnd[eei].end) {
            // while (eei >= 0 && opts.test(start, end, itemsByEnd[eei].start, itemsByEnd[eei].end)) {
                mark(itemsByEnd[eei]);
                eei--;
            }
        }
        lastEnd = opts.copy(end);

        // update
        var item_id;
        for (item_id in toUpdate) {
            if (toUpdate.hasOwnProperty(item_id)) { // JSLint (not strictly necessary)
                // eval new state
                var item = toUpdate[item_id],
                    was_in_range = (activeItems[item_id] !== undefined),
                    now_in_range = opts.test(_start, _end, item.start, item.end);

                // console.log("u", item.elt, was_in_range, now_in_range);

                if (was_in_range != now_in_range) {
                    if (now_in_range) {
                        activeItems[item_id] = item;
                        _enter(item);
                    } else {
                        delete activeItems[item_id];
                        _exit(item);
                    }
                }
            }
        }
    }

    function debug () {
        var ret = ""
        ret += "RANGER: "+_start+", "+_end+"\n";
        ret += "ITEMSBYSTART:\n";
        for (var i=0; i<itemsByStart.length && i < 10; i++) {
            var item = itemsByStart[i];
            if (ssi == i) { ret += "S"; }
            if (esi == i) { ret += "E"; }
            ret += "["+item.id+ " "+item.start+"-"+item.end+"]\n";
        }
        ret += "ITEMSBYEND:\n";
        for (var i=0; i<itemsByEnd.length && i < 10; i++) {
            var item = itemsByEnd[i];
            if (sei == i) { ret += "S"; }
            if (eei == i) { ret += "E"; }
            ret += "["+item.id+ " "+item.start+"-"+item.end+"]\n";
        }
        var actives = getActive();
        ret += "ACTIVES: " + actives.length;
        console.log(ret);
    }

    function set (start, end) {
        // console.log("ordering.setCurrentValue", v);
        _start = start;
        _end = end;
        updateForValue(start, end);
        if (opts.debug) { debug(); }
    }

    function getActive () {
        var ret = [];
        for (item_id in activeItems) {
            if (activeItems.hasOwnProperty(item_id)) { // JSLint (not strictly necessary)
                ret.push(activeItems[item_id]);
            }
        }
        return ret;
    }
    that.getActive = getActive;

    function each (callback) {
        // Iterate on all INRANGE items and call callback(s, e) on each
        // where s and e are the current range values adjusted by the start of the element ( so 0 = element start )
        // and this is bound to the original element
        var ret = [];
        for (item_id in activeItems) {
            if (activeItems.hasOwnProperty(item_id)) { // JSLint (not strictly necessary)
                var item = activeItems[item_id];
                callback.call(item.elt, _start - item.start, _end - item.start);
            }
        }
        return ret;
    }
    that.each = each;

    that.debug = function (val) {
        opts.debug = val;
    }
    that.set = set;
    that.get = function () { return currentValue; };    
    that.min = function () { return min; }
    that.max = function () { return max; }
    that.dispose = function () {
        // cleanup... call exit on all actives
        // console.log("ranger.dispose");
        for (item_id in activeItems) {
            if (activeItems.hasOwnProperty(item_id)) { // JSLint (not strictly necessary)
                var item = activeItems[item_id];
                _exit(item);
                item.elt = null;
            }
        }
    }
    return that;
}
window.ranger = ranger;
$.ranger = ranger;

})(jQuery);
