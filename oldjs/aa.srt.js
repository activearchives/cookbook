(function ($) {

/* NB: Media Fragments wants ., but SRT uses ,
*/
function timecode (secs, decimal_delimiter) {
    /* 70 => 00:01:10, 70.5 => 00:01:10.500 */
        var h, m, s, r;
        if (decimal_delimiter === undefined) decimal_delimiter = ".";
        h = Math.floor(secs/3600);
        secs -= h*3600;
        m = Math.floor(secs/60);
        secs -= m*60;
        s = Math.floor(secs);
        r = "" + (h<10 ? "0"+h:h)+":"+(m<10 ? "0"+m:m)+":"+(s<10 ? "0"+s:s);
        if (secs - s) {
            // maybe should actually round this?!
            var f = ""+(secs-s),
                fs = f.substr(2, 3);
            while (fs.length < 3) { fs = fs+"0" } 
            r += decimal_delimiter+fs;
        }
        return r;
}
window.timecode = timecode;

var extract_tagnames = function (text) {
    /*  ".... [[word1]] ... [[word2]] ..." ==> ["word1", "word2"] */
        var pat = /\[\[([^\]]+)\]\]/g,
            ret = [],
            r;
        while (r = pat.exec(text)) {
            ret.push(r[1]);
        }
        return ret;
    };

var defaults = {};

function spliturl (url) {
    var m = url.match(/^\w+:\/\//),
        protocol,
        parts;
    if (m !== null) {
        protocol = m[0];
        url = url.substr(protocol.length);
    }
    parts = url.split(/\//g);
    if (protocol) {
        parts.unshift(protocol);
    }
    return parts;
}

function srt (elt, opts) {
    //console.log("srt", elt);
    opts = $.extend({}, defaults, opts);
    var that = {elt: elt},
        $elt = $(elt),
        headwrap = $elt.find(".headwrap"),
        head = $elt.find(".head"),
        menu = $elt.find(".menu"),
        editbutton = $elt.find("button.edit"),
        body = $elt.find(".body"),
        rdf = $.rdf(),
        editing = false,
        srtsource;

    /* create structure when missing */
    if (headwrap.size() == 0) {
        headwrap = $("<div></div>").addClass("headwrap").appendTo(elt); }
    if (head.size() == 0) {
        var displayname;
        if (opts.src) {
            displayname = spliturl(opts.src).join("<br />");
        } else {
            displayname = "new";
        }
        head = $("<div></div>").addClass("head").html(displayname).appendTo(headwrap); }
    if (menu.size() == 0) {
        menu = $("<div></div>").addClass("menu").appendTo(head); }
    if (editbutton.size() == 0) {
        editbutton = $("<button></button>").text("edit").addClass("edit").appendTo(menu); }
    if (body.size() == 0) {
        body = $("<div></div>").addClass("body").appendTo(elt); }

    /* activate */
    $elt
        .addClass("aabox")
        .addClass("aasrtbox")
        .mouseenter(function ( ){
            $(this).addClass("focus");
        }).mouseleave(function () {
            $(this).removeClass("focus");
        })
        .draggable({
            handle: ".head"
        });
    body.autoscrollable();

    editbutton.click(function () {
        var w = body.width(),
            h = body.height();
        if (!editing) {
            editing = true;
            $("<button>cancel</button>").addClass("cancel").appendTo(menu).click(function () {
                editing = false;
                editbutton.text("edit");
                $(this).remove();
                $("textarea", elt).remove();
                body.appendTo(elt);
            })
            body.remove();
            editbutton.text("save");
            $("<textarea></textarea>")
                .val(srtsource)
                .css({width: w, height: h})
                .appendTo(elt);
        } else {
            var textarea = $("textarea", elt),
                text = textarea.val(),
                w = textarea.width(),
                h = textarea.height();

            function endedit () {
                editing = false;
                $("button.cancel", menu).remove();
                editbutton.text("edit");
                $("textarea", elt).remove();
                body = $("<div></div>")
                    .addClass("body")
                    .appendTo(elt)
                    .css({width: w, height: h})
                    .autoscrollable();
                process(text);            
            }
            if (opts.src) {
                var a = document.createElement("a");
                a.setAttribute("href", opts.src);
                var abspath = a.href.replace("http://"+a.host, "")
                // console.log(abspath);

                $.ajax({
                    url: "/cgi-bin/saveas.cgi",
                    data: {'path': abspath, 'data': text},
                    dataType: 'json',
                    success: function (result) {
                        console.log("success", result);
                        endedit();
                    },
                    error: function (result) {
                        console.log("error", result);
                    }
                })
            } else {
                endedit();
            }
        }
    })

    rdf.prefix("aa", "http://activearchives.org/terms/");
    rdf.prefix("dc", "http://purl.org/dc/elements/1.1/")

    function processBody (text) {
        var tagpat = /\[\[([^\]]+)\]\]/g,
            tags = [];
        return {
            tags: tags,
            text: text.replace(tagpat, function(match, g1, index, o) {
                if (g1.match(/\.\w+$/i) !== null) {
                    return '<a href="'+g1+'" class="aafile">'+g1+"</a>";
                } else {
                    tags.push(g1);
                    return '<a href="#" class="aatag">'+g1+"</a>";
                }
            })
        }
    }

    function urlstripextension (url) {
        var a = document.createElement("a");
        a.setAttribute("href", url);
        // console.log("url", url, a)
        return "http://" + a.hostname + a.pathname.replace(/\..*$/, ""); 
    }

    function process (data) {
        /* srt src => dom & rdf */
        // TO DO: SHIFT TO aasrtparse <!>
        var titles = aasrtparse(data),
            i, len, title, target, next_start;

        srtsource = data;

        for (var i=0, len=titles.length; i<len; i++) {
            var title = titles[i],
                next_start = (i+1 < len) ? titles[i+1].start : undefined,
                hash = title.start ? ("#t=" + timecode(title.start) + (next_start ? ","+timecode(next_start):"")) : "",
                // targethref = opts.src.replace(/\.\d+\.srt$/, ""),
                targethref = title.href ? title.href : (opts.src ? urlstripextension(opts.src) : ''), // TODO: TAG Docs / Anonymous... what to target ?!... use opts.target ?
                target = $.rdf.resource("<"+targethref+hash+">"),
                // body = $.rdf.literal(title.content);
                titleid = "t"+i,
                titleurl = opts.src+"#"+titleid,
                titlediv = $("<div></div>")
                    .attr("class", "title")
                    .attr("id", titleid)
                    .attr("data-embed-href", titleurl)
                    .appendTo(body),
                timingdiv = $("<div></div>")
                    .attr("class", "timing");
                titlebody = $("<div></div>");
                // body = $.rdf.literal('"'+title.content.replace(/"/g, "\"")+'"'),
                titlenode = $.rdf.resource("<"+titleurl+">");
            if (title.start !== undefined) {
                timingdiv.appendTo(titlediv);
            }
            titlebody.appendTo(titlediv);
            p = processBody(title.content);
            titlebody.html(p.text);
            tags = p.tags;
            var timinghtml = "";
            if (title.href) {
                timinghtml += title.href + "<br />\n";
            }
            if (title.start !== undefined) {
                timinghtml += timecode(title.start)+" &rarr; "+(title.end?timecode(title.end):"")
            }
            $("<a></a>")
                .attr("href", target.value.toString())
                .addClass("aafragment")
                /*
                .click(function () {
                    $(this).trigger("fragmentclick"); // warning lack of closure on target --> not usable as data here (need to wrap)
                    return false;
                })
                */
                .html(timinghtml)
                .appendTo(timingdiv);

            // titlediv.attr("data-node", titlenode.toString());
            // window.dom[titlenode.toString()] = titlediv.get(0);
            rdf.add(target+" aa:hasTitle "+titlenode+" .");
            if (tags.length) {
                // console.log("tags", tags);
                for (var ti=0, tl = tags.length; ti<tl; ti++) {
                    /* NB Uses GLOBAL rdf for tag lookup -- is this dangerous, have implications for eventual merging ?!
                    Should tags be local URLs ipv bnodes ?!
                    */
                    var tt = window.rdftag_for_name(window.rdf, tags[ti]);
                    // console.log("tt", tt);
                    rdf.add(target+" aa:hasTag "+tt+" .");
                }
            }
        }
    }

    function load (src) {
        $.ajax({
            url: src,
            data: {q: new Date()},
            success: function (data) {
                process(data);
                if (opts.load) {
                    opts.load.call(elt, that);
                }
            },
            error: function (e) {
                throw e;
            }
        })
    }
    that.load = load;

    that.rdf = function () {
        return rdf;
    }

    if (opts.src) {
        load(opts.src);
    } else if (opts.text) {
        process(opts.text);
        if (opts.load) {
            opts.load.call(elt, that);
        }
    }

    return that;
}

function jqwidget (name, fn) {
    var ret = function (opts) {
        return this.each(function () {
            var elt = this,
                obj = $(this).data(name);
            if (obj === undefined) {
                obj = fn.call(elt, elt, opts);
                $(this).data(name, obj);
            } else {
                var method = arguments[0],
                    args = Array.prototype.slice.call(arguments, 1);
                obj.method.apply(elt, args);
            }
        });
    };
    return ret;
}

$.fn.srt = jqwidget("srt", srt);

})(jQuery);