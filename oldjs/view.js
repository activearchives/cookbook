$(document).ready(function () {
    function urlprehash(url) {
        var hp = url.indexOf("#");
        return (hp === -1) ? url : url.substr(0, hp);
    }

    function urlposthash (url) { // nb returns has part of url WITHOUT the leading #
        var hp = url.indexOf("#");
        return (hp !== -1) ? url.substr(hp+1) : url;
    }

    var mediaByURL = {};

    $("a[rel='embed']").playable();
/*
    $("a[rel='embed']").each(function () {
        var href=$(this).attr("href"),
            embedelt;
        if (href.match(/(mp4|ogv|ogg|mp3)$/)) {
            if (href.match(/(ogg|mp3)$/)) {
                embedelt = $("<audio></audio>");
            } else {
                embedelt = $("<video></video>");
            };
            embedelt
                .attr("src", href)
                .attr("controls", "true")
                .bind("timeupdate", function () {
                    $(this).trigger("fragmentupdate", href+"#t="+$(this).get(0).currentTime);
                });

        } else if (href.match(/pdf$/)) {
            embedelt = $("<iframe></iframe>");
            embedelt
                .attr("src", "/lib/lib/pdfjs/web/viewer.html?file="+escape(href))
                .bind("load", function () {
                    console.log("pdf loaded");
                    // console.log(window, window.location);
                    // console.log(window.location.toString());
                    var embed_rawelt = embedelt.get(0),
                        embed_document = embed_rawelt.contentDocument,
                        embed_window = embed_rawelt.contentWindow;
                    $(embed_window).bind("pagechange", function (evt, data) {
                        var pageNumber = $(embed_document).find("#pageNumber").val();
                        console.log("pagechange", pageNumber);
                        embedelt.trigger("fragmentupdate", href+"#page="+pageNumber);
                        // console.log("pagechange", evt.pageNumber);
                    });
                });


        }
        $(this).replaceWith(embedelt);
        mediaByURL[href] = embedelt.get(0);
    });
*/

    // http://stackoverflow.com/questions/2920150/insert-text-at-cursor-in-a-content-editable-div
    function insertTextAtCursor(text) {
        var sel, range, html;
        if (window.getSelection) {
            sel = window.getSelection();
            if (sel.getRangeAt && sel.rangeCount) {
                range = sel.getRangeAt(0);
                range.deleteContents();
                range.insertNode( document.createTextNode(text) );
            }
        } else if (document.selection && document.selection.createRange) {
            document.selection.createRange().text = text;
        }
    }

    function insertLinkAtCursor(href, label) {
        var sel, range;
        if (window.getSelection) {
            sel = window.getSelection();
            if (sel.getRangeAt && sel.rangeCount) {
                range = sel.getRangeAt(0);
                var link = document.createElement("a");
                link.setAttribute("href", href);
                if (range.collapsed) {
                    link.appendChild(document.createTextNode(label));
                    range.insertNode(link);
                } else {
                    range.surroundContents(link);
                }
            }
        }
    }

    $("body").bind("fragmentupdate", function (evt, data) {
        // console.log(evt, data);
        $("#fragment").text(data);
        $("body").data("fragment", data);
    }).bind("fragmentclick", function (evt, data) {
        var baseurl = urlprehash(data),
            hash = urlposthash(data),
            media = mediaByURL[baseurl],
            t = parseFloat(hash.substr(2));
        // console.log("fragmentclick", baseurl, hash, t);
        // next abstract playable... to use fragments

        if (media) {
            media.currentTime = t;
            media.play();
        }
    })

    var aa = {
        button: {}
    };
    window.aa = aa;

    /* Wake up d3 serializations */
    // $("*[data-d3]").each(function () {
    //     this.__data__ = JSON.parse($(this).attr("data-d3"));
    // })

    /* Activate buttons */
    $("#buttons button").each(function () {
        var button = this,
            title = $(this).attr("title");
        if (title && title.match(/\+/)) { // treat as keyboard shortcut
            $(window).bind("keydown", title, function () {
                $(button).trigger("click");
            })
        }
    })

    window.aa.button.save = function () {
        console.log("save");
    };
    window.aa.button.annotate = function () {
        var anno = $("<p></p>")
            .addClass("annotation")
            .appendTo("body")
            .click(function (e) {
                if (!$(this).attr("contenteditable") && e.target.nodeName != "A") {
                    $(this).attr("contenteditable", "true").focus();
                }
            })
            .blur(function () {
                $(this).removeAttr("contenteditable");
            })
            .attr("contenteditable", "true")
            .focus();
    };
    window.aa.button.link = function () {
        var paste = $("body").data("fragment");
        insertLinkAtCursor(paste, urlposthash(paste));
    };

    $(document).on("click", function (e) {
        // console.log(e, e.target);
        if (e.target.nodeName == "A") {
            var href = $(e.target).attr("href");
            $(e.target).trigger("fragmentclick", href);
            // console.log("link", href);
            // $("audio").attr("src", href);
            e.preventDefault();
        }
    });

    /* content editable toggling / styling */

    /* trap the new links (via some kind of jquery live event mapping ?!) */



})