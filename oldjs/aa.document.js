(function () {
/*

things to add: hook to (custom) display of metadata,
poc: button => turtle dump

*/

function absurl (url) {
    var a = document.createElement("a");
    a.setAttribute("href", url);
    return a.href;
}

function widgets_for_url (href) {
    var ret = [];
    // TODO: use rdf / format (mime type) here!
    // TODO: Modularize to allow widgets themselve to register a "sniffer" function to be used here
    if (href.match(/\.srt$/i)) {
        ret.push('srt');
    } else if (href.match(/\.(webm|og[gv]|mp4)$/i)) {
        ret.push('html5media');
    } else if (href.match(/\.(jpe?g|png)$/i)) {
        ret.push('aaimage');
    }
    return ret;
}

// 
// AA generic document widget, proxy for underlying actual widget...
// adds / manages "document" chrome & proxies methods to underlying display widget
//
function aadoc (elt, opts) {
    var $elt = $(elt),
        href = absurl(opts.src),
        that = {
            elt: elt,
            href: href
        },
        node = $.rdf.resource("<"+href+">"),
        widgets = widgets_for_url(href);

    if (widgets.length) {
        $elt[widgets[0]]({
            src: href,
            load: function (obj) {
                that.widget = obj;
                if (opts.load) {
                    opts.load.call(that, that)
                }
                // process_rdf(that.widget.rdf(), hrefabs);
            }
        });
    } else {
        throw "No matching aa widget for " + href;
    }
    $elt
        .attr("data-embed-href", href);
 
    that.rdf = function () {
        return that.widget.rdf();
    }
    that.setfragment = function (f) {
        return that.widget.setfragment(f);
    }

    return that;    
}

function jqwidget (name, fn) {
    var ret = function (opts) {
        return this.each(function () {
            var elt = this,
                obj = $(this).data(name);
            if (obj === undefined) {
                obj = fn.call(elt, elt, opts);
                $(this).data(name, obj);
            } else {
                var method = arguments[0],
                    args = Array.prototype.slice.call(arguments, 1);
                obj.method.apply(elt, args);
            }
        });
    };
    return ret;
}

$.fn.aadoc = jqwidget("aadoc", aadoc);
$.fn.aadoc.absurl = absurl;

})();

