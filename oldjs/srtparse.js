/*
srtparse
Returns [ {start: 737.5, end: undefined, content: "", timecode: "00:00 -->" }]
Eventually may want to make this less permissive again (still permits tc such as 01:00 )
*/
window.srtparse = function (src) {
    var trim = function(){ return this.replace(/^\s+|\s+$/g, ''); },
        spat = /^(?:\d+\n)?((?:\d\d:)?\d\d:\d\d(?:,\d\d\d)? *--> *(?:(?:\d\d:)?\d\d:\d\d(?:,\d\d\d)?)?)$/gm,
        tcpat = /(?:(\d\d):)?(\d\d):(\d\d)(?:,(\d\d\d))? *--> *(?:(?:(\d\d):)?(\d\d):(\d\d)(?:,(\d\d\d))?)?/,
        tt = src.split(spat),
        i = 1,
        l = tt.length,
        timecode,
        content,
        tcs,
        start,
        end,
        ret = [];

    for ( ; i<l; i+=2 ) {
        timecode = tt[i],
        content = tt[i+1],
        tcs = timecode.match(tcpat),
        start = (parseInt(tcs[1]||"0")*3600) + (parseInt(tcs[2])*60) + parseFloat(tcs[3]+"."+(tcs[4]||"0")),
        end = (tcs[6] === undefined) ? undefined : (parseInt(tcs[5])*3600) + (parseInt(tcs[6])*60) + parseFloat(tcs[7]+"."+(tcs[8]||"0"));
        ret.push({start: start, end: end, timecode: timecode, content: trim.call(content)});
    }
    return ret;
};

/*
aasrtparse

Returns [ {content: 'zero section even if blank'},
          {href: 'http://localhost:8000/mymovie.webm', start: 9.5, end: undefined, timecode: '...', content: ''} ]

Permissive multi-target SRT parser
NB: This function's return always includes an initial "zero-section" (even if content is "")
Warning: have made the timecode pattern permissive of both periods AND commas as decimal separators, NOT SRT (which uses ,) compliant
Sampler:

00:09,489 --> 
Ik ben Roel van Driel, ik werk bij de gemeente Enschede en ben daar beleidsadviseur. Ja mijn voornaamste bezigheid is stadsranden ontwikkelen en het lobbyen daaromheen. Het zoeken naar geld en zoeken naar nieuwe ideeën.

00:24,120 --> 
Ja,ik ben ook betrokken bij de groene kennispoort als deelnemer, namens de gemeente Enschede.

1
00:00 --> 
Leonie de Vries, planologisch beleidsmedewerkster bij de gemeente Hengelo. Hou me vooral bezig met de stadsranden, maar ook bijvoorbeeld met de ontwikkelingen in de stad.

2
00:09,489 --> 
Ik ben Roel van Driel, ik werk bij de gemeente Enschede en ben daar beleidsadviseur. Ja mijn voornaamste bezigheid is stadsranden ontwikkelen en het lobbyen daaromheen. Het zoeken naar geld en zoeken naar nieuwe ideeën.

http://localhost:8000/tests/translearning/TwenteLeonieDeVriesRoelVanDries
00:00:09.489 --> 00:01:07.358

http://localhost:8000/tests/translearning/WillemOosterhuis
00:00:00 --> 00:02:33.560

*/
function aasrtparse (src) {
    var trim = function(){ return this.replace(/^\s+|\s+$/g, ''); },
        // spat = /^(?:\d+\n)?((?:\d\d:)?\d\d:\d\d(?:,\d\d\d)? *--> *(?:(?:\d\d:)?\d\d:\d\d(?:,\d\d\d)?)?)$/gm,
        // spat = /^(?:(\d+|http:\/\/.+?)\n)?((?:\d\d:)?\d\d:\d\d(?:,\d\d\d)? *--> *(?:(?:\d\d:)?\d\d:\d\d(?:,\d\d\d)?)?)$/gm,
        spat = /^(?:(\d+|http:\/\/.+?)\n)?((?:\d\d:)?\d\d:\d\d(?:[,\.]\d\d\d)? *--> *(?:(?:\d\d:)?\d\d:\d\d(?:[,\.]\d\d\d)?)?)$/gm,
        tcpat = /(?:(\d\d):)?(\d\d):(\d\d)(?:[,\.](\d\d\d))? *--> *(?:(?:(\d\d):)?(\d\d):(\d\d)(?:[,\.](\d\d\d))?)?/,
        tt = src.split(spat),
        i = 1,
        l = tt.length,
        target,
        timecode,
        content,
        tcs,
        start,
        end,
        ret = [];

    // Include leading "zero" section
    ret.push({content: tt[0]});

    for ( ; i<l; i+=3) {
        target = tt[i];
        timecode = tt[i+1];
        content = tt[i+2];

        tcs = timecode.match(tcpat);
        start = (parseInt(tcs[1]||"0")*3600) + (parseInt(tcs[2])*60) + parseFloat(tcs[3]+"."+(tcs[4]||"0"));
        end = (tcs[6] === undefined) ? undefined : (parseInt(tcs[5])*3600) + (parseInt(tcs[6])*60) + parseFloat(tcs[7]+"."+(tcs[8]||"0"));
        ret.push({href: target, start: start, end: end, timecode: timecode, content: trim.call(content)});
    }
    for (i=0,l=ret.length; i<l; i++) {
        ret[i].nextstart = (i+1 < l) ? ret[i+1].start : undefined;
        ret[i].duration = ret[i].end ? (ret[i].end - ret[i].start) : (ret[i].nextstart ? (ret[i].nextstart - ret[i].start) : undefined);
        ret[i].cend = ret[i].end ? ret[i].end : (ret[i].duration ? (ret[i].start + ret[i].duration) : undefined);
    }
    return ret;
};
window.aasrtparse = aasrtparse;

