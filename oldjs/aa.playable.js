(function () {
    
    /* Playable widget */

    /* function to translate source to annotations (both rdf + html) */

    function playable (elt, opts) {
        var $elt = $(elt);
        $elt.addClass("playable");

    }
 
    var defaults = {
        'target': 'body'
    }

    $.fn.play = function (opts) {
        opts = $.extend({}, defaults, opts);
        return this.each(function () {

            var href = $(this).attr("href");

            /* Is there an existing player to target? */
            var player = $('.aaplayer[data-playable-href="'+href+'"]').get(0);
            if (player) {

            } else {

            }

            player = $("<div></div>")
                    .addClass("aaplayer")
                    .attr("data-playable-href", href),
                embedelt;

            if (href.match(/(mp4|ogv|ogg|mp3)$/)) {
                if (href.match(/(ogg|mp3)$/)) {
                    embedelt = $("<audio></audio>");
                } else {
                    embedelt = $("<video></video>");
                };
                embedelt
                    .attr("src", href)
                    .attr("controls", "true")
                    .bind("timeupdate", function () {
                        $(this).trigger("fragmentupdate", href+"#t="+$(this).get(0).currentTime);
                    });

            } else if (href.match(/pdf$/)) {
                embedelt = $("<iframe></iframe>");
                embedelt
                    .attr("src", "/lib/lib/pdfjs/web/viewer.html?file="+escape(href))
                    .bind("load", function () {
                        console.log("pdf loaded");
                        // console.log(window, window.location);
                        // console.log(window.location.toString());
                        var embed_rawelt = embedelt.get(0),
                            embed_document = embed_rawelt.contentDocument,
                            embed_window = embed_rawelt.contentWindow;
                        $(embed_window).bind("pagechange", function (evt, data) {
                            var pageNumber = $(embed_document).find("#pageNumber").val();
                            console.log("pagechange", pageNumber);
                            embedelt.trigger("fragmentupdate", href+"#page="+pageNumber);
                            // console.log("pagechange", evt.pageNumber);
                        });
                    });
            }
            embedelt.appendTo(player);
            $(opts.target).append(player);

        });
    }

})();