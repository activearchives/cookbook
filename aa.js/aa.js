!function() {
  var aa = {
    version: "1.0.0"
  };
  aa.memoize = aa_memoize;
  function aa_memoize(fn, keyfn) {
    var results = {};
    keyfn = keyfn || function(x) {
      return x.toString();
    };
    var ret = function() {
      var key = keyfn.apply(null, arguments), result = results[key];
      if (result === undefined) {
        result = fn.apply(null, arguments);
        results[key] = result;
      }
      return result;
    };
    ret.items = results;
    return ret;
  }
  "use strict";
  window.MediaFragments = function(window) {
    if (!Array.prototype.forEach) {
      Array.prototype.forEach = function(fun) {
        if (this === void 0 || this === null) {
          throw new TypeError();
        }
        var t = Object(this);
        var len = t.length >>> 0;
        if (typeof fun !== "function") {
          throw new TypeError();
        }
        var thisp = arguments[1];
        for (var i = 0; i < len; i++) {
          if (i in t) {
            fun.call(thisp, t[i], i, t);
          }
        }
      };
    }
    var SEPARATOR = "&";
    var VERBOSE = true;
    var logWarning = function(message) {
      if (VERBOSE) {
        console.log("Media Fragments URI Parsing Warning: " + message);
      }
    };
    var dimensions = {
      t: function(value) {
        var components = value.split(",");
        if (components.length > 2) {
          return false;
        }
        var start = components[0] ? components[0] : "";
        var end = components[1] ? components[1] : "";
        if (start === "" && end === "" || start && !end && value.indexOf(",") !== -1) {
          return false;
        }
        var npt = /^((npt\:)?((\d+\:(\d\d)\:(\d\d))|((\d\d)\:(\d\d))|(\d+))(\.\d*)?)?$/;
        if (npt.test(start) && npt.test(end)) {
          start = start.replace(/^npt\:/, "");
          start = start.replace(/\.$/, "");
          end = end.replace(/\.$/, "");
          var convertToSeconds = function(time) {
            if (time === "") {
              return false;
            }
            var hours;
            var minutes;
            var seconds;
            time = time.split(":");
            var length = time.length;
            if (length === 3) {
              hours = parseInt(time[0], 10);
              minutes = parseInt(time[1], 10);
              seconds = parseFloat(time[2]);
            } else if (length === 2) {
              hours = 0;
              minutes = parseInt(time[0], 10);
              seconds = parseFloat(time[1]);
            } else if (length === 1) {
              hours = 0;
              minutes = 0;
              seconds = parseFloat(time[0]);
            } else {
              return false;
            }
            if (hours > 23) {
              logWarning("Please ensure that hours <= 23.");
              return false;
            }
            if (minutes > 59) {
              logWarning("Please ensure that minutes <= 59.");
              return false;
            }
            if (length > 1 && seconds >= 60) {
              logWarning("Please ensure that seconds < 60.");
              return false;
            }
            return hours * 3600 + minutes * 60 + seconds;
          };
          var startNormalized = convertToSeconds(start);
          var endNormalized = convertToSeconds(end);
          if (start && end) {
            if (startNormalized < endNormalized) {
              return {
                value: value,
                unit: "npt",
                start: start,
                end: end,
                startNormalized: startNormalized,
                endNormalized: endNormalized
              };
            } else {
              logWarning("Please ensure that start < end.");
              return false;
            }
          } else {
            if (convertToSeconds(start) !== false || convertToSeconds(end) !== false) {
              return {
                value: value,
                unit: "npt",
                start: start,
                end: end,
                startNormalized: startNormalized === false ? "" : startNormalized,
                endNormalized: endNormalized === false ? "" : endNormalized
              };
            } else {
              logWarning("Please ensure that start or end are legal.");
              return false;
            }
          }
        }
        var smpte = /^(\d+\:\d\d\:\d\d(\:\d\d(\.\d\d)?)?)?$/;
        var prefix = start.replace(/^(smpte(-25|-30|-30-drop)?).*/, "$1");
        start = start.replace(/^smpte(-25|-30|-30-drop)?\:/, "");
        if (smpte.test(start) && smpte.test(end)) {
          var convertToSecondsWithFrames = function(time) {
            if (time === "") {
              return false;
            }
            var hours;
            var minutes;
            var seconds;
            var frames;
            var subframes;
            time = time.split(":");
            var length = time.length;
            if (length === 3) {
              hours = parseInt(time[0], 10);
              minutes = parseInt(time[1], 10);
              seconds = parseInt(time[2], 10);
              frames = 0;
              subframes = 0;
            } else if (length === 4) {
              hours = parseInt(time[0], 10);
              minutes = parseInt(time[1], 10);
              seconds = parseInt(time[2], 10);
              if (time[3].indexOf(".") === -1) {
                frames = parseInt(time[3], 10);
                subframes = 0;
              } else {
                var frameSubFrame = time[3].split(".");
                frames = parseInt(frameSubFrame[0], 10);
                subframes = parseInt(frameSubFrame[1], 10);
              }
            } else {
              return false;
            }
            if (hours > 23) {
              logWarning("Please ensure that hours <= 23.");
              return false;
            }
            if (minutes > 59) {
              logWarning("Please ensure that minutes <= 59.");
              return false;
            }
            if (seconds > 59) {
              logWarning("Please ensure that seconds <= 59.");
              return false;
            }
            return hours * 3600 + minutes * 60 + seconds + frames * .001 + subframes * 1e-6;
          };
          if (start && end) {
            if (convertToSecondsWithFrames(start) < convertToSecondsWithFrames(end)) {
              return {
                value: value,
                unit: prefix,
                start: start,
                end: end
              };
            } else {
              logWarning("Please ensure that start < end.");
              return false;
            }
          } else {
            if (convertToSecondsWithFrames(start) !== false || convertToSecondsWithFrames(end) !== false) {
              return {
                value: value,
                unit: prefix,
                start: start,
                end: end
              };
            } else {
              logWarning("Please ensure that start or end are legal.");
              return false;
            }
          }
        }
        var wallClock = /^((\d{4})(-(\d{2})(-(\d{2})(T(\d{2})\:(\d{2})(\:(\d{2})(\.(\d+))?)?(Z|(([-\+])(\d{2})\:(\d{2})))?)?)?)?)?$/;
        start = start.replace("clock:", "");
        if (wallClock.test(start) && wallClock.test(end)) {
          if (start && end && !isNaN(Date.parse("2009-07-26T11:19:01Z"))) {
            if (Date.parse(start) <= Date.parse(end)) {
              return {
                value: value,
                unit: "clock",
                start: start,
                end: end
              };
            } else {
              logWarning("Please ensure that start < end.");
              return false;
            }
          } else {
            return {
              value: value,
              unit: "clock",
              start: start,
              end: end
            };
          }
        }
        logWarning("Invalid time dimension.");
        return false;
      },
      xywh: function(value) {
        var pixelCoordinates = /^(pixel\:)?\d+,\d+,\d+,\d+$/;
        var percentSelection = /^percent\:\d+,\d+,\d+,\d+$/;
        var values = value.replace(/(pixel|percent)\:/, "").split(",");
        var x = parseInt(values[0], 10);
        var y = parseInt(values[1], 10);
        var w = parseInt(values[2], 10);
        var h = parseInt(values[3], 10);
        if (pixelCoordinates.test(value)) {
          if (w > 0 && h > 0) {
            return {
              value: value,
              unit: "pixel",
              x: x,
              y: y,
              w: w,
              h: h
            };
          } else {
            logWarning("Please ensure that w > 0 and h > 0");
            return false;
          }
        } else if (percentSelection.test(value)) {
          var checkPercentSelection = function checkPercentSelection(x, y, w, h) {
            if (!(0 <= x && x <= 100)) {
              logWarning("Please ensure that 0 <= x <= 100.");
              return false;
            }
            if (!(0 <= y && y <= 100)) {
              logWarning("Please ensure that 0 <= y <= 100.");
              return false;
            }
            if (!(0 <= w && w <= 100)) {
              logWarning("Please ensure that 0 <= w <= 100.");
              return false;
            }
            if (!(0 <= h && h <= 100)) {
              logWarning("Please ensure that 0 <= h <= 100.");
              return false;
            }
            if (x + w > 100) {
              logWarning("Please ensure that x + w <= 100.");
              return false;
            }
            if (y + h > 100) {
              logWarning("Please ensure that y + h <= 100.");
              return false;
            }
            return true;
          };
          if (checkPercentSelection(x, y, w, h)) {
            return {
              value: value,
              unit: "percent",
              x: x,
              y: y,
              w: w,
              h: h
            };
          }
          logWarning("Invalid percent selection.");
          return false;
        } else {
          logWarning("Invalid spatial dimension.");
          return false;
        }
      },
      track: function(value) {
        return {
          value: value,
          name: value
        };
      },
      id: function(value) {
        return {
          value: value,
          name: value
        };
      },
      chapter: function(value) {
        return {
          value: value,
          chapter: value
        };
      }
    };
    var splitKeyValuePairs = function(octetString) {
      var keyValues = {};
      var keyValuePairs = octetString.split(SEPARATOR);
      keyValuePairs.forEach(function(keyValuePair) {
        var position = keyValuePair.indexOf("=");
        if (position < 1) {
          return;
        }
        var components = [ keyValuePair.substring(0, position), keyValuePair.substring(position + 1) ];
        if (!components[1]) {
          return;
        }
        var key = decodeURIComponent(components[0]);
        var dimensionChecker = dimensions[key];
        var value = decodeURIComponent(components[1]);
        if (dimensionChecker) {
          value = dimensionChecker(value);
        } else {
          return;
        }
        if (!value) {
          return;
        }
        if (!keyValues[key]) {
          keyValues[key] = [];
        }
        if (key !== "t") {
          keyValues[key].push(value);
        } else {
          keyValues[key][0] = value;
        }
      });
      return keyValues;
    };
    return {
      parse: function(opt_uri) {
        return MediaFragments.parseMediaFragmentsUri(opt_uri);
      },
      parseMediaFragmentsUri: function(opt_uri) {
        var uri = opt_uri ? opt_uri : window.location.href;
        var indexOfHash = uri.indexOf("#");
        var indexOfQuestionMark = uri.indexOf("?");
        var end = indexOfHash !== -1 ? indexOfHash : uri.length;
        var query = indexOfQuestionMark !== -1 ? uri.substring(indexOfQuestionMark + 1, end) : "";
        var hash = indexOfHash !== -1 ? uri.substring(indexOfHash + 1) : "";
        var queryValues = splitKeyValuePairs(query);
        var hashValues = splitKeyValuePairs(hash);
        return {
          query: queryValues,
          hash: hashValues,
          toString: function() {
            var buildString = function(name, thing) {
              var s = "\n[" + name + "]:\n";
              if (!Object.keys) Object.keys = function(o) {
                if (o !== Object(o)) {
                  throw new TypeError("Object.keys called on non-object");
                }
                var ret = [], p;
                for (p in o) {
                  if (Object.prototype.hasOwnProperty.call(o, p)) ret.push(p);
                }
                return ret;
              };
              Object.keys(thing).forEach(function(key) {
                s += "  * " + key + ":\n";
                thing[key].forEach(function(value) {
                  s += "    [\n";
                  Object.keys(value).forEach(function(valueKey) {
                    s += "      - " + valueKey + ": " + value[valueKey] + "\n";
                  });
                  s += "   ]\n";
                });
              });
              return s;
            };
            var string = buildString("Query", queryValues) + buildString("Hash", hashValues);
            return string;
          }
        };
      }
    };
  }(window);
  aa.href = aa_href;
  function aa_parse_fragment(fragment) {
    var parts = fragment.split("&"), part, m;
    for (var i = 0, l = parts.length; i < l; i++) {
      part = parts[i];
      m = part.match(/^t=(.+?)(?:,(.+))?$/);
      if (m !== null) {
        this.timeStart = m[1];
        if (m[2]) {
          this.timeEend = m[2];
        }
      }
      m = part.match(/^line=(.+?)(?:,(.+))?$/);
      if (m !== null) {
        this.lineStart = parseInt(m[1]);
        if (m[2]) {
          this.lineEnd = parseInt(m[2]);
        }
      }
    }
  }
  function aa_href(href) {
    var mf = window.MediaFragments.parse(href), that = {
      href: href
    }, hashpos = href.indexOf("#"), base = href, fragment = null;
    if (hashpos >= 0) {
      base = href.substr(0, hashpos);
      fragment = href.substr(hashpos + 1);
      aa_parse_fragment.call(that, fragment);
    }
    that["base"] = base;
    that["nofrag"] = base;
    that["basehref"] = base;
    that["fragment"] = fragment;
    var lsp = base.lastIndexOf("/");
    that["basename"] = lsp !== -1 ? base.substr(lsp + 1) : base;
    if (mf.hash && mf.hash.t && mf.hash.t.length >= 1) {
      that["start"] = mf.hash.t[0].startNormalized;
      that["end"] = mf.hash.t[0].endNormalized;
    }
    return that;
  }
  aa.ranger = ranger;
  aa.extend = aa_extend;
  function aa_extend() {
    for (var i = 1; i < arguments.length; i++) for (var key in arguments[i]) if (arguments[i].hasOwnProperty(key)) arguments[0][key] = arguments[i][key];
    return arguments[0];
  }
  function _insert_sorted(item, propname, arr) {
    var i = 0, len = arr.length;
    for (;i < len; i++) {
      if (arr[i][propname] > item[propname] || arr[i][propname] === undefined && item[propname] !== undefined) {
        arr.splice(i, 0, item);
        return;
      }
    }
    arr.push(item);
  }
  var defaults = {
    copy: function(x) {
      if (x instanceof Date) {
        var ret = new Date();
        ret.setTime(x.getTime());
        return ret;
      } else {
        return x;
      }
    },
    test: function(s1, e1, s2, e2) {
      return s2 < e1 && s1 < e2 || s1 == s2;
    },
    debug: false
  };
  function ranger(opts) {
    opts = aa.extend({}, defaults, opts);
    var that = {}, uid = 0, itemsByStart = [], itemsByEnd = [], min, max;
    that.add = function(elt, start, end) {
      if (start === undefined) {
        throw "ranger.add: no start";
      }
      if (end && end < start) {
        throw "ranger.add: end is before start, ignoring";
      }
      var item = {
        id: "I" + ++uid,
        elt: elt,
        start: start,
        end: end
      };
      if (min === undefined || item.start < min) {
        min = item.start;
      }
      if (max === undefined || item.start > max) {
        max = item.start;
      }
      if (max === undefined || item.end && item.end > max) {
        max = item.end;
      }
      _insert_sorted(item, "start", itemsByStart);
      _insert_sorted(item, "end", itemsByEnd);
      return that;
    };
    that.min = function() {
      return min;
    };
    that.max = function() {
      return max;
    };
    that.unparse = function() {
      var ret = "<ranger " + itemsByStart.length + " items [" + min + " - " + max + "]>\n";
      ret += "itemsByStart:\n";
      for (var i = 0; i < itemsByStart.length && i < 10; i++) {
        var item = itemsByStart[i];
        ret += "[" + item.id + " " + item.start + "-" + item.end + "]\n";
      }
      ret += "itemsByEnd:\n";
      for (var i = 0; i < itemsByEnd.length && i < 10; i++) {
        var item = itemsByEnd[i];
        ret += "[" + item.id + " " + item.start + "-" + item.end + "]\n";
      }
      return ret;
    };
    that.each = function(callback) {
      var i, len;
      for (i = 0, len = itemsByStart.length; i < len; i++) {
        callback.call(itemsByStart[i]);
      }
      return that;
    };
    that.get = function(index) {
      var ret = itemsByStart.slice(0);
      return index !== undefined ? ret[index] : ret;
    };
    that.data = function() {
      return itemsByStart.slice(0);
    };
    that.from = function(start, end) {
      var that = {}, ssi = -1, sei = -1, esi = -1, eei = -1, _start, _end, lastStart, lastEnd, activeItems = {}, enterItems = {}, exitItems = {};
      function updateForValue(start, end) {
        if (itemsByStart.length === 0) {
          return;
        }
        var toUpdate = {}, n;
        function mark(item) {
          toUpdate[item.id] = item;
        }
        if (lastStart == undefined || start >= lastStart) {
          n = itemsByStart.length;
          while (ssi + 1 < n && start >= itemsByStart[ssi + 1].start) {
            ssi++;
            if (ssi < n) {
              mark(itemsByStart[ssi]);
            }
          }
          n = itemsByEnd.length;
          while (sei + 1 < n && start >= itemsByEnd[sei + 1].end) {
            sei++;
            if (sei < n) {
              mark(itemsByEnd[sei]);
            }
          }
        } else {
          while (ssi >= 0 && start < itemsByStart[ssi].start) {
            mark(itemsByStart[ssi]);
            ssi--;
          }
          while (sei >= 0 && start < itemsByEnd[sei].end) {
            mark(itemsByEnd[sei]);
            sei--;
          }
        }
        lastStart = opts.copy(start);
        if (lastEnd == undefined || end >= lastEnd) {
          n = itemsByStart.length;
          while (esi + 1 < n && end > itemsByStart[esi + 1].start) {
            esi++;
            if (esi < n) {
              mark(itemsByStart[esi]);
            }
          }
          n = itemsByEnd.length;
          while (eei + 1 < n && end >= itemsByEnd[eei + 1].end) {
            eei++;
            if (eei < n) {
              mark(itemsByEnd[eei]);
            }
          }
        } else {
          while (esi >= 0 && end < itemsByStart[esi].start) {
            mark(itemsByStart[esi]);
            esi--;
          }
          while (eei >= 0 && end <= itemsByEnd[eei].end) {
            mark(itemsByEnd[eei]);
            eei--;
          }
        }
        lastEnd = opts.copy(end);
        var item_id;
        for (item_id in toUpdate) {
          if (toUpdate.hasOwnProperty(item_id)) {
            var item = toUpdate[item_id], was_in_range = activeItems[item_id] !== undefined, now_in_range = opts.test(_start, _end, item.start, item.end);
            if (was_in_range != now_in_range) {
              if (now_in_range) {
                activeItems[item_id] = item;
                enterItems[item_id] = item;
              } else {
                delete activeItems[item_id];
                exitItems[item_id] = item;
              }
            }
          }
        }
      }
      that.slide = function(start, end) {
        _start = start;
        _end = end;
        enterItems = {};
        exitItems = {};
        updateForValue(start, end);
        return that;
      };
      function values(set) {
        var item_id, ret = [];
        for (item_id in set) {
          if (set.hasOwnProperty(item_id)) {
            ret.push(set[item_id]);
          }
        }
        return ret;
      }
      function each(callback, set) {
        var item_id;
        for (item_id in set) {
          if (set.hasOwnProperty(item_id)) {
            var item = set[item_id];
            callback.call(item.elt, item.start, item.end, _start, _end);
          }
        }
      }
      that.get = function(index) {
        var ret = values(activeItems);
        return index !== undefined ? ret[index] : ret;
      };
      that.each = function(callback) {
        each(callback, activeItems);
        return that;
      };
      that.enter = function(callback) {
        var that = {}, items = enterItems;
        that.each = function(callback) {
          each(callback, items);
          return that;
        };
        that.get = function(index) {
          var ret = values(items);
          return index !== undefined ? ret[index] : ret;
        };
        if (callback) {
          each(callback, items);
        }
        return that;
      };
      that.exit = function(callback) {
        var that = {}, items = exitItems;
        that.each = function(callback) {
          each(callback, items);
          return that;
        };
        that.get = function(index) {
          var ret = values(items);
          return index !== undefined ? ret[index] : ret;
        };
        if (callback) {
          each(callback, items);
        }
        return that;
      };
      that.unparse = function() {
        var ret = "<ranger " + itemsByStart.length + " items [" + min + " - " + max + "]>\n";
        ret += "itemsByStart:\n";
        for (var i = 0; i < itemsByStart.length && i < 10; i++) {
          var item = itemsByStart[i];
          if (ssi == i) {
            ret += "S";
          }
          if (esi == i) {
            ret += "E";
          }
          ret += "[" + item.id + " " + item.start + "-" + item.end + "]\n";
        }
        ret += "itemsByEnd:\n";
        for (var i = 0; i < itemsByEnd.length && i < 10; i++) {
          var item = itemsByEnd[i];
          if (sei == i) {
            ret += "S";
          }
          if (eei == i) {
            ret += "E";
          }
          ret += "[" + item.id + " " + item.start + "-" + item.end + "]\n";
        }
        var actives = that.get();
        ret += "ACTIVES: " + actives.length;
        return ret;
      };
      _start = start;
      _end = end;
      updateForValue(start, end);
      return that;
    };
    that.slider = that.from(-1, -1);
    return that;
  }
  aa.srtparse = aa_srtparse;
  aa.aasrtparse = aa_aasrtparse;
  aa.aasrtparse_normalize = aa_aasrtparse_normalize;
  function aa_srtparse(src) {
    var trim = function() {
      return this.replace(/^\s+|\s+$/g, "");
    }, spat = /^(?:\d+\n)?((?:\d\d:)?\d\d:\d\d(?:[,\.]\d\d\d)? *--> *(?:(?:\d\d:)?\d\d:\d\d(?:[,\.]\d\d\d)?)?)$/gm, tcpat = /(?:(\d\d):)?(\d\d):(\d\d)(?:[,\.](\d\d\d))? *--> *(?:(?:(\d\d):)?(\d\d):(\d\d)(?:[,\.](\d\d\d))?)?/, tt = src.split(spat), i = 1, l = tt.length, timecode, content, tcs, start, end, ret = [];
    for (;i < l; i += 2) {
      timecode = tt[i], content = tt[i + 1], tcs = timecode.match(tcpat), start = parseInt(tcs[1] || "0") * 3600 + parseInt(tcs[2]) * 60 + parseFloat(tcs[3] + "." + (tcs[4] || "0")), 
      end = tcs[6] === undefined ? undefined : parseInt(tcs[5]) * 3600 + parseInt(tcs[6]) * 60 + parseFloat(tcs[7] + "." + (tcs[8] || "0"));
      ret.push({
        start: start,
        end: end,
        timecode: timecode,
        content: trim.call(content)
      });
    }
    return ret;
  }
  var AASRT_URL_PAT = /^(https?:\/\/.+?)$/m, AASRT_TC_PAT = /^(?:(\d\d):)?(\d\d):(\d\d)(?:[,\.](\d{1,3}))? *--> *(?:(?:(\d\d):)?(\d\d):(\d\d)(?:[,\.](\d{1,3}))?)?$/m;
  function parse_tc(tc, ret) {
    if (tc !== null) {
      ret.start = parseInt(tc[1] || 0) * 3600 + parseInt(tc[2]) * 60 + parseInt(tc[3]);
      if (tc[4] !== "") {
        ret.start = ret.start + parseFloat("0." + tc[4]);
      }
      if (tc[6] !== "") {
        ret.end = parseInt(tc[5] || 0) * 3600 + parseInt(tc[6]) * 60 + parseInt(tc[7]);
        if (tc[8] !== "") {
          ret.end = ret.end + parseFloat("0." + tc[8]);
        }
      }
    }
    return ret;
  }
  function aa_aasrtparse(src, normalize) {
    var url = null, tc = null, content = null, content_linestart = null, prev = null, curline = 0, lines = src.split(/^/m), i, len, line, m, ret = [];
    for (i = 0, len = lines.length; i < len; i++) {
      line = lines[i];
      curline += 1;
      m = line.match(AASRT_URL_PAT);
      if (m !== null) {
        if (content !== null) {
          ret.push(parse_tc(tc, {
            href: url,
            content: content,
            lineStart: content_linestart,
            lineEnd: curline - 1
          }));
        }
        url = m[1];
        tc = null;
        content_linestart = curline;
        content = "";
        prev = "URL";
      } else {
        m = line.match(AASRT_TC_PAT);
        if (m !== null) {
          if (prev !== "URL") {
            if (content !== null) {
              ret.push(parse_tc(tc, {
                href: url,
                content: content,
                lineStart: content_linestart,
                lineEnd: curline - 1
              }));
            }
            content_linestart = curline;
            content = "";
          }
          tc = m;
          prev = "TC";
        } else {
          if (content == null) {
            content_linestart = curline;
            content = line;
          } else {
            content += line;
          }
          prev = "TEXT";
        }
      }
    }
    if (content !== null) {
      ret.push(parse_tc(tc, {
        href: url,
        content: content,
        lineStart: content_linestart,
        lineEnd: curline
      }));
    }
    if (normalize) {
      aa_aasrtparse_normalize(ret);
    }
    return ret;
  }
  function aa_aasrtparse_normalize(titles) {
    var i = 0, len = titles.length;
    for (i = 0; i < len - 1; i++) {
      titles[i].end = titles[i].end || titles[i + 1].start;
      titles[i + 1].href = titles[i + 1].href || titles[i].href;
    }
    return titles;
  }
  aa.rawdrag = aa_rawdrag;
  aa.drag = aa_drag;
  function aa_rawdrag(elt) {
    var dragging = false, that = {}, callbacks = {}, pos = {};
    that.on = function(evt, callback) {
      callbacks[evt] = callback;
      return that;
    };
    function dragstart(evt) {
      elt.addEventListener("mousemove", mousemove, false);
      elt.addEventListener("mouseup", mouseup, false);
      elt.addEventListener("mouseleave", mouseup, false);
      if (callbacks.dragstart !== undefined) {
        pos.clientX = evt.clientX;
        pos.clientY = evt.clientY;
        pos.startClientX = evt.clientX;
        pos.startClientY = evt.clientY;
        pos.dx = 0;
        pos.dy = 0;
        callbacks.dragstart.call(elt, pos, evt);
      }
    }
    function drag(evt) {
      if (callbacks.drag !== undefined) {
        pos.clientX = evt.clientX;
        pos.clientY = evt.clientY;
        pos.dx = pos.clientX - pos.startClientX;
        pos.dy = pos.clientY - pos.startClientY;
        callbacks.drag.call(elt, pos, evt);
      }
    }
    function dragend(evt) {
      elt.removeEventListener("mousemove", mousemove, false);
      elt.removeEventListener("mouseup", mouseup, false);
      elt.removeEventListener("mouseleave", mouseup, false);
      if (callbacks.dragend !== undefined) {
        callbacks.dragend.call(elt, pos, evt);
      }
    }
    function mousemove(evt) {
      if (dragging) {
        drag(evt);
      }
    }
    function mouseup(evt) {
      if (dragging) {
        dragging = false;
        dragend(evt);
      }
    }
    elt.addEventListener("mousedown", function(evt) {
      if (!dragging && evt.buttons & 1) {
        dragging = true;
        dragstart(evt);
      }
    });
    return that;
  }
  function aa_drag(elt, handleSelector) {
    var start = {}, handleElt = elt.querySelector(handleSelector), drag = aa_rawdrag(handleElt).on("dragstart", function(p, e) {
      start.left = parseInt(elt.offsetLeft);
      start.top = parseInt(elt.offsetTop);
    }).on("drag", function(p, e) {
      elt.style.left = start.left + p.dx + "px";
      elt.style.top = start.top + p.dy + "px";
    });
  }
  aa.secondsToTimecode = aa_secondsToTimecode;
  aa.timecodeToSeconds = aa_timecodeToSeconds;
  function aa_secondsToTimecode(ss, style) {
    style = timecode_styles[style || "html5"];
    var h = Math.floor(ss / 3600), m, s, fract, ret;
    ss -= h * 3600;
    m = Math.floor(ss / 60);
    ss -= m * 60;
    s = Math.floor(ss);
    fract = style.delimiter + ("" + (ss - s)).substr(2, 3);
    while (fract.length < 4) {
      fract += "0";
    }
    ret = h || style.requireHours ? (h < 10 ? "0" : "") + h + ":" : "";
    ret += (m < 10 ? "0" : "") + m + ":" + ((s < 10 ? "0" : "") + s) + fract;
    return ret;
  }
  timecode_styles = {
    srt: {
      requireHours: true,
      delimiter: ","
    },
    html5: {
      requireHours: false,
      delimiter: "."
    }
  };
  function aa_timecodeToSeconds(tc) {
    var tcpat = /^(?:(\d\d):)?(\d\d):(\d\d(?:[\.,](?:\d{1,3}))?)$/, groups = tcpat.exec(tc);
    if (groups != null) {
      var h = groups[1] !== undefined ? parseInt(groups[1]) : 0, m = parseInt(groups[2]), s = parseFloat(groups[3].replace(/,/, "."));
      return h * 3600 + m * 60 + s;
    }
  }
  aa.annotations = aa_annotations;
  aa.timeRangeHref = aa_timeRangeHref;
  aa.lineRangeHref = aa_lineRangeHref;
  function aa_timeRangeHref(href, start, end) {
    var ret = href;
    if (start) {
      ret += "#t=" + aa.secondsToTimecode(start, "html5");
    }
    if (end) {
      ret += "," + aa.secondsToTimecode(end, "html5");
    }
    return ret;
  }
  function aa_lineRangeHref(href, start, end) {
    var ret = href;
    if (start) {
      ret += "#line=" + start;
    }
    if (end) {
      ret += "," + end;
    }
    return ret;
  }
  function aa_annotations() {
    var that = {}, rangerForHref = aa.memoize(function(href) {
      return aa.ranger();
    });
    that.add = function(target, body) {
      var target = aa.href(target), body = aa.href(body), ranger = rangerForHref(target.basehref);
      ranger.add(body, target.start, target.end);
    };
    that.select_target = function(target) {
      var target = aa.href(target);
      ranger = rangerForHref(target.basehref);
      var slide = ranger.slider.slide(target.start, target.end);
      return slide;
    };
    return that;
  }
  aa.frames = aa_frames;
  function aa_frames(elt) {
    var units = "px";
    $(elt).find(".hdiv").draggable({
      axis: "x",
      drag: function(evt, ui) {
        var c = $(evt.target.parentNode), cw = c.width(), l, r;
        if (units == "px") {
          l = ui.position.left;
          r = cw - l;
        } else {
          l = ui.position.left / cw * 100;
          r = 100 - l;
        }
        c.find(".left").css("right", r + units);
        c.find(".right").css("left", l + units);
        c.find(".left,.right").trigger("resize");
      },
      stop: function(evt) {
        var c = $(evt.target.parentNode);
        c.find(".left,.right").trigger("resize");
      },
      iframeFix: true
    }).click(function(evt) {
      var c = $(this.parentNode);
      cw = c.width(), l, r, left = c.find(".left"), right = c.find(".right");
      if (units == "px") {
        l = $(this).position().left;
        r = cw - l;
      } else {
        l = $(this).position().left / cw * 100;
        r = 100 - l;
      }
      left.removeClass("left").addClass("right").css("right", "");
      right.removeClass("right").addClass("left").css("left", "");
      c.find(".left").css("right", r + units);
      c.find(".right").css("left", l + units);
      c.find(".left,.right").trigger("resize");
    }).append('<div class="divcontents"></div>');
    $(elt).find(".vdiv").draggable({
      axis: "y",
      drag: function(evt, ui) {
        var c = $(evt.target.parentNode), ch = c.height(), t = ui.position.top, b = ch - t;
        c.find(".top").css("bottom", b + "px");
        c.find(".bottom").css("top", t + "px");
        c.find(".top,.bottom").trigger("resize");
      },
      stop: function(evt) {
        var c = $(evt.target.parentNode);
        c.find(".top,.bottom").trigger("resize");
      },
      iframeFix: true
    }).click(function(evt) {
      var c = $(this.parentNode), ch = c.height(), t = $(this).position().top, b = ch - t, top = c.find(".top"), bottom = c.find(".bottom");
      top.removeClass("top").addClass("bottom").css("bottom", "");
      bottom.removeClass("bottom").addClass("top").css("top", "");
      c.find(".top").css("bottom", b + "px");
      c.find(".bottom").css("top", t + "px");
      c.find(".top,.bottom").trigger("resize");
    }).append('<div class="divcontents"></div>');
  }
  aa.tabs = aa_tabs;
  function aa_tabs(elt) {
    var that = {
      elt: elt
    }, tabsbody = $("<div></div>").addClass("tabsbody").appendTo(elt), newTab = $("<div><div class='tabbody'>+</div></div>").addClass("tab").addClass("newtab").appendTo(elt);
    $(tabsbody).sortable({
      axis: "x"
    });
    newTab.click(function() {
      $(elt).trigger("tabnew");
    });
    that.activate = function(data) {
      var tabelt;
      $(".tab", elt).each(function() {
        var d = $(this).data("tab");
        if (data == d) {
          tabelt = this;
        }
      });
      if (tabelt) {
        clickTab.call(tabelt, data);
      }
    };
    function clickTab(data) {
      $(".tab", elt).removeClass("active");
      $(this).addClass("active");
      $(elt).trigger("tabchanged", data);
    }
    that.new = function(data, activate, activate_event) {
      var $tabelt = $("<div></div>"), tabelt = $tabelt.get(0);
      content = $("<div></div>").addClass("tabbody").html(data.label || data.href || "&hellip;").appendTo($tabelt);
      $tabelt.addClass("tab").data("tab", data).appendTo(tabsbody).click(function(evt) {
        clickTab.call(tabelt, $.extend({}, data, {
          originalEvent: evt,
          click: true
        }));
      });
      if (activate) {
        clickTab.call(tabelt, $.extend({}, data, {
          originalEvent: activate_event,
          click: false
        }));
      }
      resizeTabs();
    };
    function resizeTabs() {
      var aw = $(elt).width(), cw = $(tabsbody).width(), nw = $(newTab).width(), totalwidth = 0, tabs = $(".tab", tabsbody);
      tabs.each(function() {
        $(this).css("maxWidth", "");
        var tw = $(this).width();
        totalwidth += tw;
      });
      if (totalwidth > aw) {
        maxWidth = (aw - nw) / tabs.size();
        tabs.each(function() {
          $(this).css("maxWidth", maxWidth + "px");
        });
      }
    }
    that.resize = resizeTabs;
    return that;
  }
  aa.editor = {};
  aa.editor.iframe = aa_iframe_editor;
  function aa_iframe_editor() {
    var factory = {
      name: "iframe"
    }, sessionsByHref = {};
    factory.get_session = function(href) {
      return sessionsByHref[href];
    };
    factory.get_editor = function() {
      var editor = {
        factory: factory
      };
      editor.elt = document.createElement("div");
      $(editor.elt).addClass("iframe");
      function set_contents(i) {
        if ($(i).is(":visible")) return;
        $("iframe", editor.elt).hide();
        $(i).show();
      }
      editor.href = function(href, done) {
        var href = aa.href(href), session = sessionsByHref[href.nofrag];
        if (session == "loading") {
          return false;
        }
        if (session != undefined) {
          if (done) {
            window.setTimeout(function() {
              done.call(session);
            }, 0);
          }
          session.iframe.src = href.href;
          set_contents(session.iframe);
          return true;
        }
        sessionsByHref[href] = "loading";
        session = {
          href: href.nofrag,
          editor: editor
        };
        session.iframe = document.createElement("iframe");
        $(editor.elt).append(session.iframe);
        $(session.iframe).hide();
        session.iframe.addEventListener("load", function() {
          var iframe = this;
          d3.select(iframe.contentDocument).on("click.aaeditor", function() {
            var t = d3.event.target;
            if (t.nodeName == "A") {
              var href = d3.select(t).attr("href");
              if (href) {
                d3.event.preventDefault();
                $(editor.elt).trigger("frameclick", {
                  originalEvent: d3.event,
                  originalTarget: d3.event.target
                });
              }
            }
          });
        }, false);
        sessionsByHref[href.nofrag] = session;
        if (done) {
          window.setTimeout(function() {
            done.call(session);
          }, 0);
        }
        set_contents(session.iframe);
        session.iframe.src = href.href;
      };
      return editor;
    };
    return factory;
  }
  aa.editor.ace = aa_aceeditor;
  function aa_aceeditor(ace) {
    ace = ace || window.ace;
    var factory = {
      name: "aceeditor"
    }, sessionsByHref = {}, modelist = ace.require("ace/ext/modelist"), TokenClicks = ace.require("kitchen-sink/token_clicks").TokenClicks;
    factory.get_session = function(href) {
      return sessionsByHref[href];
    };
    factory.get_editor = function() {
      var editor = {
        factory: factory
      }, tokenclicks, current_href, highlightLineStart, highlightLineEnd;
      editor.elt = document.createElement("div");
      var root = d3.select(editor.elt).attr("class", "editor"), body = root.append("div").attr("class", "editorbody"), foot = root.append("div").attr("class", "editorfoot");
      editor.aceeditor = ace.edit(body[0][0]);
      tokenclicks = new TokenClicks(editor.aceeditor, {
        tokenChange: function(token) {
          if (token != null && (token.type == "heading.timecode.start" || token.type == "heading.timecode.end")) {
            return true;
          }
          if (token != null && token.type == "markup.underline") {
            return true;
          }
        }
      });
      tokenclicks.on("tokenclick", function(token) {
        if (token.type == "heading.timecode.start" || token.type == "heading.timecode.end") {
          var t = aa.timecodeToSeconds(token.value);
          if (t) {
            var href = current_href.replace(/\.srt$/, "");
            $(editor.elt).trigger("fragmentclick", {
              href: href + "#t=" + aa.secondsToTimecode(t)
            });
          } else {
            console.log("bad timecode");
          }
        } else if (token.type == "markup.underline") {
          right.open(token.value);
        }
      });
      $(document).bind("resize", function(e) {
        editor.aceeditor.resize();
      });
      var modeselector = foot.append("select").attr("class", "editormode");
      modeselector.on("change", function() {
        editor.aceeditor.getSession().setMode(this.value);
      }).selectAll("option").data(modelist.modes).enter().append("option").attr("value", function(d) {
        return d.mode;
      }).text(function(d) {
        return d.caption;
      });
      var save = foot.append("button").text("save").on("click", function() {
        var text = editor.aceeditor.getValue();
        $.ajax({
          url: "/aa/cgi-bin/saveas.cgi",
          data: {
            path: current_href,
            data: text
          },
          method: "post",
          dataType: "json",
          success: function(response) {
            console.log("saved", response);
          },
          error: function(response) {
            console.log("error", response);
          }
        });
      });
      function highlight(s, e) {
        var session = editor.aceeditor.getSession();
        if (highlightLineStart) {
          for (var i = highlightLineStart - 1; i <= highlightLineEnd - 1; i++) {
            session.removeGutterDecoration(i, "ace_gutter_active_annotation");
          }
        }
        highlightLineStart = s;
        highlightLineEnd = e;
        if (highlightLineStart) {
          for (var i = highlightLineStart - 1; i <= highlightLineEnd - 1; i++) {
            session.addGutterDecoration(i, "ace_gutter_active_annotation");
          }
          editor.aceeditor.scrollToLine(highlightLineStart, true, true);
        }
      }
      editor.href = function(href, done) {
        if (arguments.length == 0) {
          var ret = current_href;
          if (highlightLineStart) {
            ret = aa.lineRangeHref(current_href, highlightLineStart, highlightLineEnd);
          }
          return ret;
        }
        var href = aa.href(href), session = sessionsByHref[href.nofrag];
        current_href = href.nofrag;
        if (session == "loading") {
          return false;
        }
        if (session != undefined) {
          if (done) {
            window.setTimeout(function() {
              done.call(session);
            }, 0);
          }
          editor.aceeditor.setSession(session.acesession);
          if (href.lineStart) {
            highlight(href.lineStart, href.lineEnd);
            $(editor.elt).trigger("fragmentupdate", {
              editor: editor
            });
          }
          return true;
        }
        sessionsByHref[href.nofrag] = "loading";
        $.ajax({
          url: href.nofrag,
          data: {
            f: new Date().getTime()
          },
          success: function(data) {
            var mode = modelist.getModeForPath(href.nofrag).mode || "ace/mode/text", session = {
              href: href.nofrag,
              editor: editor
            };
            session.acesession = ace.createEditSession(data, mode);
            index(href.nofrag, data);
            modeselector[0][0].value = mode;
            editor.aceeditor.setSession(session.acesession);
            editor.aceeditor.setHighlightActiveLine(false);
            session.acesession.setUseWrapMode(true);
            sessionsByHref[href.nofrag] = session;
            if (done) {
              window.setTimeout(function() {
                done.call(session);
              }, 0);
            }
          },
          error: function(code) {
            console.log("aceeditor: error loading", href.nofrag, code);
          }
        });
      };
      var observed_href = null;
      $(document).on("fragmentupdate", function(e, data) {
        if (e.target !== editor.elt && data.editor) {
          observed_href = data.editor.href();
        }
      });
      editor.newSession = function() {
        var session = ace.createEditSession("", "ace/mode/srt-md");
        editor.aceeditor.setSession(session);
      };
      function bind_keys(e) {
        e.commands.addCommand({
          name: "pasteTimecode",
          bindKey: {
            win: "ctrl-shift-down",
            mac: "command-shift-down"
          },
          exec: function() {
            if (observed_href) {
              var href = aa.href(observed_href), mode = editor.aceeditor.getSession().getMode().$id, link;
              if (mode == "ace/mode/srtmd") {
                link = aa.secondsToTimecode(href.start) + " -->\n";
              } else if (mode == "ace/mode/markdown") {
                link = "[" + href.basename + "](" + href.href + ")";
              } else {
                link = href.href;
              }
              editor.aceeditor.insert(link);
            }
          },
          readOnly: false
        });
        e.commands.addCommand({
          name: "toggleMedia",
          bindKey: {
            win: "ctrl-shift-up",
            mac: "command-shift-up"
          },
          exec: function() {
            $(editor.elt).trigger("editortoggle", {
              editor: editor
            });
          },
          readOnly: true
        });
        e.commands.addCommand({
          name: "jumpMediaBack",
          bindKey: {
            win: "ctrl-shift-left",
            mac: "command-shift-left"
          },
          exec: function() {
            $(editor.elt).trigger("editorback", {
              editor: editor
            });
          },
          readOnly: true
        });
        e.commands.addCommand({
          name: "jumpMediaForward",
          bindKey: {
            win: "ctrl-shift-right",
            mac: "command-shift-right"
          },
          exec: function() {
            $(editor.elt).trigger("editorforward", {
              editor: editor
            });
          },
          readOnly: true
        });
        e.commands.addCommand({
          name: "newSession",
          bindKey: {
            win: "ctrl-n",
            mac: "command-n"
          },
          exec: function() {
            editor.newSession();
          },
          readOnly: true
        });
      }
      bind_keys(editor.aceeditor);
      return editor;
    };
    return factory;
  }
  aa.editor.html5video = aa_html5video_editor;
  function aa_html5video_editor() {
    var factory = {
      name: "html5video"
    }, sessionsByHref = {};
    factory.get_session = function(href) {
      return sessionsByHref[href];
    };
    factory.get_editor = function() {
      var editor = {
        factory: factory
      }, media, current_href_nofrag;
      editor.elt = document.createElement("div");
      $(editor.elt).addClass("html5video");
      function set_contents(m) {
        if ($(m).is(":visible")) {
          return;
        }
        $("video,audio", editor.elt).hide();
        media = m;
        $(m).show();
      }
      editor.href = function(href, done) {
        if (arguments.length == 0) {
          return aa.timeRangeHref(current_href_nofrag, media.currentTime);
        }
        var href = aa.href(href), session = sessionsByHref[href.nofrag];
        current_href_nofrag = href.nofrag;
        if (session == "loading") {
          return false;
        }
        if (session != undefined) {
          if (done) {
            window.setTimeout(function() {
              done.call(session);
            }, 0);
          }
          if (href.start) {
            session.media.currentTime = href.start;
            session.media.play();
          }
          set_contents(session.media);
          return true;
        }
        sessionsByHref[href] = "loading";
        session = {
          href: href.nofrag,
          editor: editor
        };
        session.media = document.createElement("video");
        session.media.setAttribute("controls", "");
        $(editor.elt).append(session.media);
        $(session.media).hide();
        session.media.addEventListener("timeupdate", function(e) {
          var media = this;
          $(editor.elt).trigger("fragmentupdate", {
            editor: editor,
            originalEvent: e,
            originalTarget: e.target
          });
        }, false);
        sessionsByHref[href.nofrag] = session;
        if (done) {
          window.setTimeout(function() {
            done.call(session);
          }, 0);
        }
        set_contents(session.media);
        session.media.src = href.href;
      };
      editor.toggle = function() {
        if (media) {
          media.paused ? media.play() : media.pause();
        }
        return that;
      };
      editor.play = function() {
        if (media) {
          media.play();
        }
        return that;
      };
      editor.pause = function() {
        if (media) {
          media.pause();
        }
        return that;
      };
      editor.jumpBack = function() {
        if (media) {
          media.currentTime = media.currentTime - 5;
        }
        return that;
      };
      editor.jumpForward = function() {
        if (media) {
          media.currentTime = media.currentTime + 5;
        }
        return that;
      };
      editor.currentTime = function(t) {
        if (arguments.length == 0) {
          if (media) {
            return media.currentTime;
          }
        } else {
          if (media) {
            media.currentTime = t;
          }
          return that;
        }
      };
      return editor;
    };
    return factory;
  }
  aa.playable = playable;
  function playable(d) {
    var that = {}, currentTime = 0, elt = null;
    that.d = d;
    that.currentTime = function(t) {
      if (t === undefined) {
        return currentTime;
      } else {
        currentTime = t;
        if (elt && elt.duration && elt.currentTime !== undefined) {
          elt.currentTime = t;
        }
        return that;
      }
    };
    that.exit = function() {
      if (elt !== null) {
        $(elt).hide();
      }
    };
    that.enter = function() {
      if (elt !== null) {
        $(elt).show();
        return elt;
      }
      if (d.format == "audio/ogg") {
        elt = document.createElement("audio");
        elt.setAttribute("controls", "controls");
        elt.setAttribute("src", d["@id"]);
      } else if (d.format == "video/webm") {
        elt = document.createElement("video");
        elt.setAttribute("controls", "controls");
        elt.setAttribute("src", d["@id"] + ".mp4");
      } else if (d.format == "image/jpeg") {
        elt = document.createElement("img");
        elt.setAttribute("src", d["@id"] + ".320x.jpg");
      }
      return elt;
    };
    return that;
  }
  if (typeof define === "function" && define.amd) define(aa); else if (typeof module === "object" && module.exports) module.exports = aa;
  this.aa = aa;
}();