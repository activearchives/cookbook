aa.srtparse = aa_srtparse;
aa.aasrtparse = aa_aasrtparse;
aa.aasrtparse_normalize = aa_aasrtparse_normalize;

/*
Vanilla srtparsing, a bit relaxed, allowing MM:SS and MM:SS[.,]123

Returns [ {start: 737.5, end: undefined, content: "", timecode: "00:00 -->" }]
Eventually may want to make this less permissive again (still permits tc such as 01:00 )

*/
function aa_srtparse (src) {
    var trim = function(){ return this.replace(/^\s+|\s+$/g, ''); },
        spat = /^(?:\d+\n)?((?:\d\d:)?\d\d:\d\d(?:[,\.]\d\d\d)? *--> *(?:(?:\d\d:)?\d\d:\d\d(?:[,\.]\d\d\d)?)?)$/gm,
        tcpat = /(?:(\d\d):)?(\d\d):(\d\d)(?:[,\.](\d\d\d))? *--> *(?:(?:(\d\d):)?(\d\d):(\d\d)(?:[,\.](\d\d\d))?)?/,
        tt = src.split(spat),
        i = 1,
        l = tt.length,
        timecode,
        content,
        tcs,
        start,
        end,
        ret = [];

    for ( ; i<l; i+=2 ) {
        timecode = tt[i],
        content = tt[i+1],
        tcs = timecode.match(tcpat),
        start = (parseInt(tcs[1]||"0")*3600) + (parseInt(tcs[2])*60) + parseFloat(tcs[3]+"."+(tcs[4]||"0")),
        end = (tcs[6] === undefined) ? undefined : (parseInt(tcs[5])*3600) + (parseInt(tcs[6])*60) + parseFloat(tcs[7]+"."+(tcs[8]||"0"));
        ret.push({start: start, end: end, timecode: timecode, content: trim.call(content)});
    }
    return ret;
};

//
// "aa" srtparser with urls & source line numbers
//
var AASRT_URL_PAT = /^(https?:\/\/.+?)$/m,
    AASRT_TC_PAT = /^(?:(\d\d):)?(\d\d):(\d\d)(?:[,\.](\d{1,3}))? *--> *(?:(?:(\d\d):)?(\d\d):(\d\d)(?:[,\.](\d{1,3}))?)?$/m;

/*
def parse_tc (tc, ret):
    if tc != None:
        ret['start'] = (int(tc[0] or 0) * 3600) + (int(tc[1]) * 60) + int(tc[2])
        if tc[3] != None:
            ret['start'] = ret['start'] + float("0."+tc[3])

        if tc[5] != None:
            ret['end'] = (int(tc[4] or 0) * 3600) + (int(tc[5]) * 60) + int(tc[6]) 
            if tc[7] != None:
                ret['end'] = ret['end'] + float("0."+tc[7])
    return ret
*/

function parse_tc (tc, ret) {
    if (tc !== null) {
        ret.start = (parseInt(tc[1]||0) * 3600) + (parseInt(tc[2]) * 60) + parseInt(tc[3]);
        if (tc[4] !== "") {
            ret.start = ret.start + parseFloat("0."+tc[4]);
        }
        if (tc[6] !== "") {
            ret.end = (parseInt(tc[5]||0) * 3600) + (parseInt(tc[6]) * 60) + parseInt(tc[7]);
            if (tc[8] !== "") {
                ret.end = ret.end + parseFloat("0."+tc[8]);
            }
        }
    }
    return ret;
}

function aa_aasrtparse(src, normalize) {
    var url = null,
        tc = null,
        content = null,
        content_linestart = null,
        prev = null,
        curline = 0,
        // lines = src.split(/\r\n|[\n\v\f\r\x85\u2028\u2029]/),
        lines = src.split(/^/m),
        i, len, line, m,
        ret = [];

    for (i=0, len=lines.length; i<len; i++) {
        line = lines[i];
        curline += 1;
        m = line.match(AASRT_URL_PAT);
        if (m !== null) {
            if (content !== null) {
                ret.push(parse_tc(tc, {href: url, content: content, lineStart: content_linestart, lineEnd: curline-1}));
            }
            url = m[1];
            tc = null;
            content_linestart = curline;
            content = '';
            prev = 'URL';
        } else {
            m = line.match(AASRT_TC_PAT);
            if (m !== null) {
                if (prev !== 'URL') {
                    // freestanding timecode
                    if (content !== null) {
                        ret.push(parse_tc(tc, {href: url, content: content, lineStart: content_linestart, lineEnd: curline-1}));
                    }
                    content_linestart=curline;
                    content = '';
                }
                tc = m;
                prev = 'TC';
            } else {
                // Append contents
                if (content == null) {
                    content_linestart = curline;
                    content = line;
                } else {
                    content += line;
                }
                prev = "TEXT"
            }
        }
    }
    if (content !== null) {
        ret.push(parse_tc(tc, {href: url, content: content, lineStart: content_linestart, lineEnd: curline}))
    }
    if (normalize) {
        aa_aasrtparse_normalize(ret);
    }
    return ret;
}

function aa_aasrtparse_normalize(titles) {
    // href ==> href, end <== start
    var i=0, len=titles.length;
    for (i=0; i<len-1; i++) {
        titles[i].end = titles[i].end || titles[i+1].start;
        titles[i+1].href = titles[i+1].href || titles[i].href;
    }
    return titles;
} 
