aa.recyclable = aa_recyclable;

// A getter for elements that can later be "released" and reused
// (given by subsequent calls to the main getter function)

function aa_recyclable (create_item, that) {
    var items=[];

    function release (item) {
        var i, l, wrap;
        for (i=0, l=items.length; i<l; i++) {
            wrap = items[i];
            if (wrap.item == item && wrap.locked) {
                wrap.locked = false;
                return true;
            }
        }
    }

    return function () {
        var i, l, wrap;
        for (i=0, l=items.length; i<l; i++) {
            wrap = items[i];
            if (!wrap.locked) {
                wrap.locked = true
                return wrap.item;
            }
        }
        wrap = {
            locked: true,
            item: create_item.call(that || window)
        }
        console.log("CREATED", wrap.item);
        wrap.item.release = function () {
            release(wrap.item);
        }
        items.push(wrap);
        return wrap.item;
    }
}
