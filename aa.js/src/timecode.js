aa.secondsToTimecode = aa_secondsToTimecode;
aa.timecodeToSeconds = aa_timecodeToSeconds;

function aa_secondsToTimecode (ss, style) {
    style = timecode_styles[style || 'html5'];
    var h = Math.floor(ss/3600),
        m,
        s,
        fract,
        ret;
    ss -= (h * 3600);
    m = Math.floor(ss / 60);
    ss -= (m * 60);
    s = Math.floor(ss);
    fract = style.delimiter + (""+(ss - s)).substr(2, 3);
    while (fract.length < 4) { fract += "0"; } // PAD TO ALWAYS BE THREE DIGITS LONG (eg .040)
    ret = (h || style.requireHours ? ((((h<10)?"0":"")+h)+":") : '')
    ret += (((m<10)?"0":"")+m)+":"+(((s<10)?"0":"")+s)+fract;
    return ret;
}

timecode_styles = {
    'srt': {
        requireHours: true,
        delimiter: ','
    },
    'html5': {
        requireHours: false,
        delimiter: '.'
    }
}


function aa_timecodeToSeconds (tc) {
    var tcpat = /^(?:(\d\d):)?(\d\d):(\d\d(?:[\.,](?:\d{1,3}))?)$/,
        groups = tcpat.exec(tc);
    if (groups != null) {
        var h = groups[1] !== undefined ? parseInt(groups[1]) : 0,
            m = parseInt(groups[2]),
            s = parseFloat(groups[3].replace(/,/, "."));
        return (h * 3600) + (m * 60) + s;
    }
}