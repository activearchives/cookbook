aa.tabs = aa_tabs;

/** TABS **/
// requires jquery, jQueryUI.sortable

function aa_tabs (elt) {
    var that = {elt: elt},
        tabsbody = $("<div></div>").addClass('tabsbody').appendTo(elt),
        newTab = $("<div><div class='tabbody'>+</div></div>").addClass('tab').addClass('newtab').appendTo(elt);

    $(tabsbody).sortable({
        axis: 'x'
    });

    newTab.click(function () {
        $(elt).trigger("tabnew");
    })
    that.activate = function (data) {
        var tabelt;
        $(".tab", elt).each(function () {
            var d = $(this).data("tab");
            if (data == d) {
                tabelt = this;
            }
        });
        if (tabelt) {
            clickTab.call(tabelt, data);
        }
    }
    function clickTab (data) {
        $(".tab", elt).removeClass("active");
        $(this).addClass('active');
        $(elt).trigger("tabchanged", data);
    }

    that.new = function (data, activate, activate_event) {
        var $tabelt = $("<div></div>"),
            tabelt = $tabelt.get(0);

        content = $("<div></div>")
          .addClass("tabbody")
          .html(data.label || data.href || "&hellip;")
          .appendTo($tabelt);
          
        $tabelt.addClass('tab')
          .data("tab", data)
          .appendTo(tabsbody)
          .click(function (evt) {
            clickTab.call(tabelt, $.extend({}, data, {originalEvent: evt, click: true}));
          });
        if (activate) {
            clickTab.call(tabelt, $.extend({}, data, {originalEvent: activate_event, click: false}));
        }
        resizeTabs();
    }

    function resizeTabs () {
        var aw = $(elt).width(),
            cw = $(tabsbody).width(),
            nw = $(newTab).width(),
            totalwidth = 0,
            tabs = $(".tab", tabsbody);
        tabs.each(function () {
            $(this).css("maxWidth", "");
            var tw = $(this).width();
            totalwidth += tw;
        });
        // console.log("totalwidth", totalwidth, "aw", aw, "nw", nw);
        if (totalwidth > aw) {
            // resize tabs to fit
            maxWidth = (aw - nw) / tabs.size();
            // console.log("resize", maxWidth, tabs.size());
            tabs.each(function () {
                $(this).css("maxWidth", maxWidth+"px");
            })
        }
    }
    that.resize = resizeTabs;
    // $(elt).bind("resize", resizeTabs);
    return that;
}
