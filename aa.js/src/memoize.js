aa.memoize = aa_memoize;

function aa_memoize (fn, keyfn) {
    var results = {};
    keyfn = keyfn || function (x) { return x.toString(); };
    var ret = function () {
        var key = keyfn.apply(null, arguments),
            result = results[key];
        if (result === undefined) {
            result = fn.apply(null, arguments);
            results[key] = result;
        }
        return result;
    };
    ret.items = results;
    return ret;
}
