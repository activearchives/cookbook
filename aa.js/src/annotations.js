aa.annotations = aa_annotations;
aa.timeRangeHref = aa_timeRangeHref;
aa.lineRangeHref = aa_lineRangeHref;

import "ranger";
import "memoize";

function aa_timeRangeHref (href, start, end) {
    var ret = href;
    if (start) {
        ret += "#t=" + aa.secondsToTimecode(start, 'html5');
    }
    if (end) {
        ret += "," + aa.secondsToTimecode(end, 'html5');
    }
    return ret;
}

function aa_lineRangeHref (href, start, end) {
    var ret = href;
    if (start) {
        ret += "#line=" + start;
    }
    if (end) {
        ret += ","+end;
    }
    return ret;
}

/*

Generalizing what annotation actually is is...

Collage manager,
where glue(a, b)
where a & b may be fragments.
Then manage efficient requests for what's stuck to x (with or without fragments).

Selections should be cascadable & detect / prevent cycles.

glue("video#t=01:00,02:00", "srt#l=20,29")
glue("srt#l=20,29", "tagx")

select("video").linked() => ["srt#l=20,29"]
select("video").all() => ["srt#l=20,29", "tagx"]

select("tagx") => "srt#l=20,29"
select("tagx") => ["srt#l=20,29", "video#t=01:00,02:00"]

ability for a given source to represent & union different regions (possibly non-contiguous)


*/

function aa_annotations () {
	var that = {},
		rangerForHref = aa.memoize(function (href) {
			return aa.ranger();
		});

	// TODO: make annotations bi-directional !!??
	that.add = function (target, body) {
		var target = aa.href(target),
			body = aa.href(body),
			ranger = rangerForHref(target.basehref);
		ranger.add(body, target.start, target.end);
	}

	that.select_target = function (target) {
		var target = aa.href(target);
		ranger = rangerForHref(target.basehref);
		var slide = ranger.slider.slide(target.start, target.end);
		return slide;
			// .enter()
			// .exit()
			// .each();
	}

	return that;
}

