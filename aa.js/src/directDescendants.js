function directDescendants () {
    // returns this.childNodes.filter(this.nodeType=1)
    var ret = [];
    for (var i=0; i<this.childNodes.length; i++) {
        if (this.childNodes[i].nodeType == 1) {
            ret.push(this.childNodes[i]);
        }
    }
    return ret;
}
var frames = d3.select("#frame").selectAll(directDescendants);
