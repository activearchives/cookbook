aa.rawdrag = aa_rawdrag;
aa.drag = aa_drag;

function aa_rawdrag (elt) {
    var dragging = false,
        that = {},
        callbacks = {},
        pos = {};

    that.on = function (evt, callback) {
        callbacks[evt] = callback;
        return that;
    }

    function dragstart(evt) {
        // console.log("dragstart");
        elt.addEventListener("mousemove", mousemove, false);
        elt.addEventListener("mouseup", mouseup, false);
        elt.addEventListener("mouseleave", mouseup, false);
        if (callbacks.dragstart !== undefined) {
            pos.clientX = evt.clientX;
            pos.clientY = evt.clientY;
            pos.startClientX = evt.clientX;
            pos.startClientY = evt.clientY;
            pos.dx = 0;
            pos.dy = 0;
            callbacks.dragstart.call(elt, pos, evt);
        }
    }

    function drag (evt) {
        if (callbacks.drag !== undefined) {
            pos.clientX = evt.clientX;
            pos.clientY = evt.clientY;
            pos.dx = pos.clientX - pos.startClientX;
            pos.dy = pos.clientY - pos.startClientY;
            callbacks.drag.call(elt, pos, evt);
        }
    }

    function dragend(evt) {
        // console.log("dragend");
        elt.removeEventListener("mousemove", mousemove, false);
        elt.removeEventListener("mouseup", mouseup, false);
        elt.removeEventListener("mouseleave", mouseup, false);
        if (callbacks.dragend !== undefined) {
            callbacks.dragend.call(elt, pos, evt);
        }
    }

    function mousemove (evt) {
        if (dragging) {
            drag(evt);
        }
    }

    function mouseup (evt) {
        if (dragging) {
            dragging = false;
            dragend(evt);
        }
    }

    elt.addEventListener("mousedown", function (evt) {
        if (!dragging && (evt.buttons & 1)) {
            dragging = true;
            dragstart(evt);
        }
    });
    /*
    window.addEventListener("mousemove", function (evt) {
        if (dragging) {
            //console.log("window.mousemove", evt);
            drag(evt);
        }
    }, false);
    */
    return that;
}

function aa_drag (elt, handleSelector) {
    var start = {},
        handleElt = elt.querySelector(handleSelector),
        drag = aa_rawdrag(handleElt)
            .on("dragstart", function (p, e) {
                // start.left = parseInt(this.parentNode.offsetLeft);
                // start.top = parseInt(this.parentNode.offsetTop);
                start.left = parseInt(elt.offsetLeft);
                start.top = parseInt(elt.offsetTop);
                // console.log("start", start);
            })
            .on("drag", function (p, e) {
                // this.parentNode.style.left = (start.left + p.dx) + "px";
                // this.parentNode.style.top = (start.top + p.dy) + "px";
                elt.style.left = (start.left + p.dx) + "px";
                elt.style.top = (start.top + p.dy) + "px";
            });
}

