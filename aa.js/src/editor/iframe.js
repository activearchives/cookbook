import "editor";

aa.editor.iframe = aa_iframe_editor;

function aa_iframe_editor () {
    var factory = {name: 'iframe'},
        sessionsByHref = {};

    factory.get_session = function (href) {
        return sessionsByHref[href];
    }

    factory.get_editor = function () {
        // console.log("CREATE IFRAME DIV");
        var editor = { factory: factory };
        editor.elt = document.createElement("div");
        $(editor.elt).addClass('iframe');
        // editor.aceeditor = ace.edit(editor.elt);

        function set_contents (i) {
            // console.log("hello set_contents", $(editor.elt).contents().get(0), i);
            if ($(i).is(":visible")) return;
            $("iframe", editor.elt).hide();
            $(i).show();
        }

        editor.href = function (href, done) {
            var href = aa.href(href),
                session = sessionsByHref[href.nofrag];
            
            if (session == "loading") {
                return false;
            }

            if (session != undefined) {
                // console.log("reusing iframe");
                if (done) {
                    window.setTimeout(function () {
                        done.call(session);
                    }, 0);
                }
                session.iframe.src = href.href; // udpate fragment if present
                set_contents(session.iframe);
                return true;
            }

            // console.log("CREATE IFRAME!!!!");
            sessionsByHref[href] = "loading";
            session = { href: href.nofrag, editor: editor }
            session.iframe = document.createElement("iframe");
            $(editor.elt).append(session.iframe);
            $(session.iframe).hide();
            session.iframe.addEventListener("load", function () {
                var iframe = this;
                // MAP CLICKS TO frameclick // NB: THIS ONLY WORKS FOR LOCAL CONTENT
                d3.select(iframe.contentDocument).on("click.aaeditor", function () {
                    var t = d3.event.target;
                    // console.log("iframe click", t, t.nodeName);
                    if (t.nodeName == "A") {
                        var href = d3.select(t).attr("href");
                        if (href) {
                            d3.event.preventDefault();
                            $(editor.elt).trigger("frameclick", { originalEvent: d3.event, originalTarget: d3.event.target });                            
                        }
                    }
                });
            }, false);
            sessionsByHref[href.nofrag] = session;
            if (done) {
                window.setTimeout(function () {
                    done.call(session);
                }, 0);
            }
            set_contents(session.iframe);
            session.iframe.src = href.href;
        }
        return editor;
    };

    return factory;
};
