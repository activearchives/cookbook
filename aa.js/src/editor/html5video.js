import "editor";

aa.editor.html5video = aa_html5video_editor;

function aa_html5video_editor () {
    var factory = {name: 'html5video'},
        sessionsByHref = {};

    factory.get_session = function (href) {
        return sessionsByHref[href];
    }

    factory.get_editor = function () {
        // console.log("CREATE IFRAME DIV");
        var editor = { factory: factory },
            media,
            current_href_nofrag;

        editor.elt = document.createElement("div");
        $(editor.elt).addClass('html5video');
        // editor.aceeditor = ace.edit(editor.elt);

        function set_contents (m) {
            // console.log("hello set_contents", $(editor.elt).contents().get(0), i);
            if ($(m).is(":visible")) {
                // console.log("set_contents... doing nothing");
                return;
            }
            $("video,audio", editor.elt).hide();
            media = m;
            $(m).show();
        }

        editor.href = function (href, done) {
            if (arguments.length == 0) {
                return aa.timeRangeHref(current_href_nofrag, media.currentTime);
            }
            var href = aa.href(href),
                session = sessionsByHref[href.nofrag];

            current_href_nofrag = href.nofrag;

            if (session == "loading") {
                return false;
            }

            if (session != undefined) {
                // console.log("reusing iframe");
                if (done) {
                    window.setTimeout(function () {
                        done.call(session);
                    }, 0);
                }
                if (href.start) {
                    // console.log("jumping to time", href.start);
                    session.media.currentTime = href.start;
                    session.media.play();                    
                }
                set_contents(session.media);
                return true;
            }

            // console.log("CREATE IFRAME!!!!");
            sessionsByHref[href] = "loading";
            session = { href: href.nofrag, editor: editor }
            session.media = document.createElement("video");
            session.media.setAttribute("controls", "");
            $(editor.elt).append(session.media);
            $(session.media).hide();
            session.media.addEventListener("timeupdate", function (e) {
                var media = this;
                // console.log("timeupdate", e, this);
                $(editor.elt).trigger("fragmentupdate", { editor: editor, originalEvent: e, originalTarget: e.target });                            
            }, false);
            sessionsByHref[href.nofrag] = session;
            if (done) {
                window.setTimeout(function () {
                    done.call(session);
                }, 0);
            }
            set_contents(session.media);
            session.media.src = href.href;
        }


        editor.toggle = function () {
            if (media) {
                media.paused ? media.play() : media.pause();
            }
            return that;
        }

        editor.play = function () {
            if (media) {
                media.play();
            }
            return that;
        }

        editor.pause = function () {
            if (media) {
                media.pause();
            }
            return that;
        }

        editor.jumpBack = function () {
            if (media) {
                media.currentTime = media.currentTime - 5;
            }
            return that;
        }

        editor.jumpForward = function () {
            if (media) {
                media.currentTime = media.currentTime + 5;
            }
            return that;
        }

        editor.currentTime = function (t) {
            if (arguments.length == 0) {
                if (media) {
                    return media.currentTime;
                }           
            } else {
                if (media) {
                    media.currentTime = t;
                }           
                return that;
            }
        }


        return editor;
    };

    return factory;
};
