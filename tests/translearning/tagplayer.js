(function () {

function escapequotes (text) {
    return text.replace(/\"/g, "\\\"");
}

window.rdftag_for_name = function (rdf, name) {
    /* create, if necessary, tag as both rdf node + dom for tagname */
    var b = rdf
        .where("?tag a aa:Tag")
        .where('?tag dc:title "'+escapequotes(name)+'"');
    if (b.size() > 0) {
        return b.get(0).tag; }

    var node = $.rdf.blank('[]');
    rdf.add(node+' a aa:Tag .');
    rdf.add(node+' dc:title "'+escapequotes(name)+'" .');

    return node;
};

function memoize (fn) {
    var results = {},
        memo = function (arg) {
            var ret = results[arg.toString()];
            if (ret === undefined) {
                ret = fn.call(null, arg);
                results[arg.toString()] = ret;
            }
            return ret;
        };
    /* simple accessor to cached result that won't call the function if not necessary */
    memo.get = function (arg) {
        return results[arg.toString()];
    }
    return memo;
}

function ajax_load_each (urls, each, done) {
    var next = function () {
        if (urls.length) {
            var url = urls.shift();
            $.ajax({
                url: url,
                success: function (data) {
                    each.call(url, url, data);
                    window.setTimeout(next, 0);
                },
                error: function (e) {
                    throw e;
                }
            })
        } else {
            done.call();
        }
    }
    next();
}

function stripfragment (url) {
    var x = url.indexOf("#");
    if (x !== -1) {
        return url.substr(0, x);
    } else {
        return url;
    }
}

function timecode (secs) {
    /* 70 => 00:01:10, 70.5 => 00:01:10.500 */
        var h, m, s, r;
        h = Math.floor(secs/3600);
        secs -= h*3600;
        m = Math.floor(secs/60);
        secs -= m*60;
        s = Math.floor(secs);
        r = "" + (h<10 ? "0"+h:h)+":"+(m<10 ? "0"+m:m)+":"+(s<10 ? "0"+s:s);
        if (secs - s) {
            // maybe should actually round this?!
            var f = ""+(secs-s),
                fs = f.substr(2, 3);
            while (fs.length < 3) { fs = fs+"0" } 
            r += "."+fs;
        }
        return r;
}
window.timecode = timecode;

function append_in_textorder (container, selector, item) {
    var itemtext = $(item).text(),
        list = $(container).find(selector).get();
    for (var i=0, l=list.length; i<l; i++) {
        var text = $(list[i]).text();
        if (itemtext < text) {
            item.insertBefore(list[i]);
            return i;
        }
    }
    item.appendTo(container);
}

var mfparset = function (url) {
    var mf = MediaFragments.parseMediaFragmentsUri(url);
    if (mf.hash && mf.hash.t && mf.hash.t.length) {
        return {
            start: mf.hash.t[0].startNormalized,
            end: mf.hash.t[0].endNormalized
        }
    }
}

var rangerfor;

$(document).ready(function () {

    var rdfByContext = {},
        reset_rdf_flag = true;

    var ubodyid = 0;
    window.dom = {}; // still used by source opener (to prevent dups)

    function reset_rdf () {
        window.rdf = $.rdf();
        window.rdf.prefix("aa", "http://activearchives.org/terms/");
        window.rdf.prefix("dc", "http://purl.org/dc/elements/1.1/");
        // rebuild all rangers (timelines)
        /* TO DO -- when restoring rangers, how to preserve the page state (ie all the ranger times) 
            will need some way to re-assert enter / exit states when reloading
        */
        rangerfor = memoize(function (url) {
            return window.ranger();
        });
        for (var ctx in rdfByContext) {
            var rdf = rdfByContext[ctx];
            add_rdf(rdf);
        }
    }
    reset_rdf();

    /* maybe this could be simplified, anyway not yet tested */
    window.setInterval(function () {
        if (reset_rdf_flag) {
            console.log("resetting rdf");
            reset_rdf_flag = false;
            reset_rdf();
        }
    }, 1000);

    /*
    Processes rdf from a source, if it's a reload, forces a reset of all rdf,
    otherwise calls the (lighter) incremental add_rdf function
    */
    function process_rdf (rdf, context) {
        // console.log("process_rdf: "+rdf.databank.size()+" triples, context:"+context);
        // to do deal with rebuilding structures when a context is cleared
        if (rdfByContext[context] !== undefined) {
            rdfByContext[context] = rdf;
            reset_rdf_flag = true;
        } else {
            rdfByContext[context] = rdf;
            add_rdf(rdf, context);
        }
    }

    /*
    Main function to incrementally add rdf (from context) to the stack,
    calls any "data helpers" like rdf_to_ranger that maintain data in response to the rdf
    */
    function add_rdf (rdf, context) {
        console.log("add_rdf", rdf.databank.size());
        window.rdf = window.rdf.add(rdf); // aha, it seems that with rdf.add(rdf) the original is *not* modified.
        /* begin RDF Processing */
        process_rdf_to_ranger(rdf);
        /* end RDF Processing */
        console.log(window.rdf.databank.size()+" total triples")
    }

    /*
    Rangers make it efficient to manage sets of time linked elements.
    In addition a ranger has a slider that remembers the currently selected range and efficiently allows updtes with enter/exit

    Records triples with mediafragment sujects or objects on a ranger/timeline object for the (unhashed) target from target start to end

    NEW: Generalized RDF+(Temporal)Mediafragments construct / not specific to annotations or dom
    NEXT: Think of equivalent for not time ranges.
    */
    function process_rdf_to_ranger (rdf) {
        // console.log("process_rdf_to_ranger", rdf.databank.size());
        var timepat = /t=/,
            seen = {},
            all_mf = rdf.where("?s ?p ?o")
                // .filter(function () { return timepat.test(this.target.value.fragment) })
                .each(function () {
                    var smf = mfparset(this.s.value.toString()),
                        omf = (this.o.type == "uri") ? mfparset(this.o.value.toString()) : undefined,
                        timeline;
                    if (smf !== undefined) {
                        timeline = rangerfor(stripfragment(this.s.value.toString()));
                        timeline.add({s: this.s, p: this.p, o: this.o, backlink: false, smf: smf, omf: omf}, smf.start, smf.end);
                    }
                    if (omf !== undefined) {
                        timeline = rangerfor(stripfragment(this.o.value.toString()));
                        timeline.add({s: this.s, p: this.p, o: this.o, backlink: true, smf: smf, omf: omf}, omf.start, omf.end);
                    }
                });
    }

    /*
    New thinking: tag clicking, creates the "tag document"...
    as an (unsaved) AA/SRT document
    */
    function tagclick (tagname) {
        //console.log("tagclick", this);
        var tag = window.rdf.where("?tag a aa:Tag").where('?tag dc:title "'+escapequotes(tagname)+'"').get(0).tag,
            tagplaylist = rdf.where("?target aa:hasTag "+tag).map(function () { return this.target }),
            text = "";

        // console.log("tag playlist:" + tagplaylist.size(), tag);

        tagplaylist.each(function () {
            console.log(" "+this);
            var base = stripfragment(this.value.toString()),
                mf = mfparset(this.value.toString());
            if (mf && mf.start !== undefined) {
                text += base+"\n";
                text += timecode(mf.start)+" -->"+(mf.end !== undefined ? " "+timecode(mf.end) : "")+"\n\n";
            } else {
                text += this.value.toString()+"\n\n";
            }
        });

        // create a new (unnamed) SRT document
        var elt = $("<div></div>")
            .appendTo("#content")
            .srt({
                text: text,
                load: function (obj) {
                    process_rdf(obj.rdf(), $.rdf.blank("[]"));
                }
            });

        elt.css("left", (400+Math.floor(Math.random()*50))+"px")
            .css("top", (100+Math.floor(Math.random()*50))+"px");
        // console.log(timeline.unparse());
        /*
        $("svg").remove();
        var controller = ranger_controller($("#controller").get(0), timeline, {
            drag: function () {
                // transmit the update ...
                var slide = this.timeline.slider;
                slide.each(function (start, end, s, e) {
                    var base = stripfragment(this.value.toString()),
                        elt = dom["<"+base+">"],
                        mft = mfparset(this.value.toString());
                    console.log(this.value.toString(), mft.start, start, end, s, e);
                    if (elt) {
                        elt.currentTime = mft.start + (s - start);
                    }
                })
            }
        });
        */
    }
    // $(document).on("click", ".tag", tagclick);

    /*
    function done () {
        console.log(rdf.databank.size()+" triples");
        process_rdf_to_ranger(rdf);

        // select a tag and play it
        var tagname = "Wie ben jij?",
            tag = rdf.where("?tag a aa:Tag").where('?tag dc:title "'+escapequotes(tagname)+'"').get(0).tag,
            tagplaylist = rdf.where("?target aa:hasTag "+tag).map(function () { return this.target });
        console.log("tag playlist:" + tagplaylist.size() + " matches");
        tagplaylist.each(function () {
            var mf = MediaFragments.parseMediaFragmentsUri(this.value.toString()),
                base = stripfragment(this.value.toString()),
                r = rangerfor(base);
                // range = r.from(-1, -1);
                // range = r.from(mf.hash.t[0].startNormalized, mf.hash.t[0].endNormalized);

            // this code should move OUT
            var video = $("<video controls></video>")
                .attr("src", base)
                .appendTo("#media")
                .bind("timeupdate", function () {
                    var ct = this.currentTime,
                        slide = r.slider.slide(ct, ct);
                    slide.enter(function (s, e, cs, ce) {
                        $(dom[this]).addClass("active");
                    });
                    slide.exit(function () {
                        $(dom[this]).removeClass("active");
                    });
                });
            dom["<"+base+">"] = video.get(0);
        });
    }
    */

    // load all sources
    // var sources = $("ul.srt a").map(function () { return $(this).attr("href") }).get();
    // ajax_load_each(sources, process_srt, done);

    /* Open clicked files using a viewer widget & process related metadata */
    $("ul a").click(function () {
        var href = $(this).attr("href"),
            hrefabs = this.href,
            node = $.rdf.resource("<"+href+">"),
            elt = window.dom[node.toString()]; // TODO: REPLACE WITH [data-embed-href]
        if (!elt) {
            var klass;
            if (href.match(/\.srt$/)) {
                klass = 'srt';
            } else if (href.match(/\.webm$/)) {
                klass = 'html5media';
            }
            if (klass) {
                elt = $("<div></div>")
                    .appendTo("#content")
                    [klass]({
                        src: hrefabs,
                        load: function (obj) {
                            process_rdf(obj.rdf(), hrefabs);
                        }
                    });

                elt.css("left", (400+Math.floor(Math.random()*50))+"px")
                    .css("top", (100+Math.floor(Math.random()*50))+"px");
                window.dom[node.toString()] = elt.get(0);
            }
        }
        return false;
    });


    /* Generic Document Event Handlers */
    /* Fragment updates from media elements */
    $("body").bind("fragmentupdate", function (evt, d) {
        // console.log("fragmentupdate", d); // this is ringing back to the source
        // return;
        var mf = mfparset(d),
            time;

        if (mf && mf.start !== undefined) {
            time = mf.start;
            set_fragment_time(d, time, {}); // problem is this is called losing touched context (as its async)
        }
    });
    /* Click events from SRTs (tags and timecodes) */
    $(document).on("click", "a.aatag", function () {
        console.log("tag click", this);
        tagclick($(this).text());
        return false;
    }).on("click", "a.aafragment", function () {
        // console.log("fragment click", this);
        var href = this.href,
            mf = mfparset(href);
        if (mf && mf.start !== undefined) {
            set_fragment_time(href, mf.start, {click: true});
        }
        return false;
    });

    /*

    ranger is for webm... should be linked to non-webm timeline, which is then linked to the titles

    need to differentiate a "hard / driving" set time commands,
    and "softer" timeupdate responses

    Situations

    1. Video is scrubbed / time gets set "manually"

    fragmentupdate <VanDries.webm#t=00:00:09.489>    

    # does this count as a "driving" command
    # setDriver if unset, set to unset in future?
    # is this news ? (ie does the already stored fragment need to change in response)

    set VanDries.webm 9.489
    slide = VanDries.webm.timeline.slide(9.489)
    slide.enter() => _enter
    slide.exit() = > _exit
    slide.update() =>
      other temporal?
        translate time set Other translated_time(9.489)
      set vanDries (9.489)
      slide = VanDries.timeline.slide(9.489)
      slide.enter() => _enter
      slide.update() =>
        vanDries.webm -- TOUCHED, ignore



    <VanDries#t=0,dur> :hasAnnotation <VanDries.webm#t=0,dur>  -- from aa.html5media
    <VanDries#t=00:00:09.489,00:00:24.120> :hasTitle <VanDries01.srt#t02> -- from aa.srt

    VanDries.webm (slider) =update=> VanDries (slider) =enter=> VanDries01.srt

    set VanDries 9.489
        enter VanDries01.srt#t02    

    VanDries             [0                       DURATION]
        VanDries.webm    [0                       DURATION]
        VanDries01.srt   [  ][   ][  ][  ]


    2. Annotation fragment link is clicked

    First problem here,
    Once you add a call back to the actual media element (so that ranger drives the video),
    You have a ring as ranger => video => timeupdate => ranger
    Simple solution: check values (update only when different)... but this also seems conceptually dangerous
    as small precision differences could in principle create a ring as well.
    Better: in principle: the ranger driving the media element is in fact exceptional
    (mostly ranger follows media) --> and linked to the "special" event of a user click

    <!> Also has to do with setting currentTime needlessly (once it's already set on media, triggering a needless timeupdate)

    fragmentclick TwenteLeonieDeVriesRoelVanDries#t=00:00:09.489,00:00:24.120

    The problem with a simple set_element flag is that the link is actually linked to the virtual timeline,
    not DIRECTLY to the media element, so the flag can get lost if not propagated.


    VanDries01.srt =through.link-href=> VanDries (slider) =update=> VanDries.webm (slider)
                   <=(enter)===========
 
    */

    function _enter (n) {
        // console.log("enter", n.value.toString());
        var elt = $('[data-embed-href="'+n.value.toString()+'"]');
        elt.addClass("active");
        elt.each(function () {
            // console.log("as", $(this).closest(".aa-autoscrollable"), this);
            $(this).closest(".aa-autoscrollable").autoscrollable("scrollto", this);
        })
    }

    function _exit (n) {
        // console.log("exit", n.value.toString());
        $('[data-embed-href="'+n.value.toString()+'"]').removeClass("active");
    }
    // touched critically prevents cycles in a single call chain to linked elements
    // when click is set to true in touched, it will actively drive media widgets
    // (otherwise the assumpution is something is already driving the calls)
    function set_fragment_time (href, t, touched) {
        var base = stripfragment(href),
            r = rangerfor.get(base);
        // console.log("set_fragment_time", base, href, t);
        if (touched[base]) {
            // console.log("STOP");
            return;
        };
        touched[base] = true;

        if (touched.click) { // only talk back to media elements when this set chain is a "CLICK"
            // (and never in response to a timeupdate or other non-click event)
            var elt = $('[data-embed-href="'+base+'"]');
            if (elt.size()) {
                console.log("set_fragment_time: elt", elt.get(0));
                elt.data("aaobj").setfragment("#t="+timecode(t));
            } else {
                console.log("element not found", base);
            }
        }

        var slide = r.slider.slide(t, t);
        // PROPAGATE THE SLIDE TO CONNECTED ELEMENTS
        slide.enter(function () {
            // trigger linked elements "enter" behavior
            var other = (this.backlink) ? this.s : this.o;
            _enter(other);
        });
        slide.exit(function () {
            // trigger linked elements "exit" behavior
            var other = (this.backlink) ? this.s : this.o;
            _exit(other);
        });
        // propagate the slide to temporal "others" in update set
        slide.each(function () {
            var other = (this.backlink) ? this.s : this.o,
                thismf = (this.backlink) ? this.omf : this.smf,
                omf = (this.backlink) ? this.smf : this.omf;
            // console.log("update", other.value.toString());
//            if (other.type == "uri" && touched[other.value.toString()] === undefined) {
            if (other.type == "uri") {
                if (omf) {
                    var translated_time = omf.start + (t - thismf.start); // not sure about this!
                    set_fragment_time(other.value.toString(), translated_time, touched);
                }
            }
        });
    }

});


})();