Cookbook
==========

![http://activearchives.org/mw/images/f/f3/ToServeMan.png](http://activearchives.org/mw/images/f/f3/ToServeMan.png)

Cookbook is a local server to facilitate collaborative coding & publishing.

Cookbook is a hypertext writing & coding tool.

Cookbook is a collage writing space.


Installation
------------
These instructions are based on a standard debian install.

You need pip, the python package manager, from the commandline try running pip, if it's not there, you could:
 
    sudo easy_install pip

This install creates a python virtual environment and uses git. You may need to first install the virtualenv tool with:

    sudo pip install virtualenv

Then you can create a folder (say in your home) for the aa software, create a virtual env, checkout the code from gitorious and install it to the virtual env.

    git clone git://gitorious.org/activearchives/cookbook.git
    cd cookbook
    virtualenv venv
    source venv/bin/activate
    python setup.py sdist
    pip install dist/ActiveArchives-0.1.0.tar.gz

Running
------------

    source path/to/cookbook/venv/bin/activate
    cd /ANY/FOLDER/YOU/WANT/TO/WORK/WITH/
    aa

Upgrading an existing install
--------------------------------

    cd path/to/cookbook
    git pull
    source venv/bin/activate
    python setup.py install


Usage Scenarios
----------------

### Commandline video editing (Taking Care of Things)

### Epub writing

### Video Q&A (Open House)

### Personal Playlists (A Music player)

### OGG Annotation


Plan
-----
Core functionality ("Sublimation"):

* In browser file editing via Ace, with git & makefile awareness.
* "Mini-maps" and "previews" as standard recipes... system is smart about updating open displays (or introduce some kind of "live" preview toggle). Custom active previews of text documents (could be textual overview a la sublime, but could also be content-specific / non-textual / symbolic / toc style)

* "Active logs": Navigation history is recorded to a textual log file (in pwd) with hyperlinks ==> via a "history bot".
* Bots are on/off switchable (to transparently provide functionality like a "private mode")

(Trigger an event related to the node being activated) ... The view starts with a command palette of scripts that can be performed on pwd, such as: Directory Listing. DL produces text/html giving a tabular display of the contents of cwd with filename, size, and content-type (what about git specific data too!?). Clicking on a file link opens it's command palette: Open in editor (special function), matching recipes.


    # How to make a png from a text file
    titles/%.png: titles/%.title
        scripts/title.sh $< $@



    #!/bin/bash
    # How to make a png from a text file
    # %.png: %.title.utf8
    #     scripts/title.sh $< $@

    FONT=~/.fonts/Libertinage-i.ttf
    FONTSIZE=36
    #TEXT='The longer someone works in an archive, the more a colleague becomes also a database, and has everything in his head.'
    TEXT=$(cat $1)
    FILENAME=$2
    WIDTH=768
    HEIGHT=576
    PADDING=20
    CROPWIDTH=$((WIDTH-PADDING))
    # echo TEXT $TEXT
    # echo saving text to $FILENAME
    convert -font $FONT -background transparent -fill black -pointsize $FONTSIZE -size ${CROPWIDTH}x${HEIGHT} -gravity Center caption:"$TEXT" tmp.png
    convert tmp.png -channel RGBA -blur 0x3 tmp.shadow.png
    convert -font $FONT -background transparent -fill white -pointsize $FONTSIZE -size ${CROPWIDTH}x${HEIGHT} -gravity Center caption:"$TEXT" tmp.png
    composite tmp.shadow.png tmp.png -compose Plus tmp.comp.png
    rm tmp.png
    rm tmp.shadow.png
    convert tmp.comp.png -background transparent -gravity center -extent ${WIDTH}x${HEIGHT} $FILENAME
    rm tmp.comp.png
    # mv tmp.comp.png $2

    # melt -profile square_pal source/P1173650.JPG -track pango.comp.png -transition composite

Recipes (Standard Cupboard)
-------------
File visualisations...
* Directory Listing(s)
* Video thumbnail
* Audio waveform

* Titler (text to frame)
* Timeline.py (SRT to "tracks")
* and "tracks" to MELT command

Tools to assemble recipes into custom "cookbook" (generated bash + makefile!?)


My todo's
* Adjust editor to have a header / footer with mode + theme dropdowns
* Save button !?
* filename display
* tabs ? (extensible file views.... like a PREVIEW) or lovely a make page