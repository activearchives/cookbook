Planning
========

Multi-file editor

Key thing to have an editor that works with a folder as a collection of interrelated files. When looking at the history of a file, interesting to see not just that one file, but the relations to other sources (such as a Word file, or that it's been treated by some other filter).

In this sense it's MediaWiki + git + makefile.

Ultimately, it's about publishing / compiling results (as publishable HTML, as epub, as a video).

In the case of openhouse, output could be a self-contained HTML (or linked to other files). Compile srt data to a single file.

Key Question: How could SRT data be more self-standing. Translation to "annotations" that can be accumulated / compiled. Imagine making a JSON-LD version in annotation style. Use standard JSON-LD code to compile different annotations?

Is an annotated URL a kind of document (imagine serializing the ranger) -- is this not then a kind of timeline document.

* File grouping based on name conventions for derivatives.
* <!> Use git versioning to perform "in-place" transformations.
* Hide/delete originals?
* Imagine a document stream where only "tip" files remain
* Different outcomes as different branches

VJ14
* most open ended, not clear what's necessary beyond "videowiki" (and even that not clear)

DPT
* Markdown to HTML
* Integration of "patches"
* pandoc

POC: Word file to epub with pandoc. Record the steps using git to take SNAPSHOTS and describe the process. VIEW the git repo as a RECIPE.

* Use git to allow for playing with variations, allow you to clean up folders knowing you can "undo", return to a previous snapshot.

For SotQ:
(calibre)       ebook-convert DOCX to HTML
(python)        Clean HTML (maybe this step isn't really needed?)
(pandoc)        HTML to markdown
(text editor)   Editing
(python)        Patch Footnotes

When using an "external tool" -- how to record information about it
(such as the version used) eventually to assist running again.

* <!> git diff => representation of the transformation of a pipeline, the missing pieces are the command used to get from state A to B, and eventually the actual software involved (scripts, executables).

Imagine shell automatically written to a log file and committed along with the files.





