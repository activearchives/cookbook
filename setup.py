#!/usr/bin/env python

import distutils.command.install_lib
from distutils.core import setup
import os


def find (p, d):
    ret = []
    for b, dd, ff in os.walk(os.path.join(p, d)):

        for f in ff:
            if not f.startswith("."):
                fp = os.path.join(b, f)
                ret.append(os.path.relpath(fp, p))
    ret.sort()
    # for x in ret[:10]:
    #     print "**", x
    return ret

class chmodcgi_install_lib(distutils.command.install_lib.install_lib):
    def run(self):
        distutils.command.install_lib.install_lib.run(self)
        for fn in self.get_outputs():
            if fn.endswith(".cgi"):
                # copied from distutils source - make the binaries executable
                mode = ((os.stat(fn).st_mode) | 0555) & 0755
                distutils.log.info("changing mode of %s to %o", fn, mode)
                os.chmod(fn, mode)

setup(
    name='ActiveArchives',
    version='0.1.0',
    author='Active Archives Contributors',
    author_email='mm@automatis.org',
    packages=['activearchives', 'activearchives.commands', 'activearchives.test'],
    package_dir={'activearchives': 'activearchives'},
    package_data={'activearchives': find("activearchives", "templates/") + find("activearchives", "data/")},
    scripts=['bin/aa'],
    url='http://activearchives.org/wiki/AA-CLI/',
    license='LICENSE.txt',
    description='Cookbook is a collaborative scripting & publishing framework',
    # long_description=open('README.md').read(),
    install_requires=[
         "twisted",
         "html5lib",
         "rdflib",
         "Jinja2",
         "whoosh"
    ],
    cmdclass={'install_lib':chmodcgi_install_lib},
)
