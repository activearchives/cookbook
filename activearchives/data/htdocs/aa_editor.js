/*

Frame around an ACE editor adding AA specific functionality.

.edit (url) -- (re)open a document/edit session

Features
* Mode dropdown

Events
* fragmentclick: when timecodes are clicked

*/
function aa_editor (elt) {
	// requires: ace, tokenclicks, d3 (for modeselector)
	elt = typeof(elt) == "string" ? document.querySelector(elt) : elt;
	var that = {elt: elt},
		editor = ace.edit(elt.querySelector("#editor .aceeditor")),
	    modelist = require("ace/ext/modelist"),
	    TokenClicks = require('kitchen-sink/token_clicks').TokenClicks,
	    editSessions = {},
	    editHref,
	    tokenclicks = new TokenClicks(editor, {
	        tokenChange: function (token) {
	            // console.log("token", token)
	            if (token != null && (
	                (token.type == "heading.timecode.start") ||
	                (token.type == "heading.timecode.end") )) {
	                return true;
	            }
	        }
	    });

	editor.setTheme("ace/theme/dawn");
	editor.getSession().setMode("ace/mode/text");

	tokenclicks.on("tokenclick", function (token) {
	    // console.log("tokenclick", token);
	    if ((token.type == "heading.timecode.start") || (token.type == "heading.timecode.end")) {
	        var t = aa.timecodeToSeconds(token.value);
	        if (t) {
	            // console.log("media.currentTime", t);
	            // emit generic editor event
	            // better to create a fake anchor element to trigger this? maybe not
	            var href = editHref.replace(/\.srt$/, '');
	            $(elt).trigger("fragmentclick", { href: href+"#t="+aa.secondsToTimecode(t) });
	        } else {
	            console.log("bad timecode");
	        }
	    }
	});

	function session (href) {
		return editSessions[href];
	}
	that.session = session;

	that.newSession = function () {
		var session = ace.createEditSession("", "ace/mode/srt-md");
		editor.setSession(session);
	}

	function edit (href, done) {
	    if (editSessions[href] !== undefined) {
		    editHref = href;
	        editor.setSession(editSessions[href]);
	        var select = d3.select(".editormode")[0][0],
	            mode = editSessions[href].getMode();
	        select.value = mode.$id;
	        if (done) {
	        	done.call(that, editSessions[href]);
	        }
	    } else {

	        $.ajax({
	            url: href,
	            data: { f: (new Date()).getTime() },
	            success: function (data) {
	                // console.log("got data", data);
	                var mode = modelist.getModeForPath(href).mode || "ace/mode/text",
	                    session = ace.createEditSession(data, mode);
	                // console.log("mode", mode);
	                var select = d3.select(".editormode")[0][0];
	                select.value = mode;
	                editHref = href;
	                editSessions[href] = session;
	                editor.setSession(session);
	                // editor.setOption("showLineNumbers", false);
	                editor.setHighlightActiveLine(false);
	                session.setUseWrapMode(true);
	                if (done) {
	                	done.call(that, session);
	                }
	            },
	            error: function (code) {
	                console.log("error", this, code);
	            }
	        });
	    }            
	}
	that.edit = edit;

	// MODE SELECTOR
	// todo: maybe not use d3 here ;)... though it's lovely actually
	d3.select("select.editormode")
	    .on("change", function () {
	        // console.log("change", this, this.value);
	        editor.getSession().setMode(this.value);
	    })
	    .selectAll("option")
	    .data(modelist.modes)
	    .enter()
	    .append("option")
	    .attr("value", function (d) { return d.mode; })
	    .text(function (d) { return d.caption; });

	// SAVE BUTTON
	d3.select("button#save")
	    .on("click", function () {
	        var text = editor.getValue();
	        // console.log(editHref, text);
	        $.ajax({
	            url: "/aa/cgi-bin/saveas.cgi",
	            data: {
	                path: editHref,
	                data: text
	            },
	            method: 'post',
	            dataType: 'json',
	            success: function (response) {
	                console.log("response", response);
	            },
	            error: function (response) {
	              console.log("error", response);
	            }
	        });
	    });

	that.aceeditor = editor;

	that.insertHref = function (href) {
		// parse href for time
		var aahref = aa.href(href);
		editor.insert(aa.secondsToTimecode(aahref.start) + " -->\n");
	}

	return that;
} 



