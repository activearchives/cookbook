function aa_iframe_session (elt, href) {
	elt = typeof(elt) == "string" ? document.querySelector(elt) : elt;
	var that = {elt: elt},
    	media = null,
    	base_href = null,
    	bodyelt = $(".playerbody", elt).get(0);

	// Sniff for & watch media events in playable frames
	function iframe_onload (evt) {
	    var iframe = evt.target,
	        mediaevents = ["play", "timeupdate", "pause", "durationchange"],
	        mediaEvent = function (ename) {
	            // console.log("mediaEvent", this, ename);
	            media = this.target;
	            // retrigger events on elt
	            $(player).trigger(ename, this);
	        };
	    // console.log("player_iframe_onload", this, evt.target);
	    // Look for a video or audio tag
	    var m = iframe.contentDocument.querySelector("audio,video");
	    if (m !== null) {
	        mediaevents.forEach(function (eventName) {
	            // console.log("attaching event", eventName);
	            m.addEventListener(eventName, function (evt) {
	                mediaEvent.call(evt, eventName);
	            }, false);

	        });
	    }
	    d3.select(iframe.contentDocument).on("click.aaplayer", function () {
	        var t = d3.event.target;
	        // console.log("editor, iframe click", t, t.nodeName);
	        if (t.nodeName == "A") {
	            var href = d3.select(t).attr("href");
	            d3.event.preventDefault();
	            $(elt).trigger("fragmentclick", {href: href, originalTarget: t});
	        }
	    });

	}

	function playHref (href) {
		// parse for timecode!
		var old_base = base_href;
		var input = $(".urlinput", elt);
		// console.log(input, input);
		input.val(href);
		base_href = href.replace(/#.*$/, '');
		console.log("play", href, base_href);
		if (old_base == base_href && media) {
			var href = aa.href(href);
			// console.log("timeupdate", href.start);
			media.currentTime = href.start;
			return;
		}
	    var iframe = bodyelt.querySelector("iframe");
	    if (iframe == null) {
	        iframe = document.createElement("iframe");
	        bodyelt.appendChild(iframe);
	        iframe.addEventListener("load", iframe_onload, false);
	    }
	    iframe.src = href;
	    return that;
	}
	that.playHref = playHref;

	that.base = function () {
		return base_href;
	}

	that.href = function (href) {
		var ct, start, end;
		if (arguments.length == 0) {
			if (media) {
				ct = media.currentTime,
				start = aa.secondsToTimecode(ct, 'html5');
				end = aa.secondsToTimecode(ct+0.25, 'html5');
				return base_href+"#t="+start+","+end;
			} else {
				return base_href;
			}
		} else {
			that.playHref(href);
			return that;
		}
	}

	that.toggle = function () {
		if (media) {
			media.paused ? media.play() : media.pause();
		}
		return that;
	}

	that.play = function () {
		if (media) {
			media.play();
		}
		return that;
	}

	that.pause = function () {
		if (media) {
			media.pause();
		}
		return that;
	}

	that.jumpBack = function () {
		if (media) {
			media.currentTime = media.currentTime - 5;
		}
		return that;
	}

	that.jumpForward = function () {
		if (media) {
			media.currentTime = media.currentTime + 5;
		}
		return that;
	}

	that.currentTime = function (t) {
		if (arguments.length == 0) {
			if (media) {
				return media.currentTime;
			}			
		} else {
			if (media) {
				media.currentTime = t;
			}			
			return that;
		}
	}

	return that;

}