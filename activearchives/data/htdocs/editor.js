var annotations = aa.annotations();
/*

ace editor is a player connected to a particular element...
(so conceptually like a native html5 media element that can have it's source changed ...)
The idea when dragging, would be to keep the player/elt if possible & have the underlying session request a new element to become visible...

So: when a session is hidden, it drops/detaches its element/player/editor.
Also when sessions are created, they don't immediately take active/DOM form, possibly appearing only as an inactive tab.

I LOVE LIFE <3

<3 I HAVE NO FEARS

OPEN /01_Manu.ogv
==> [ HTMLMEDIA factory, iframe factory (direct), ... evt others ]
PLACE in a browser / frame element.
RELEASE/UNLOCK current element that is visible in the frame... (though this shouldn't hide the element... yet)
(RELEASE simply "unlocks" the element to allow changing source)
SEEK AN UNLOCKED element from the (leading) factory candidate, creating a new element if necessary.
Set the element.source and LOCK it.
Replace current Framed element in browser if it isn't the one that's visible (hiding any previous).

NB: Element may be momentarily visible while still unlocked in the event of a swap / source shift for instance.

ok what now...
poc videowiki:
* add back indexing (on open)
* token clicks
* fragments in editor/player
** update on playback
** fragmentupdate events
** paste event
* tag dragging / closing

*/

// enable frame-like behavior
aa.frames(document.getElementById('root'));

var editors = {
    'ace': aa.editor.ace(ace),
    'html5video': aa.editor.html5video(),
    'iframe': aa.editor.iframe()
}

function editor_for (href) {
    if (href.match(/\.srt$/)) {
        return editors.ace;
    } else if (href.match(/\.(ogv|mp4|webm|ogg|mp3)$/)) {
        return editors.html5video;
    } else {
        return editors.iframe;
    }
}

/********************************/
/* BROWSER **********************/
/********************************/
/* BROWSER **********************/
/********************************/

function aa_browser () {
    var that = {elt: this},
        $head = $(".head", this),
        tabs = aa.tabs($(".tabs", this).get(0)),
        $urlinput = $(".urlinput", this),
        $ment = $(".menu", this),
        $body = $(".body", this),
        $foot = $(".foot", this),
        get_editor = aa.memoize(function (factory) {
            return factory.get_editor();
        }, function (factory) {
            return factory.name;
        }),
        openHrefs = {},
        current_editor;

    $urlinput.on("keypress", function (e) {
        if (e.keyCode == 13) {
            that.open($(this).val());
        }
    })

    $(tabs.elt).on("tabchanged", function (e, data) {
        // check click to prevent cycle
        if (data.click) {
            that.open(data.href);
        }
    })

    $(that.elt).on("resize", function (e) {
        if (e.target == that.elt) {
            tabs.resize();
        }
    });

    that.editor_for_href = function (href) {
        // FUTURE
        // PROBLEM: the sharing of editor / session is problematic
        // actually I want an edit session, and to set ITS href
        // not the editor per se (as it may or not be immediately visible)
        // THIS in relation to "highlighting"...
        //
        var href = aa.href(href),
            tab = openHrefs[href.nofrag];
        if (tab !== undefined) {
            return get_editor(editor_for(href.nofrag));
        }
    }

    function show_editor (e) {
        if (e.elt.parentNode === $body.get(0)) {
            // console.log("show_editor: already visible");
        } else {
            // console.log("EMPTY BODY/APPEND")
            $body.empty();
            $body.append(e.elt);            
        }
    }

    that.open = function (href, label) {
        var href = aa.href(href),
            tab = openHrefs[href.nofrag],
            editor = get_editor(editor_for(href.nofrag));
        
        // console.log("open", href.base, editor);

        current_editor = editor;
        $urlinput.val(href.href);
        if (tab !== undefined) {
            tabs.activate(tab);
        } else {
            openHrefs[href.nofrag] = tab = {href: href.nofrag};
            tabs.new(tab, true);
        }
        show_editor(editor);
        editor.href(href.href, function () {});
    }

    $(that.elt).bind("fragmentupdate", function (e, x) {
        // console.log("fragmentupdate", e, x, this);
        if (current_editor) {
            var href = current_editor.href();
            if (href) { $urlinput.val(href); }
        }
    })

    return that;
}

var left = aa_browser.call(document.getElementById('left')),
    right = aa_browser.call(document.getElementById('right'));

// left.open("/aa/cgi-bin/home.srt", "home");
right.open("/aa/cgi-bin/listing.cgi", "listing");

/********************************/
/******************************/
/* Event routing              */
/******************************/
/* frameclick                 */
/******************************/
$(document).bind("frameclick", function (e, data) {
    // data.originalEvent, originalTarget
    // this = iframe / factory element
    // console.log("frameclick", e, this);
    var href = data.originalTarget.href,
        is_left = $.contains(left.elt, this),
        open_in_same = data.originalEvent.ctrlKey;
    // console.log("frameclick", is_left ? "left" : "right", data.originalEvent);
    var open_left = open_in_same ? is_left : !is_left,
        browser = open_left ? left : right;
    browser.open(href);
});

$(document).bind("fragmentclick", function (e, d) {
    var href = d.href,
        left = this.parentNode
    href = href.replace(/#/, '.ogv#');
    // console.log("fragmentclick", d.href, href);
    right.open(href);
    // play();
})

function text2tags (text) {
    var tagpat = /\[\[(.+?)\]\]/g,
        tags = [],
        m;
    while (m = tagpat.exec(text)) {
        tags.push(m[1]);
    }
    return tags;
}

function index (href, data) {
    if (href.match(/\.srt$/)) {
        // console.log("index", href);
        var targetBase = href.replace(/\.srt$/, ''),
            bodyBase = href,
            titles = aa.aasrtparse(data, true);
        // console.log("srt.titles", titles.length);
        $(titles).each(function () {
            var t = aa.timeRangeHref(targetBase, this.start, this.end),
                b = aa.lineRangeHref(bodyBase, this.lineStart, this.lineEnd),
                tags = text2tags(this.content);
            // console.log("a", t, b);
            annotations.add(t, b);
            tags.forEach(function (t) {
                // console.log("tag", b, t);
            });
        })
    } else {
        console.log("index, unknown type", href);
    }
}

function highlight (href) {
    var href = aa.href(href),
        bb = [left, right];
    bb.forEach(function (b) {
        var editor = b.editor_for_href(href.nofrag);
        if (editor) {
            // console.log("highlight", href.href, editor);
            editor.href(href.href);
        }
    })
}

/* Annotations */
$(document).bind("fragmentupdate", function (e, data) {
    // console.log("fragmentupdate", data.editor.href());
    // e.target == editor div
    var href = aa.href(data.editor.href()),
        base = href.base.replace(/\.ogv$/, ''),
        target;
    if (href.start) {
        target = aa.timeRangeHref(base, href.start, href.start+0.25);
        var slide = annotations.select_target(target);
        slide.enter(function (x) {
            highlight(this.href);
        });
        /*
        slide.exit(function (x) {
            // console.log("exit", x, this.href);
            return;
        });
        */
    }

    // console.log("target", target)nct
})
$(document).bind("editortoggle", function (e) {
    console.log("editortoggle");
}).bind("editorback", function (e) {
    console.log("editorback");
}).bind("editorforward", function(e) {
    console.log("editorforward");
});


/* EDITOR KEY SHORTCUTS */
/*
(function () {
    this.commands.addCommand({
        name: 'pasteTimecode',
        bindKey: {win: 'ctrl-shift-down',  mac: 'command-shift-down'},
        exec: function () {
            editor.insertHref(player.href());
        },
        readOnly: false
    });
    this.commands.addCommand({
        name: 'toggleMedia',
        bindKey: {win: 'ctrl-shift-up',  mac: 'command-shift-up'},
        exec: function () { player.toggle(); },
        readOnly: true // false if this command should not apply in readOnly mode
    });
    this.commands.addCommand({
        name: 'jumpMediaBack',
        bindKey: {win: 'ctrl-shift-left',  mac: 'command-shift-left'},
        exec: function () { player.jumpBack(); },
        readOnly: true // false if this command should not apply in readOnly mode
    });
    this.commands.addCommand({
        name: 'jumpMediaForward',
        bindKey: {win: 'ctrl-shift-right',  mac: 'command-shift-right'},
        exec: function () { player.jumpForward(); },
        readOnly: true // false if this command should not apply in readOnly mode
    });
    this.commands.addCommand({
        name: 'newSession',
        bindKey: {win: 'ctrl-n',  mac: 'command-n'},
        exec: function () { editor.newSession(); },
        readOnly: true // false if this command should not apply in readOnly mode
    });

}).call(editor.aceeditor);


*/




