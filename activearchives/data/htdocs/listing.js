function load(href, root_li, list_depth) {
	d3.json("?p="+encodeURIComponent(href), function (err, data) {

		var ul = d3.select(root_li).select('ul');
		if (ul[0][0] == null) {
			ul = d3.select(root_li).append('ul');
		}
		
		ul.selectAll("li.depth"+(list_depth+1))
		  .data(data.files)
		  .enter()
		.append("li")
		.append("a")
		  .attr('class', function (d) {
		  	var classes = d.type ? d.type.replace(/\//g, " ") : "";
		  	classes = (classes ? classes+" " : "") + "depth" + (list_depth+1);
		  	return classes;
		  })
		  .attr("href", function (d) {
		  	return d.url;
		  }).text(function (d) {
		  	return d.name;
		  });
	});
}

d3.select(document).on("click", function (e) {
	// console.log("document click");
	var target = d3.select(d3.event.target);
	if (target.classed("directory")) {
		var root_li = d3.event.target.parentNode;
		d3.event.preventDefault();
		var href = target.attr("href"),
			parse_depth = function (classes) {
				if (classes) {
					var m = classes.match(/depth(\d*)/);
					if (m !== null) return m[1];
				}
			},
			list_depth = parse_depth(target.attr("class")) || 0;
		console.log("load", href, root_li, list_depth);
		load(href, root_li, list_depth);
	}
});

load('/', document.getElementsByTagName('li')[0], 0);