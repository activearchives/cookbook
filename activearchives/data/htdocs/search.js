(function () {
	// alert("search");

	var video;

	$(document).on("click", "a.link", function () {
		var href = $(this).attr("href");
		console.log("href", href);
		if (video === undefined) {
			video = $("<video controls autoplay></video>").appendTo("#player");
		}
		video.attr("src", href);
		return false;
	});

	$("form#search").submit(function () {
		// console.log("search")
		$.ajax({
			url: "/aa/cgi-bin/search.cgi",
			data: {
				s: $("#searchtext").val()
			},
			dataType: "json",
			success: function (data) {
				// console.log("data", data);
				$("#results").text("");
				$(data).each(function () {
					var d = $("<div></div>").appendTo("#results").addClass("result");
					$("<a>link</a>").addClass("link").attr("href", this.targetUrl+"#"+this.targetFragment)
						.text(this.targetFragment)
						.appendTo(d);
					$("<div></div>").addClass("highlights").html(this.highlights).appendTo(d);
					// $("<div></div>").addClass("content").html(this.content).appendTo(d);
				})
			},
			error: function (obj, e) {
				console.log("error", e);
			}
		})
		return false;
	})

})();
