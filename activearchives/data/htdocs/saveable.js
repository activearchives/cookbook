window.addEventListener("keydown", function (evt) {
	if (!evt && window.event) { evt = window.event; }
	// var keyCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
	if (evt.ctrlKey && evt.key == "s") {
		evt.preventDefault();
		// temporarily remove *.tmp
		var nosave = document.querySelector(".nosave"),
			parent;
		if (nosave) {
			parent = nosave.parentNode;
			parent.removeChild(nosave);
		}
		$.post("/aa/cgi-bin/savehtml.cgi", {
			path: window.location.pathname,
			prettyprint: true,
			data: document.body.parentNode.outerHTML,
			success: function () {
				// console.log("postsave");
				if (nosave) { parent.appendChild(nosave); }
				alert("saved "+window.location.pathname);
			}
		})
	}
}, false);