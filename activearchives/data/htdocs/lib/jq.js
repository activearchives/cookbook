/* https://developer.mozilla.org/en-US/Add-ons/Code_snippets/QuerySelector */
// jquery replacements
//
function $ (selector, el) {
    if (!el) {el = document;}
    // return el.querySelectorAll(selector); // return raw NodeList
    // Return a proper Array (instead of a NodeList)
    return Array.prototype.slice.call(el.querySelectorAll(selector));
}

function $1 (selector, el) {
    if (!el) {el = document;}
    return el.querySelector(selector);
}

function ajax(opts) {
    var req = new XMLHttpRequest(),
        url = opts.url;
    req.onreadystatechange = function() {
        if (req.readyState == 4 ) {
            if (req.status == 200) {
                opts.success.call(req, req.responseText);
            } else {
                opts.error.call(req, req.status);
            }
        }
    };
    if (opts.data) {
        query = "";
        for (var key in data) {
            if data.hasOwnProperty(key) {
                query += (query ? '&' : '')+key+"="+encodeURIComponent(data[key]);
            }
        }
        url += "?"+query;
    }
    req.open("GET", url, true);
    req.send();
}
