#!/usr/bin/env python

import os, json, cgi, sys
from whoosh.index import open_dir
from whoosh.qparser import QueryParser
import cgitb; cgitb.enable()
from whoosh import highlight

pwd = os.environ.get("PWD", os.environ.get("PATH_TRANSLATED"))
index_path = os.path.join(pwd, "aaindex")
if not os.path.exists(index_path):
	print "Content-Type: application/json"
	print
	print json.dumps({'error': 'No index'})
	sys.exit(0)

ix = open_dir(index_path)
parser = QueryParser("content", ix.schema)
fs = cgi.FieldStorage()
querytext = fs.getvalue("s", "artist")
q = parser.parse(querytext)

with ix.searcher() as searcher:
	results = searcher.search(q)
	results.fragmenter = highlight.WholeFragmenter()
	print "Content-Type: application/json"
	print
	ret = []
	for r in results:
		ret.append({
			'targetUrl': r['targetUrl'],
			'targetFragment': r.get('targetFragment'),
			'highlights': r.highlights("content"),
			# 'highlights': hi.highlight_hit("content", r),
			'content': r['content']
		})
	print json.dumps(ret)

