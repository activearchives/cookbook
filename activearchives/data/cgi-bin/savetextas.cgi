#!/usr/bin/env python
#-*- coding:utf-8 -*-

import cgitb; cgitb.enable()
import cgi, codecs, sys, os
from settings import PAGES_PATH, PAGES_URL


method = os.environ.get("REQUEST_METHOD", "GET").upper()
ins = cgi.FieldStorage()
filename = ins.getvalue("filename", "")
data = ins.getvalue("text", "")

if method == "GET":
  print "Content-type: text/html"
  print
  print """<form method="post" action="">
filename: <input type="text" name="filename" value="{0}" />
<div>
  <textarea name="text">{1}</textarea>
</div>
<input type="submit" name="_submit" value="save" />
</form>
""".format(filename, data)

elif method == "POST":

  if not filename or not data:
    raise Exception("missing params")

  outpath = os.path.join(PAGES_PATH, filename)
  url = PAGES_URL.rstrip("/")+"/"+filename
  f = codecs.open(outpath, "w")
  f.write(data)
  f.close()

  print "Content-type: text/html"
  print
  print """<a href="{0}">{1}</a>""".format(url, filename)

