#!/usr/bin/env python

import cgitb; cgitb.enable()
import os, cgi, json, sys, mimetypes
from git import *


pwd = os.environ.get("PWD", os.environ.get("PATH_TRANSLATED", "."))

fs = cgi.FieldStorage()
requested_path = fs.getvalue("p")
if requested_path:
    path = os.path.join(pwd, requested_path.lstrip("/"))
    if not path.startswith(pwd):
        raise Exception("Bad path")
else:
    path = pwd

d = {'path': path}


def _blob(x):
    ret = {}
    ret['type'] = x.type
    ret['sha'] = x.hexsha
    ret['abspath'] = x.abspath
    if x.type == 'blob':
        ret['mime_type'] = x.mime_type
        ret['size'] = x.size
    return ret

try:
    repo = Repo(path)
    d['is_repo'] = True
    d['is_bare'] = repo.bare
    if not repo.bare:
        d['working_dir']=repo.working_dir
        d['is_dirty'] = repo.is_dirty()
        d['untracked_files'] = repo.untracked_files
        # repo.tree() == repo.heads.master.commit.tree
        d['tree'] = [_blob(x) for x in repo.tree()]

except InvalidGitRepositoryError:
    d['is_repo'] = False
    pass

print "Content-type: application/json"
print
print json.dumps(d).encode("utf-8")
sys.exit(0)


