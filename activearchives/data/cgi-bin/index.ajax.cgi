#!/usr/bin/env python

import cgitb; cgitb.enable()
import cgi, os, urllib, sys
from xml.sax.saxutils import quoteattr


env = os.environ
fs = cgi.FieldStorage()
qpath = fs.getvalue("path", "").strip("/")
pwd = env.get("PATH_TRANSLATED")
path = os.path.join(pwd, qpath)
displaypath = cgi.escape(urllib.unquote(qpath))
parts = displaypath.strip("/").split("/")
p = '/'
paths = []
for x in parts:
    p = os.path.join(p, x)
    paths.append((p, x))
navpath = '/'.join(['<a href="{0}">{1}</a>'.format(*x) for x in paths])
encoding = sys.getfilesystemencoding()
vars = {
    'encoding' : encoding,
    'displaypath': displaypath,
    'displaypathattr': quoteattr(displaypath),
    'navpath': navpath,
    'url': qpath,
    'path': path.rstrip("/")+"/",
    'pwd' : os.environ.get("PATH_TRANSLATED", ".").rstrip("/")+"/"
}

print """Content-type: text/html; charset={0[encoding]}

<!DOCTYPE html>
<html>
<head>
<title>aa: {0[displaypath]}</title>
<link href="/lib/styles.css" rel="stylesheet" />
<meta charset="utf-8" />
<script>
var PWD = "{0[pwd]}",
    URL = "{0[url]}",
    PATH = "{0[path]}";
</script>
<script src="/lib/lib/jquery/jquery-1.10.2.min.js"></script>
<script src="/lib/lib/rdfquery/jquery.rdfquery.core.min-1.0.js"></script>
<script src="/lib/lib/rdfquery/jquery.rdfquery.rdfa.min-1.0.js"></script>
<script src="/lib/lib/rdfquery/jquery.rdf.turtle.js"></script>
<script src="/lib/lib/d3/d3.min.js"></script>
<script src="/lib/lib/humanize.js"></script>
<script src="/lib/ranger.classic.js"></script>
<script src="/lib/data.timeline.js"></script>
<script src="/lib/index.js"></script>
</head>
<body>
<form method="post" action="/cgi-bin/mkdir.cgi">
    <span class="navpath"><a href="/">&larr;</a>/{0[navpath]}</span>
    <input type="text" name="name" />
    <input type="submit" value="mkdir" />
    <button onclick="play()" id="play">play</button>
    <input type="hidden" name="path" value={0[displaypathattr]} />
</form>
<div>
</div>
<div id="timeline"></div>
<div id="table"></div>
</body>
</html>
""".format(vars)


