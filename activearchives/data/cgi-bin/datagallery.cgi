#!/usr/bin/env python

import os, cgi
import cgitb; cgitb.enable()


print "Content-type:text/html;charset=utf-8"
print

fs = cgi.FieldStorage()
pwd = os.environ.get("PWD", os.environ.get("PATH_TRANSLATED", "."))
rpath = fs.getvalue("path", "").strip("/")
apath = os.path.abspath(os.path.join(pwd, rpath)).rstrip("/")+"/"

items = os.listdir(pwd)
items.sort()

for p in items:
    print """<div class="item">"""
    print """<div class="path">{0}</div>""".format(p)
    print """</div>"""
