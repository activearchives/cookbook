#!/usr/bin/env python

import os, cgi, json
import cgitb; cgitb.enable()


fs = cgi.FieldStorage()
pwd = os.environ.get("PWD", os.environ.get("PATH_TRANSLATED", "."))
rpath = fs.getvalue("path", "").strip("/")
apath = os.path.abspath(os.path.join(pwd, rpath)).rstrip("/")+"/"
items = os.listdir(pwd)
items.sort()
print "Content-type:application/json;charset=utf-8"
print
print json.dumps(items)

