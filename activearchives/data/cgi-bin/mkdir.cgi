#!/usr/bin/env python

import cgitb; cgitb.enable()
import cgi, os, sys

# print "Content-type: text/html"
# print
# print "hello"
# cgi.print_environ()

def redirect(url, message="Redirecting"):
    print "Content-type: text/html;charset=utf-8"
    print
    print """<html xmlns="http://www.w3.org/1999/xhtml">    
  <head>      
    <title>{1}</title>
    <link rel="stylesheet" href="/lib/styles.css" />     
    <meta http-equiv="refresh" content="0;URL='{0}'" />    
  </head>    
  <body> 
    <p><a href="{0}">{1}</a></p> 
  </body>  
</html>""".format(url, message)

env = os.environ
referer = env.get("HTTP_REFERER")
pwd = env.get("PATH_TRANSLATED")
fs = cgi.FieldStorage()
name = fs.getvalue("name", "").strip("/")
path = fs.getvalue("path", "").strip("/")

if name:
    abspath = os.path.join(pwd, path, name)
    try:
        os.mkdir(abspath)
    except OSError:
        redirect(referer, "Directory already exists")
        sys.exit(0)
    redirect(referer, "Created directory")
    # print "Location: {0}".format(referer)
else:
    print "Content-type: text/html"
    print
    print "Error"

