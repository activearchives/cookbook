#!/usr/bin/env python

import os, cgi, json
import cgitb; cgitb.enable()
from subprocess import check_output, CalledProcessError


fs = cgi.FieldStorage()
pwd = os.environ.get("PWD", os.environ.get("PATH_TRANSLATED", "."))
rpath = fs.getvalue("path", "").strip("/")
apath = os.path.abspath(os.path.join(pwd, rpath)).rstrip("/")
if os.path.isdir(apath):
    apath += "/"

try:
    mt = check_output(["file", "--brief", "--mime-type", apath]).strip()
    print "Content-type: application/json;charset=utf-8"
    print
    print json.dumps({
        'mime-type': mt,
        'status': 0
    })
except CalledProcessError, e:
    print "Content-type: application/json;charset=utf-8"
    print
    print json.dumps({'status': 1, 'error' : str(e)})
