#!/usr/bin/env python

import cgitb; cgitb.enable()
import cgi, sys, os
from subprocess import check_output

m = os.environ.get("REQUEST_METHOD")
pwd = os.environ.get("PWD", os.environ.get("PATH_TRANSLATED", "."))

if m == "POST":
    print "Content-type: text/html;charset=utf-8"
    print
    outpath = "/tmp/temp.docx"
    out = open(outpath, "wb")
    bytes = 0
    while True:
        data = sys.stdin.read()
        if not data:
            break
        bytes += len(data)
        out.write(data)
    out.close()

    tmpout = os.path.join(pwd, "temp")
    # cmd = 'ebook-converter "{0}" "{1}"'.format(outpath, tmpout)
    ret = check_output(['ebook-convert', outpath, tmpout])

    print "POST, wrote {0} bytes to {1}, {2}".format(bytes, tmpout, ret)
    sys.exit(0)

print "Content-type: text/html;charset=utf-8"
print
print ("""<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
</head>
<body>
    <form method="post" action="">
        <div id="input" class="page">
                <h2>1. Input</h2>
                <p>Either choose a file...<br />
                        <input type="file" name="file" />
                </p>
                <p>
                    or paste some text...<br />
                    <textarea></textarea>
                </p>
        </div>
        <div id="tools" class="page">
            <h2>2. Tool(s)</h2>
            <select name="output">
                <option>pandoc</option>
            </select>
        </div>
        <div id="output" class="page">
            <h2>3. Output</h2>
            <select name="output">
                <option value="html">HTML (web page)</option>
                <option value="pdf">PDF</option>
            </select>
                <input type="submit" name="_submit" value="ok" />
        </div>
    </form>
</body>
</html>""")
