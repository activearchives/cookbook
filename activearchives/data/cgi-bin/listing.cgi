#!/usr/bin/env python

import cgitb; cgitb.enable()
import os, cgi, json, sys, mimetypes

MIME_TYPES = {}

MIME_TYPES['jsonp'] = 'text/javascript'


def getmime (f):
    if os.path.isdir(f):
        return "inode/directory"
    else:
        base, ext = os.path.splitext(f)
        ext = ext[1:].lower()
        if ext in MIME_TYPES:
            return MIME_TYPES[ext]
        else:
            return mimetypes.guess_type(f)[0]

pwd = os.environ.get("PWD", os.environ.get("PATH_TRANSLATED", "."))

fs = cgi.FieldStorage()
requested_path = fs.getvalue("p")

if requested_path:
    path = os.path.join(pwd, requested_path.lstrip("/"))
    if not path.startswith(pwd):
        raise Exception("Bad path")
else:
    path = pwd

listdir = os.listdir(path)
listdir.sort(key=lambda a: a.lower())
d = {
    'files': []
}
for l in listdir:
    fp = os.path.join(path, l)
    rp = os.path.relpath(fp, pwd)
    f = {}
    f['url'] = "/"+rp
    f['name'] = l
    f['size'] = os.stat(fp).st_size
    f['type'] =  getmime(fp)
    d['files'].append(f)

if requested_path:
    cb = fs.getvalue("callback")
    if cb:
        print "Content-type: text/javascript"
        print
        sys.stdout.write(cb+"(");
        json.dump(d, sys.stdout);
        sys.stdout.write(");\n\n")
        sys.exit(0)

    print "Content-type: application/json"
    print
    print json.dumps(d).encode("utf-8")
    sys.exit(0)

print "Content-type: text/html;charset=utf-8"
print

print """<!DOCTYPE html>
<html>
<head>
<title>/</title>
<script src="/aa/lib/jquery/jquery-1.10.2.min.js"></script>
<script src="/aa/lib/d3/d3.min.js"></script>
<link rel="stylesheet" type="text/css" href="/aa/listing.css">
<meta charset="utf-8">
</head>
<body>
<ul>
<li><a href="/" class="directory">{0}</a></li>
""".format(pwd)

# for f in d['folders']:
#     print """<li><a href="{0}" class="directory">{0}</a></li>""".format(f)
# for f in d['files']:
#     print """<li><a href="{0}" class="file">{0}</a></li>""".format(f)

print """
</ul>
</body>
<script src="/aa/listing.js"></script>
</html>
"""