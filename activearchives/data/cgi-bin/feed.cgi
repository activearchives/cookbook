#!/usr/bin/python

import cgitb; cgitb.enable()
import cgi, feedparser, rdflib, time, datetime

DC = rdflib.Namespace("http://purl.org/dc/elements/1.1/")
DCTYPE = rdflib.Namespace("http://purl.org/dc/dcmitype/")
MEDIA = rdflib.Namespace("http://search.yahoo.com/mrss/")

def rssreadpages (u):
    entries = []
    while True:
        f = feedparser.parse(u)
        entries.extend(f.entries)
        try:
            u = [x for x in f['feed']['links'] if x['rel'] == 'next'][0].get("href")
        except IndexError:
            break
        break
    return entries

def caddl (g, s, p, o):
    if o:
        g.add((s, p, rdflib.Literal(o)))

def feedparserentry_to_rdf(i, g):
    isImage, isVideo = False, False

    url = i.get("link")
    s = rdflib.URIRef(url)
    caddl(g, s, DC['title'], i.get("title"))
    caddl(g, s, DC['creator'], i.get("author"))
    pub = i.get("published_parsed")
    if pub:
        pub = datetime.datetime.fromtimestamp(time.mktime(pub))
        g.add((s, DC['published'], rdflib.Literal(pub.isoformat())))

    thumbs = i.get("media_thumbnail")
    if thumbs:
        for thumb in thumbs:
            if thumb == {}: continue
            ts = rdflib.URIRef(thumb.get("url"))
            g.add((s, MEDIA['thumbnail'], ts))
            caddl(g, ts, MEDIA['width'], thumb.get("width"))
            caddl(g, ts, MEDIA['height'], thumb.get("height"))
            caddl(g, ts, MEDIA['type'], thumb.get("type"))

    for c in i.get("media_content"):
        if c == {}: continue
        cs = rdflib.URIRef(c.get("url"))
        # url, type, width, height = c.get("url"), c.get("type"), c.get("width"), c.get("height")
        g.add((s, MEDIA['content'], cs))
        caddl(g, cs, MEDIA['width'], c.get("width"))
        caddl(g, cs, MEDIA['height'], c.get("height"))
        caddl(g, cs, MEDIA['type'], c.get("type"))
        isImage = True


    mp = i.get("media_player")
    if mp:
        isImage = True
        isVideo = True
        displayurl = mp.get("url")

    if isImage:
        g.add((s, DC['type'], DCTYPE['Image']))
    if isVideo:
        g.add((s, DC['type'], DCTYPE['MovingImage']))


        # images: media_content -- medium + large version

fs = cgi.FieldStorage()
u = fs.getvalue("url", "http://vimeo.com/album/2176642/rss")
g = rdflib.Graph()
for e in rssreadpages(u):
    feedparserentry_to_rdf(e, g)

g.bind("dc", str(DC))
g.bind("dctype", str(DCTYPE))
g.bind("media", str(MEDIA))

format = fs.getvalue("format", "xml")


MIME = {
    'xml': 'application/rdf+xml',
    'turtle': 'text/plain'
}

print "Content-type: "+MIME[format]
print
print g.serialize(format=format)



"""
ITEM FROM VIMEO FEED

{'author': u'Piet Zwart Institute',
 'author_detail': {'name': u'Piet Zwart Institute'},
 'authors': [{}],
 'guidislink': False,
 'href': u'',
 'id': u'tag:vimeo,2012-12-06:clip55014518',
 'link': u'http://vimeo.com/55014518',
 'links': [{'href': u'http://vimeo.com/55014518',
            'rel': u'alternate',
            'type': u'text/html'},
           {'href': u'http://vimeo.com/moogaloop.swf?clip_id=55014518',
            'length': u'76004931',
            'rel': u'enclosure',
            'type': u'application/x-shockwave-flash'}],
 'media_content': [{}],
 'media_credit': {'role': u'author', 'scheme': u'http://vimeo.com/pzi'},
 'media_player': {'content': u'',
                  'url': u'http://vimeo.com/moogaloop.swf?clip_id=55014518'},
 'media_thumbnail': [{'height': u'150',
                      'url': u'http://b.vimeocdn.com/ts/380/594/380594930_200.jpg',
                      'width': u'200'}],
 'published': u'Thu, 06 Dec 2012 12:01:28 -0500',
 'published_parsed': time.struct_time(tm_year=2012, tm_mon=12, tm_mday=6, tm_hour=17, tm_min=1, tm_sec=28, tm_wday=3, tm_yday=341, tm_isdst=0),
 'summary': u'<p><a href="http://vimeo.com/55014518"><img alt="" src="http://b.vimeocdn.com/ts/380/594/380594930_200.jpg" /></a></p><p><p class="first"></p></p><p><strong>Cast:</strong> <a href="http://vimeo.com/pzi">Piet Zwart Institute</a></p><p><strong>Tags:</strong> <a href="http://vimeo.com/tag:Prototyping+Futures">Prototyping Futures</a></p>',
 'summary_detail': {'base': u'http://vimeo.com/album/2176642/rss',
                    'language': None,
                    'type': u'text/html',
                    'value': u'<p><a href="http://vimeo.com/55014518"><img alt="" src="http://b.vimeocdn.com/ts/380/594/380594930_200.jpg" /></a></p><p><p class="first"></p></p><p><strong>Cast:</strong> <a href="http://vimeo.com/pzi">Piet Zwart Institute</a></p><p><strong>Tags:</strong> <a href="http://vimeo.com/tag:Prototyping+Futures">Prototyping Futures</a></p>'},
 'title': u'Prototyping Futures / Occupying the Present -Michael Murtaugh',
 'title_detail': {'base': u'http://vimeo.com/album/2176642/rss',
                  'language': None,
                  'type': u'text/plain',
                  'value': u'Prototyping Futures / Occupying the Present -Michael Murtaugh'}}


ITEM FROM gallery3 photo gallery

{'content': [{'base': u'http://prototypingfutures.net/gallery/index.php/rss/feed/gallery/album/1',
              'language': None,
              'type': u'text/html',
              'value': u'<span></span>\n          <p>\n                      <img alt="" height="480" src="http://prototypingfutures.net/gallery/var/resizes/P5190004.JPG?m=1337591656" title="P5190004" width="640" /><br />\n                                </p>'}],
 'guidislink': False,
 'href': u'',
 'id': u'http://prototypingfutures.net/gallery/index.php/photos/2',
 'link': u'http://prototypingfutures.net/gallery/index.php/photos/2',
 'links': [{'href': u'http://prototypingfutures.net/gallery/index.php/photos/2',
            'rel': u'alternate',
            'type': u'text/html'}],
 'media_content': [{'filesize': u'87884',
                    'height': u'480',
                    'type': u'image/jpeg',
                    'url': u'http://prototypingfutures.net/gallery/var/resizes/P5190004.JPG?m=1337591656',
                    'width': u'640'},
                   {'filesize': u'2132082',
                    'height': u'3024',
                    'isdefault': u'true',
                    'type': u'image/jpeg',
                    'url': u'http://prototypingfutures.net/gallery/var/albums/P5190004.JPG?m=1337591653',
                    'width': u'4032'}],
 'media_group': u'',
 'media_thumbnail': [{'height': u'150',
                      'url': u'http://prototypingfutures.net/gallery/var/thumbs/P5190004.JPG?m=1337591654',
                      'width': u'200'}],
 'published': u'Mon, 21 May 2012 11:14:13 CEST',
 'published_parsed': time.struct_time(tm_year=2012, tm_mon=5, tm_mday=21, tm_hour=0, tm_min=0, tm_sec=0, tm_wday=0, tm_yday=142, tm_isdst=0),
 'summary': u'',
 'summary_detail': {'base': u'http://prototypingfutures.net/gallery/index.php/rss/feed/gallery/album/1',
                    'language': None,
                    'type': u'text/html',
                    'value': u''},
 'title': u'P5190004',
 'title_detail': {'base': u'http://prototypingfutures.net/gallery/index.php/rss/feed/gallery/album/1',
                  'language': None,
                  'type': u'text/plain',
                  'value': u'P5190004'}}

"""
