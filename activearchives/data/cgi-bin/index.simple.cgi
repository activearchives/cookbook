#!/usr/bin/env python

import cgitb; cgitb.enable()
import cgi, os, urllib, sys
from xml.sax.saxutils import quoteattr


env = os.environ
fs = cgi.FieldStorage()

qpath = fs.getvalue("path", "").strip("/")
pwd = env.get("PATH_TRANSLATED")
path = os.path.join(pwd, qpath)
print "Directory listing", path

# referer = env.get("HTTP_REFERER")


listdir = os.listdir(path)
listdir.sort(key=lambda a: a.lower())
folders = []
files = []
for l in listdir:
    fp = os.path.join(path, l)
    if os.path.isdir(fp):
        folders.append(l)
    else:
        files.append(l)

displaypath = cgi.escape(urllib.unquote(qpath))

parts = displaypath.strip("/").split("/")
p = '/'
paths = []
for x in parts:
    p = os.path.join(p, x)
    paths.append((p, x))
navpath = '/'.join(['<a href="{0}">{1}</a>'.format(*x) for x in paths])

encoding = sys.getfilesystemencoding()
print "Content-type", "text/html; charset=%s" % encoding
# print "Content-type: text/html;charset=utf-8"
print
print '<!DOCTYPE html>'

vars = {
    'displaypath': displaypath,
    'displaypathattr': quoteattr(displaypath),
    'navpath': navpath
}

print """<html>
<head>
<title>aa: {0[displaypath]}</title>
<link href="/lib/styles.css" rel="stylesheet" />
<meta charset="utf-8" />
</head>
<body>
<form method="post" action="/cgi-bin/mkdir.cgi">
<span class="navpath"><a href="/">&larr;</a>/{0[navpath]}</span>
<input type="text" name="name" />
<input type="submit" value="mkdir" />
<input type="hidden" name="path" value={0[displaypathattr]} />
</form>
<div class="listing">
<h2>folders</h2>
<ul class="listing folders">
""".format(vars)

def display (name):
    fullname = os.path.join(path, name)
    displayname = linkname = name
    # Append / for directories or @ for symbolic links
    if os.path.isdir(fullname):
        displayname = name + "/"
        linkname = name + "/"
        linkurl = urllib.quote(linkname) # direct link (relative to current location)
    else:
        fullurl = "/"+os.path.join(qpath, linkname)
        linkurl = "/cgi-bin/view.cgi?url="+urllib.quote(fullurl) # link through view.cgi

    if os.path.islink(fullname):
        displayname = name + "@"

    print ('<li><a href="%s">%s</a></li>\n'
            % (linkurl, cgi.escape(displayname)))

for name in folders:
    display(name)

print """</ul>
<h2>files</h2>
<ul class="listing files">"""

for name in files:
    display(name)

print """</ul></div>
</body>
</html>"""

