#!/usr/bin/python

import cgi, urllib2, urlparse, html5lib, rdflib
import cgitb; cgitb.enable()

DC = rdflib.Namespace("http://purl.org/dc/elements/1.1/")
DCMITYPE = rdflib.Namespace("http://purl.org/dc/dcmitype/")
AA = rdflib.Namespace("http://activearchives.org/terms/")

ret = rdflib.Graph()
fs = cgi.FieldStorage()
url = fs.getvalue("url", "http://prototypingfutures.net/archive/video/")
f = urllib2.urlopen(url)
seen = set((url, ))
t = html5lib.parse(f, namespaceHTMLElements=False)
for a in t.findall(".//a[@href]"):
    href = a.attrib.get("href")
    # Absolutize
    href = urlparse.urljoin(url, href)
    # Strip query + fragment
    p = urlparse.urlparse(href)
    href = urlparse.urlunparse((p[0], p[1], p[2], p[3], '', ''))
    if href not in seen and not url.startswith(href): # exclude seen & parent directory
        seen.add(href)
        u2 = urllib2.urlopen(href)
        info = u2.info()
        ct = info.get("content-type")
        if ';' in ct:
            ct, encoding = ct.split(";")
        s = rdflib.URIRef(href)
        ret.add((s, DC['format'], rdflib.Literal(ct)))
        if info.get("content-length"):
            ret.add((s, AA['content-length'], rdflib.Literal(info.get("content-length"))))
        u2.close()

print "Content-type:application/rdf+xml"
print
print ret.serialize()

