#!/usr/bin/env python

import cgitb; cgitb.enable()
import cgi, os, urllib, sys
from xml.sax.saxutils import quoteattr


env = os.environ
fs = cgi.FieldStorage()
url = fs.getvalue("url", "").rstrip("/")
pwd = env.get("PATH_TRANSLATED")
path = os.path.join(pwd, url)
# referer = env.get("HTTP_REFERER")
displaypath = cgi.escape(urllib.unquote(url))
parts = displaypath.strip("/").split("/")
p = '/'
paths = []
for x in parts:
    p = os.path.join(p, x)
    paths.append((p, x))
navpath = '/'.join(['<a href="{0}">{1}</a>'.format(*x) for x in paths])

encoding = sys.getfilesystemencoding()
print "Content-type", "text/html; charset=%s" % encoding
print
print """<!DOCTYPE html>
<html>
<head>
    <title>aa: {0[displaypath]}</title>
    <link href="/lib/styles.css" rel="stylesheet" />
    <meta charset="utf-8" />
    <script src="/lib/lib/jquery/jquery-1.10.2.min.js"></script>
    <script src="/lib/lib/jquery.hotkeys.js"></script>
    <script src="/lib/lib/jquery.insertatcaret.js"></script>
    <script src="/lib/lib/rdfquery/jquery.rdfquery.core.min-1.0.js"></script>
    <script src="/lib/lib/rdfquery/jquery.rdfquery.rdfa.min-1.0.js"></script>
    <script src="/lib/lib/rdfquery/jquery.rdf.turtle.js"></script>
    <script src="/lib/lib/d3/d3.min.js"></script>
    <script src="/lib/lib/humanize.js"></script>
    <script src="/lib/view.js"></script>
    <script src="/lib/aa.playable.js"></script>
</head>
<body>
    <div id="head">
        <form method="post" action="/cgi-bin/mkdir.cgi">
            <span class="navpath"><a href="/">&larr;</a>/{0[navpath]}</span>
            <input type="hidden" name="path" value={0[displaypathattr]} />
            <span id="fragment">#</span>
        </form>
        <span id="buttons">
            <button onclick="aa.button.annotate()" title="ctrl+f2" id="annotate">annotate</button>
            <button onclick="aa.button.save()" title="ctrl+f3" id="save">save</button>
            <button onclick="aa.button.link()" title="ctrl+down" id="link">link</button>
        </span>
    </div>
    <div>
        <a rel="embed" href="{0[url]}">{0[url]}</a>
    </div>
</body>
</html>
""".format({
    'displaypath': displaypath,
    'displaypathattr': quoteattr(displaypath),
    'navpath': navpath,
    'url': url
})


