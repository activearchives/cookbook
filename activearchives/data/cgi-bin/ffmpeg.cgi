#!/usr/bin/env python

import cgi, subprocess, json, re, rdflib
import cgitb; cgitb.enable()
FFMPEG = "ffmpeg"

DC = rdflib.Namespace("http://purl.org/dc/elements/1.1/")
DCMITYPE = rdflib.Namespace("http://purl.org/dc/dcmitype/")
MEDIA = rdflib.Namespace("http://search.yahoo.com/mrss/")
AA = rdflib.Namespace("http://activearchives.org/terms/")

ret = rdflib.Graph()
fs = cgi.FieldStorage()
url = fs.getvalue("url", "http://prototypingfutures.net/archive/video/Film%20op%2020-05-2012%20om%2015.39.webm")
s = rdflib.URIRef(url)
p = subprocess.Popen([FFMPEG, "-i", url], stderr=subprocess.PIPE)
o = p.communicate()[1]
"""
 ## sample ffmpeg output
  Duration: 00:08:16.25, start: 0.000000, bitrate: 290 kb/s
    Stream #0:0: Video: vp8, yuv420p, 640x480, SAR 1:1 DAR 4:3, 30 fps, 30 tbr, 1k tbn, 1k tbc (default)
    Stream #0:1: Audio: vorbis, 44100 Hz, stereo, s16 (default)
"""
m = re.search(r"Duration: .*? (\d\d):(\d\d):(\d\d)\.(\d\d)", o, re.X)
if m:
    d = [int(x) for x in m.groups()]
    seconds = 3600*d[0] + 60*d[1] + d[2] + float("."+str(d[3]))
    ret.add((s, MEDIA['duration'], rdflib.Literal(seconds)))
    # ret['duration_raw'] = m.groups()
# re hack: avoids 0x445 by requiring 2 or more nums
m = re.search(r"Stream .*? Video: \s* (?P<codec>\w+) .*? (?P<width>\d\d+)x(?P<height>\d\d+)", o, re.X)
video = False
if m:
    md = m.groupdict()
    video=True
    # ret['video'] = {'codec': md.get("codec"), 'width': int(md.get("width")), 'height': int(md.get("height"))}
    ret.add((s, MEDIA['type'], rdflib.Literal('video')))
    ret.add((s, MEDIA['width'], rdflib.Literal(int(md.get("width")))))
    ret.add((s, MEDIA['height'], rdflib.Literal(int(md.get("height")))))
m = re.search(r"Stream .*? Audio: \s* (?P<codec>\w+)", o, re.X)
if m:
    md = m.groupdict()
    # ret['audio'] = {'codec': md.get("codec")}
    if not video:
        ret.add((s, MEDIA['type'], rdflib.Literal('audio')))



print "Content-type:application/rdf+xml"
print
print ret.serialize()

