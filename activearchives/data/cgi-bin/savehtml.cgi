#!/usr/bin/env python

import cgitb; cgitb.enable()
import cgi, json, sys, os
import html5lib
from xml.etree import cElementTree as ET 

env = os.environ
referer = env.get("HTTP_REFERER")
pwd = env.get("PWD", env.get("PATH_TRANSLATED"))
fs = cgi.FieldStorage()
path = fs.getvalue("path", "").strip("/")
data = fs.getvalue("data", "")
prettyprint = fs.getvalue("prettyprint", "")

# print "Content-type: application/json;charset=utf-8"
# print
# print json.dumps({'result': True, 'message': 'saved', 'pwd': pwd, 'path': path, 'referer': referer})
# sys.exit(0)

def indent(elem, level=0):
    """ in-place pretty-printer from effbot.org """
    i = "\n" + level*"  "
    if len(elem):
        if not elem.text or not elem.text.strip():
            elem.text = i + "  "
        if not elem.tail or not elem.tail.strip():
            elem.tail = i
        for elem in elem:
            indent(elem, level+1)
        if not elem.tail or not elem.tail.strip():
            elem.tail = i
    else:
        if level and (not elem.tail or not elem.tail.strip()):
            elem.tail = i

if path:
    abspath = os.path.join(pwd, path)
    try:
        if os.path.exists(abspath):
            if os.path.exists(abspath+"~"): # for windows (rename fails if ~ exists)
                os.remove(abspath+"~")
            os.rename(abspath, abspath+"~")
        f = open(abspath, "w")
        if (prettyprint):
            t = html5lib.parse(data, namespaceHTMLElements=False)
            indent(t)
            data = ET.tostring(t, method="html")
        f.write(data)
        f.close()
        print "Content-type: application/json;charset=utf-8"
        print
        print json.dumps({'result': True, 'message': 'saved'})

    except OSError, e:
        print "Content-type: application/json;charset=utf-8"
        print
        print json.dumps({'result': False, 'message': 'OSError: '+str(e)})
