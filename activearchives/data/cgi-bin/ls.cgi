#!/usr/bin/env python

import cgi, rdflib, os, sys, mimetypes
import cgitb; cgitb.enable()


def guessmime (f):
    return mimetypes.guess_type(f)[0]

DIR = rdflib.Literal("inode/directory")
DC = rdflib.Namespace("http://purl.org/dc/elements/1.1/")
DCT = rdflib.Namespace("http://purl.org/dc/terms/")
DCMITYPE = rdflib.Namespace("http://purl.org/dc/dcmitype/")
AA = rdflib.Namespace("http://activearchives.org/terms/")

g = rdflib.Graph()
fs = cgi.FieldStorage()
format = fs.getvalue("format", "xml")
path = fs.getvalue("path", "")
pwd = os.environ.get("PWD", os.environ.get("PATH_TRANSLATED", "."))
rpath = fs.getvalue("path", "").strip("/")
apath = os.path.abspath(os.path.join(pwd, rpath)).rstrip("/")+"/"

s = rdflib.URIRef("file://"+apath)
g.add((s, DC['format'], DIR))
files = os.listdir(apath)
for f in files:
    fpath = os.path.join(apath, f)
    if os.path.isdir(fpath):
        fpath = fpath.rstrip("/")+"/"
    fs = rdflib.URIRef("file://"+fpath)
    g.add((fs, DC['title'], rdflib.Literal(f)))
    g.add((fs, DCT['isPartOf'], s))
    if os.path.isdir(fpath):
        g.add((fs, DC['format'], DIR))
    else:
        mt = guessmime(fpath)
        if mt:
            g.add((fs, DC['format'], rdflib.Literal(mt)))
        stat = os.stat(fpath)
        g.add((fs, DCT['extent'], rdflib.Literal(stat.st_size)))

# seen = set((url, ))
        # ret.add((s, DC['format'], rdflib.Literal(ct)))
        # if info.get("content-length"):
        #     ret.add((s, AA['content-length'], rdflib.Literal(info.get("content-length"))))

MIMES = {}
MIMES['xml'] = "application/rdf+xml"
MIMES['turtle'] = "plain/text"

print "Content-type:{0};charset=utf-8".format(MIMES[format])
print
print g.serialize(format=format)

