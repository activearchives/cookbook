#!/usr/bin/python

from activearchives.srt import aasrtparse, timecode
import sys, argparse


# HOW TO ABSTRACT
# process a list of files, produce a new set of transformed files (with the same names ? -- mogrify ?!)

parser = argparse.ArgumentParser(description='Add end times to SRT files')
parser.add_argument('--style', dest='style', default="aasrt", help='timesheet style (values: aa (default), srt')
parser.add_argument('srtfile', nargs='*', help='srtfile(s) to process')

def main(args=None, stdin=sys.stdin, stdout=sys.stdout):
    args = parser.parse_args(args)
    if len(args.srtfile) == 0:
        args.srtfile.append(sys.stdin)
        use_stdout = True
    else:
        use_stdout = False

    for f in args.srtfile:
        if type(f) == str or type(f) == unicode:
            filename = f
            f = open(f)
        else:
            filename = None
        input = f.read()
        if type(input) == str:
            input = input.decode("utf-8")

        titles = aasrtparse(input)

        if titles[0]['content']:
            stdout.write(titles[0]['content'].encode("utf-8"))

        for title in titles[1:]:
            if 'nextstart' in title and title['end'] == None:
                title['end'] = title['nextstart']
            if 'end' in title and title['end']:
                stdout.write("{0} --> {1}".format(timecode(title['start'], 'compactsrt'), timecode(title['end'], 'compactsrt')))
            else:
                stdout.write("{0} -->".format(timecode(title['start'], 'compactsrt')))
            stdout.write(title['content'].encode("utf-8"))


