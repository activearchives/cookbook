#!/usr/bin/python

from activearchives.srt import aasrtparse, timecode
import sys


def main(args=None, stdin=sys.stdin, stdout=sys.stdout):
    try:
        input = open(args[0]).read()
    except IndexError:
        input = stdin.read()
    if type(input) == str:
        input = input.decode("utf-8")

    titles = aasrtparse(input)

    if titles[0]['content']:
        stdout.write(titles[0]['content'].encode("utf-8"))

    for title in titles[1:]:
        if 'nextstart' in title and title['end'] == title['nextstart']:
            title['end'] = None
        if 'end' in title and title['end']:
            stdout.write("{0} --> {1}".format(timecode(title['start'], 'compactsrt'), timecode(title['end'], 'compactsrt')))
        else:
            stdout.write("{0} -->".format(timecode(title['start'], 'compactsrt')))
        stdout.write(title['content'].encode("utf-8"))

