#!/usr/bin/python

from __future__ import print_function
import argparse, os, sys
import activearchives
import whoosh
from whoosh.index import create_in, open_dir
from whoosh.fields import *
from activearchives.srt import aasrtparse, timecode
# import yaml

""" Annotation indexer """

def try_yaml_document (content):
    doc = None
    try:
        for doc in yaml.safe_load_all(content):
            break
    except Exception:
        pass

    return doc

def line_fragment (start, end):
    if end:
        return "l={0},{1}".format(start,end)
    else:
        return "l={0}".format(start)

def time_fragment (start, end=None):
    if end:
        return "t={0},{1}".format(timecode(start, style='html5'), timecode(end, style='html5'))
    else:
        return "t={0}".format(timecode(start, style='html5'))

def main(args=None):
    parser = argparse.ArgumentParser(description='Index documents')
    parser.add_argument('--path', default=u'./aaindex', help='the path of the index directory')
    args = parser.parse_args(args)

    indexpath = args.path
    if (not os.path.exists(indexpath)):
        print('Creating new index {0}'.format(indexpath), file=sys.stderr)
        os.mkdir(indexpath)

        annotationSchema = Schema(
            id=ID(stored=True),
            bodyUrl = ID(stored=True),
            bodyFragment = ID(stored=True),
            bodyFragmentTypes = KEYWORD(stored=True),
            bodyFragmentLineStart = NUMERIC(stored=True),
            bodyFragmentLineEnd = NUMERIC(stored=True),

            # content & tags are expressed in the body,
            # but as they describe the target
            # are conceptually an attribute of the annotation
            content=TEXT(stored=True),
            tags = KEYWORD(stored=True, lowercase=True, commas=True),
            
            targetUrl = ID(stored=True),
            targetFragment = ID(stored=True),
            targetFragmentTypes = KEYWORD(stored=True),
            targetFragmentTimeStart = NUMERIC(stored=True),
            targetFragmentTimeEnd = NUMERIC(stored=True),
            targetContent=TEXT
        )
        ix = create_in(indexpath, annotationSchema)
    else:
        ix = open_dir(indexpath)

    about = None

    writer = ix.writer()
    for root, dd, ff in os.walk(u"."):
        for f in ff:
            base, ext = os.path.splitext(f)
            if ext.lower() == ".srt":
                fp = os.path.join(root, f)
                fp = os.path.relpath(fp) # strips ./ at start
                fpsrt = fp
                print(fp, file=sys.stderr)
                content = open(fp).read().decode("utf-8")
                # meta = try_yaml_document(content)
                # print("meta", meta)
                titles = aasrtparse(content)
                for t in titles:
                    if 'href' not in t or t['href'] == None:
                        print ("Skipping", t, file=sys.stderr)
                        continue
                    args = {}
                    args['bodyUrl'] = unicode(fpsrt)
                    args['bodyFragmentTypes'] = u'linerange'
                    bfrag = None
                    if 'lineStart' in t:
                        args['bodyFragmentLineStart'] = t['lineStart']
                        args['bodyFragmentLineEnd'] = t['lineEnd']
                        bfrag = line_fragment(t['lineStart'], t['lineEnd'])
                        args['bodyFragment'] = unicode(bfrag)
                    if bfrag:
                        bodyUrlFull = fpsrt + "#" + bfrag
                    else:
                        bodyUrlFull = fpsrt

                    args['targetUrl'] = unicode(t['href'])
                    args['targetFragmentTypes'] = u'timerange'
                    tfrag = None
                    if 'start' in t:
                        args['targetFragmentTimeStart'] = t['start']
                        if 'end' in t:
                            args['targetFragmentTimeEnd'] = t['end']
                            tfrag = time_fragment(t['start'], t['end'])
                        else:
                            tfrag = time_fragment(t['start'])
                        args['targetFragment'] = unicode(tfrag)

                    args['content'] = t['content']
                    if tfrag:
                        targetUrlFull = t['href']+"#"+tfrag
                    else:
                        targetUrlFull = t['href']

                    print ("{0} <==> {1}".format(bodyUrlFull, targetUrlFull, file=sys.stderr))
                    # print (t['content'], file=sys.stderr)
                    print (file=sys.stderr)
                    # url = fp + ""

                    writer.update_document(**args)
    writer.commit()

if __name__ == "__main__":
    main()
