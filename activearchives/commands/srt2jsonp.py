#!/usr/bin/python

from activearchives.srt import aasrtparse, timecode
import sys, json


def main(args=None, stdin=sys.stdin, stdout=sys.stdout):
    source = None
    try:
        source = args[0]
        input = open(source).read()
    except IndexError:
        input = stdin.read()
    if type(input) == str:
        input = input.decode("utf-8")

    titles = aasrtparse(input)
    # from pprint import pprint
    # pprint(titles)
    out= {}
    if source:
        out['source'] = source
    out['titles'] = titles
    sys.stdout.write("jsonp(");
    json.dump(out, sys.stdout, sort_keys=True, indent=2, separators=(',',': '))
    sys.stdout.write(");\n")

