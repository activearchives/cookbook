#!/usr/bin/python
# twisted server
# 1. serve local files from pwd
# 2. serve /lib from aa/htdocs

import argparse, socket, os
import activearchives

from twisted.web.server import Site
from twisted.web.resource import Resource
from twisted.web.static import File, DirectoryLister, getTypeAndEncoding, formatFileSize
from twisted.internet import reactor
from twisted.web.twcgi import CGIDirectory
from twisted.internet.error import CannotListenError
from twisted.python import filepath
from jinja2 import Environment, PackageLoader, ChoiceLoader, FileSystemLoader
from time import sleep


USER_AA_PATH = os.path.expanduser("~/.aa")


class ShadowFile (File):
    """ Allow "shadow" paths to be added that serve matching files first. NB: directory listings of shadow directories are not enabled. """
    def __init__(self, path, *args, **kwargs):
        self.shadows = []
        super(ShadowFile, self).__init__(path, *args, **kwargs)

    def addShadow (self, path):
        self.shadows.append(File(path))

    def getChild(self, path, request):
        for s in self.shadows:
            ret = s.getChild(path, request)
            if ret != s.childNotFound and not isinstance(ret, DirectoryLister):
                return ret
        return super(ShadowFile, self).getChild(path, request)

# import urllib, cgi, itertools


def main(args=None):
    parser = argparse.ArgumentParser(description='Happy to serve you')
    parser.add_argument('--port', type=int, default=8000, help='the port number to listen to')
    parser.add_argument('-t', '--notryports', default=True, action="store_false", help='if a port is busy, automatically try other ones')
    parser.add_argument('--share', default=False, action="store_true", help='Run as server accessible via your local network')
    args = parser.parse_args(args)

    # server = BaseHTTPServer.HTTPServer
    # handler = AAHandler
    # handler.cgi_directories = ["/cgi-bin"]

    tryports = args.notryports
    port = args.port
    ipaddr = None

    if args.share:
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        s.connect(("wikipedia.org",80))
        ipaddr = s.getsockname()[0]
        interface = ipaddr # None would listen to all
        s.close()
    else:
        interface = "localhost"

    # Twisted
    cwd = os.getcwd()
    resource = File(".") # Resource()
    resource.putChild("f", File(cwd))
    aa = ShadowFile(activearchives.getDataPath("htdocs"))
    # Allow user .aa folder to shadow files served from /aa
    if os.path.exists(os.path.join(USER_AA_PATH, "htdocs")):
        aa.addShadow(os.path.join(USER_AA_PATH, "htdocs"))
    aa.putChild("cgi-bin", CGIDirectory(activearchives.getDataPath("cgi-bin")))
    resource.putChild("aa", aa)
    # resource.putChild("", File(activearchives.getDataPath("root/index.html")))
    # resource.putChild("", File("."))

    factory = Site(resource)


    while True:
        try:
            if ipaddr:
                server_address = (ipaddr, port)
                servername = ipaddr
            else:
                server_address = ("", port)
                servername = "localhost"

            reactor.listenTCP(port, factory, interface=interface)
            print "Archiving starts now --> http://{0}:{1}".format(servername, port)
            reactor.run()

        except (socket.error, CannotListenError) as e:
            if isinstance(e, CannotListenError) or e.errno == 98:
                if tryports:
                    if port < 2000:
                        port = 2000
                    else:
                        port += 1
                    sleep(.01)
                else:
                    print """
====================================
Error: port ({0}) is already in use
====================================

You can pick another port number
(for example 9999) with:

    aa --port 9999
""".format(port)
                    break
            else:
                raise(e)

if __name__ == "__main__":
    main()
