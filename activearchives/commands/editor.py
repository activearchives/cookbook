#!/usr/bin/python
# twisted server
# 1. serve local files from pwd
# 2. serve /lib from aa/htdocs

import argparse, socket, os
import activearchives

from twisted.web.server import Site
from twisted.web.resource import Resource
from twisted.web.static import File, DirectoryLister, getTypeAndEncoding, formatFileSize
from twisted.internet import reactor
from twisted.web.twcgi import CGIDirectory
from twisted.internet.error import CannotListenError
from twisted.python import filepath
from jinja2 import Environment, PackageLoader, ChoiceLoader, FileSystemLoader
from time import sleep


USER_AA_PATH = os.path.expanduser("~/.aa")


class ShadowFile (File):
    """ Allow "shadow" paths to be added that serve matching files first. NB: directory listings of shadow directories are not enabled. """
    def __init__(self, path, *args, **kwargs):
        self.shadows = []
        super(ShadowFile, self).__init__(path, *args, **kwargs)

    def addShadow (self, path):
        self.shadows.append(File(path))

    def getChild(self, path, request):
        for s in self.shadows:
            ret = s.getChild(path, request)
            if ret != s.childNotFound and not isinstance(ret, DirectoryLister):
                return ret
        return super(ShadowFile, self).getChild(path, request)

import urllib, cgi, itertools

if os.path.exists(os.path.join(USER_AA_PATH, 'templates')):
    env = Environment(loader=ChoiceLoader([
        FileSystemLoader(os.path.join(USER_AA_PATH, 'templates')),
        PackageLoader('activearchives', 'templates')
    ]))
else:
    env = Environment(loader=PackageLoader('activearchives', 'templates'))

class AADirectoryLister(Resource):
    """
    Print the content of a directory.

    @ivar template: page template used to render the content of the directory.
        It must contain the format keys B{header} and B{tableContent}.
    @type template: C{str}

    @ivar linePattern: template used to render one line in the listing table.
        It must contain the format keys B{class}, B{href}, B{text}, B{size},
        B{type} and B{encoding}.
    @type linePattern: C{str}

    @ivar contentEncodings: a mapping of extensions to encoding types.
    @type contentEncodings: C{dict}

    @ivar defaultType: default type used when no mimetype is detected.
    @type defaultType: C{str}

    @ivar dirs: filtered content of C{path}, if the whole content should not be
        displayed (default to C{None}, which means the actual content of
        C{path} is printed).
    @type dirs: C{NoneType} or C{list}

    @ivar path: directory which content should be listed.
    @type path: C{str}
    """

    def __init__(self, pathname, dirs=None,
                 contentTypes=File.contentTypes,
                 contentEncodings=File.contentEncodings,
                 defaultType='text/html'):
        Resource.__init__(self)
        self.contentTypes = contentTypes
        self.contentEncodings = contentEncodings
        self.defaultType = defaultType
        # dirs allows usage of the File to specify what gets listed
        self.dirs = dirs
        self.path = pathname


    def _getFilesAndDirectories(self, directory):
        """
        Helper returning files and directories in given directory listing, with
        attributes to be used by the listing.html template.

        @return: tuple of (directories, files)
        @rtype: C{tuple} of C{list}
        """
        files = []
        dirs = []
        for path in directory:
            url = urllib.quote(path, "/")
            escapedPath = cgi.escape(path)
            if os.path.isdir(os.path.join(self.path, path)):
                url = url + '/'
                dirs.append({'text': escapedPath + "/", 'href': url,
                             'size': '',
                             'mime': 'inode/directory',
                             'type': 'directory',
                             'encoding': ''})
            else:
                mimetype, encoding = getTypeAndEncoding(path, self.contentTypes,
                                                        self.contentEncodings,
                                                        self.defaultType)
                try:
                    size = os.stat(os.path.join(self.path, path)).st_size
                except OSError:
                    continue
                files.append({
                    'text': escapedPath, "href": url,
                    'mime': mimetype,
                    'type': mimetype,
                    'encoding': (encoding and '[%s]' % encoding or ''),
                    'size': formatFileSize(size)})
        return dirs, files


    def _buildTableContent(self, elements):
        """
        Build a table content using C{self.linePattern} and giving elements odd
        and even classes.
        """
        tableContent = []
        rowClasses = itertools.cycle(['odd', 'even'])
        for element, rowClass in zip(elements, rowClasses):
            element["class"] = rowClass
            tableContent.append(self.linePattern % element)
        return tableContent

    def render(self, request):
        """
        Render a listing of the content of C{self.path}.
        """
        request.setHeader("content-type", "text/html; charset=utf-8")
        if self.dirs is None:
            directory = os.listdir(self.path)
            directory.sort()
        else:
            directory = self.dirs

        dirs, files = self._getFilesAndDirectories(directory)
        # tableContent = "".join(self._buildTableContent(dirs + files))
        template = env.get_template('editor.html')
        cwd = os.getcwd()
        parent = os.path.abspath(os.path.join(self.path, os.pardir))     
        if (os.path.exists(parent) and parent.startswith(cwd)):
            parent = "/" + os.path.relpath(parent, cwd).lstrip(".")
        else:
            parent = None

        return template.render(path=urllib.unquote(request.uri), items=dirs+files, parent=parent).encode("utf-8@")


    def __repr__(self):
        return '<AADirectoryLister of %r>' % self.path

    __str__ = __repr__


class AAFile (File):

    def __init__(self, path, *args, **kwargs):
        super(AAFile, self).__init__(path, *args, **kwargs)

    def directoryListing(self):
        return AADirectoryLister(self.path,
                               self.listNames(),
                               self.contentTypes,
                               self.contentEncodings,
                               self.defaultType)


def main(args=None):
    parser = argparse.ArgumentParser(description='Happy to serve you')
    parser.add_argument('--port', type=int, default=8000, help='the port number to listen to')
    parser.add_argument('-t', '--notryports', default=True, action="store_false", help='if a port is busy, automatically try other ones')
    parser.add_argument('--share', default=False, action="store_true", help='Run as server accessible via your local network')
    args = parser.parse_args(args)

    # server = BaseHTTPServer.HTTPServer
    # handler = AAHandler
    # handler.cgi_directories = ["/cgi-bin"]

    tryports = args.notryports
    port = args.port
    ipaddr = None

    if args.share:
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        s.connect(("wikipedia.org",80))
        ipaddr = s.getsockname()[0]
        interface = ipaddr # None would listen to all
        s.close()
    else:
        interface = "localhost"

    # Twisted
    cwd = os.getcwd()
    resource = AAFile(".") # Resource()
    resource.putChild("f", File(cwd))
    aa = ShadowFile(activearchives.getDataPath("htdocs"))
    # Allow user .aa folder to shadow files served from /aa
    if os.path.exists(os.path.join(USER_AA_PATH, "htdocs")):
        aa.addShadow(os.path.join(USER_AA_PATH, "htdocs"))
    aa.putChild("cgi-bin", CGIDirectory(activearchives.getDataPath("cgi-bin")))
    resource.putChild("aa", aa)
    # resource.putChild("", File(activearchives.getDataPath("root/index.html")))
    # resource.putChild("", File("."))

    factory = Site(resource)


    while True:
        try:
            if ipaddr:
                server_address = (ipaddr, port)
                servername = ipaddr
            else:
                server_address = ("", port)
                servername = "localhost"

            reactor.listenTCP(port, factory, interface=interface)
            print "Archiving starts now --> http://{0}:{1}".format(servername, port)
            reactor.run()

        except (socket.error, CannotListenError) as e:
            if isinstance(e, CannotListenError) or e.errno == 98:
                if tryports:
                    if port < 2000:
                        port = 2000
                    else:
                        port += 1
                    sleep(.01)
                else:
                    print """
====================================
Error: port ({0}) is already in use
====================================

You can pick another port number
(for example 9999) with:

    aa --port 9999
""".format(port)
                    break
            else:
                raise(e)

if __name__ == "__main__":
    main()
