#!/usr/bin/python

import argparse
# from activearchives import AAHandler
# import socket
# from time import sleep
from git import *


def ddiff (d):
    if d.a_blob != None:
        name = d.a_blob.name
    else:
        name = d.b_blob.name

    msg = u""
    if d.deleted_file:
        msg += u"deleted: "
        msg += u"\t" + name
    elif d.new_file:
        msg += u"new file:"
        msg += u"\t" + name
    elif d.renamed:
        msg += u"renamed: "
        msg += u"\t{0} --> {1}".format(d.rename_from, d.rename_to)
    else:
        msg =  u"modified:"
        msg += u"\t" + name

    return msg

def main(args=None):
    parser = argparse.ArgumentParser(description='AA git interface')
    parser.add_argument('cmd')
    args = parser.parse_args(args)

    cmd = args.cmd
    if cmd == "status":
        repo = Repo(".")
        print "STATUS", repo.working_dir
        if not repo.is_dirty():
            print "# CLEAN"

        print "# Changes staged for commit: (DIFF index/head)"
        hcommit = repo.head.commit
        for diff in hcommit.diff(): # DIFF tree against commit
            print "#\t", ddiff(diff).encode("utf-8")
        print

        print "# Changes not staged for commit: (DIFF index/working)"
        for diff in repo.index.diff(None):
            print "#\t", ddiff(diff).encode("utf-8")
        print

        print "# Untracked files"
        for f in repo.untracked_files:
            print "#\t", f


if __name__ == "__main__":
    main()
