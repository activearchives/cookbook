#!/usr/bin/env python

import BaseHTTPServer, CGIHTTPServer
import sys, argparse, socket, os, cgi, urllib
from xml.sax.saxutils import quoteattr
from time import sleep
import posixpath
try:
    from cStringIO import StringIO
except ImportError:
    from StringIO import StringIO

data = os.path.join(os.path.dirname(os.path.realpath(__file__)), "data")

def getDataPath (p=None):
    if p:
        return os.path.join(data, p)
    else:
        return data

class AAHandler (CGIHTTPServer.CGIHTTPRequestHandler):
    rewrites = [
        ("cgi-bin", os.path.join(data, "cgi-bin")),
        ("lib", os.path.join(data, "htdocs")),
    ]

    def run_cgi(self):
        """Execute a CGI script."""
        path = self.path
        dir, rest = self.cgi_info
        print "run_cgi", path, dir, rest
        return CGIHTTPServer.CGIHTTPRequestHandler.run_cgi(self)

    def translate_path(self, path):
        """Translate a /-separated PATH to the local filename syntax.

        Patched version of SimpleHTTPServer's method to allow remapping cgi-bin to an abspath outside of file directory.
        """
        # abandon query parameters

        path = path.split('?',1)[0]
        path = path.split('#',1)[0]
        path = posixpath.normpath(urllib.unquote(path))
        words = path.split('/')
        words = filter(None, words)
        path = os.getcwd()

        ####################
        # REWRITES
        ####################
        if len(words):
            for (rewrite_from, rewrite_to) in self.rewrites:
                if words[0] == rewrite_from:
                    words = words[1:]
                    path = rewrite_to
                    break
        ####################

        for word in words:
            drive, word = os.path.splitdrive(word)
            head, word = os.path.split(word)
            if word in (os.curdir, os.pardir): continue
            path = os.path.join(path, word)
        return path

    # TODO: REDIRECT TO A directory listing CGI

    def list_directory(self, path):
        """Helper to produce a directory listing (absent index.html).

        Return value is either a file object, or None (indicating an
        error).  In either case, the headers are sent, making the
        interface the same as for send_head().

        """
        try:
            listdir = os.listdir(path)
        except os.error:
            self.send_error(404, "No permission to list directory")
            return None


        self.path = "/cgi-bin/index.cgi?path=" + urllib.quote(self.path)
        self.is_cgi() # force cgi_info to get generated
        return self.run_cgi()



